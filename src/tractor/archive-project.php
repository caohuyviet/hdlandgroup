<?php
/**
 * The template for displaying archive Project pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Tractor
 * @since   1.0
 */
get_header();

$style      = Tractor::setting( 'archive_project_style' );
$columns    = Tractor::setting( 'archive_project_columns' );
$gutter     = Tractor::setting( 'archive_project_gutter' );
$image_size = Tractor::setting( 'archive_project_thumbnail_size' );
$animation  = Tractor::setting( 'archive_project_animation' );
?>
<?php Tractor_Templates::title_bar(); ?>
	<div id="page-content" class="page-content">
		<div class="container">
			<div class="row">

				<?php Tractor_Templates::render_sidebar( 'left' ); ?>

				<div class="page-main-content">
					<?php if ( have_posts() ) : ?>
						<?php
						$args = array();

						$args[] = 'style="' . $style . '"';
						$args[] = 'columns="' . $columns . '"';
						$args[] = 'gutter="' . $gutter . '"';
						$args[] = 'image_size="' . $image_size . '"';
						$args[] = 'animation="' . $animation . '"';
						$args[] = 'pagination="pagination"';
						$args[] = 'pagination_align="center"';
						$args[] = 'main_query="1"';

						$shortcode_string = '[tm_project ' . implode( ' ', $args ) . ']';

						echo do_shortcode( $shortcode_string );
						?>
					<?php else :
						get_template_part( 'components/content', 'none' );
					endif; ?>
				</div>

				<?php Tractor_Templates::render_sidebar( 'right' ); ?>

			</div>
		</div>
	</div>
<?php get_footer();
