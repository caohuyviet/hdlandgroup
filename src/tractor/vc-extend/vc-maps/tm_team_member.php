<?php

class WPBakeryShortCode_TM_Team_Member extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		Tractor_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Team Member', 'tractor' ),
	'base'                      => 'tm_team_member',
	'category'                  => TRACTOR_VC_SHORTCODE_CATEGORY,
	'allowed_container_element' => 'vc_row',
	'icon'                      => 'insight-i insight-i-member',
	'params'                    => array_merge( array(
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Style', 'tractor' ),
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( '1', 'tractor' ) => '1',
				esc_html__( '2', 'tractor' ) => '2',
				esc_html__( '3', 'tractor' ) => '3',
			),
			'std'         => '1',
		),
		array(
			'type'        => 'attach_image',
			'heading'     => esc_html__( 'Photo of member', 'tractor' ),
			'param_name'  => 'photo',
			'admin_label' => true,
		),
		array(
			'type'       => 'dropdown',
			'heading'    => esc_html__( 'Photo Effect', 'tractor' ),
			'param_name' => 'photo_effect',
			'value'      => array(
				esc_html__( 'None', 'tractor' )      => '',
				esc_html__( 'Grayscale', 'tractor' ) => 'grayscale',
			),
			'std'        => '',
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Name', 'tractor' ),
			'admin_label' => true,
			'param_name'  => 'name',
		),
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Position', 'tractor' ),
			'param_name'  => 'position',
			'description' => esc_html__( 'Example: CEO/Founder', 'tractor' ),
		),
		array(
			'type'       => 'textarea',
			'heading'    => esc_html__( 'Description', 'tractor' ),
			'param_name' => 'desc',
		),
		array(
			'type'       => 'textfield',
			'heading'    => esc_html__( 'Profile url', 'tractor' ),
			'param_name' => 'profile',
		),
		Tractor_VC::get_animation_field(),
		Tractor_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Social Networks', 'tractor' ),
			'type'       => 'param_group',
			'heading'    => esc_html__( 'Social Networks', 'tractor' ),
			'param_name' => 'social_networks',
			'params'     => array_merge( Tractor_VC::icon_libraries( array( 'allow_none' => true ) ), array(
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Link', 'tractor' ),
					'param_name'  => 'link',
					'admin_label' => true,
				),
			) ),
			'value'      => rawurlencode( wp_json_encode( array(
				array(
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-twitter',
				),
				array(
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-facebook',
				),
				array(
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-googleplus',
				),
				array(
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-linkedin',
				),
			) ) ),
		),
	), Tractor_VC::get_vc_spacing_tab() ),
) );
