<?php

class WPBakeryShortCode_TM_Separator extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $tractor_shortcode_lg_css;
		global $tractor_shortcode_md_css;
		global $tractor_shortcode_sm_css;
		global $tractor_shortcode_xs_css;
		extract( $atts );

		$wrapper_tmp = '';

		if ( $atts['align'] !== '' ) {
			$wrapper_tmp .= "text-align: {$atts['align']};";
		}

		if ( $atts['md_align'] !== '' ) {
			$tractor_shortcode_md_css .= "$selector { text-align: {$atts['md_align']} }";
		}

		if ( $atts['sm_align'] !== '' ) {
			$tractor_shortcode_sm_css .= "$selector { text-align: {$atts['sm_align']} }";
		}

		if ( $atts['xs_align'] !== '' ) {
			$tractor_shortcode_xs_css .= "$selector { text-align: {$atts['xs_align']} }";
		}

		if ( $wrapper_tmp !== '' ) {
			$tractor_shortcode_lg_css .= "$selector { $wrapper_tmp }";
		}

		Tractor_VC::get_vc_spacing_css( $selector, $atts );
	}

}

vc_map( array(
	'name'     => esc_html__( 'Separator', 'tractor' ),
	'base'     => 'tm_separator',
	'category' => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-call-to-action',
	'params'   => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Modern Dots', 'tractor' )      => 'modern-dots',
				esc_html__( 'Thick Short Line', 'tractor' ) => 'thick-short-line',
				esc_html__( 'Single Line', 'tractor' )      => 'single-line',
			),
			'std'         => 'thick-short-line',
		),
		array(
			'heading'     => esc_html__( 'Smooth Scroll', 'tractor' ),
			'description' => esc_html__( 'Input valid id to smooth scroll to a section on click. E.g #about-us-section', 'tractor' ),
			'type'        => 'textfield',
			'param_name'  => 'smooth_scroll',
		),
		Tractor_VC::extra_class_field(),
	), Tractor_VC::get_alignment_fields(), Tractor_VC::get_vc_spacing_tab() ),
) );

