<?php

class WPBakeryShortCode_TM_Jobs_Box extends WPBakeryShortCode {
	public function get_inline_css( $selector = '', $atts ) {
		Tractor_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'     => esc_html__( 'Jobs Box', 'tractor' ),
	'base'     => 'tm_jobs_box',
	'category' => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-info-boxes',
	'params'   => array_merge( array(
		array(
			'heading'     => esc_html__( 'Title', 'tractor' ),
			'type'        => 'textfield',
			'param_name'  => 'title',
			'admin_label' => true,
			'std'         => '',
		),
		array(
			'heading'    => esc_html__( 'Link', 'tractor' ),
			'type'       => 'vc_link',
			'param_name' => 'link',
		),
		Tractor_VC::get_animation_field(),
		Tractor_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Jobs', 'tractor' ),
			'heading'    => esc_html__( 'Jobs', 'tractor' ),
			'type'       => 'param_group',
			'param_name' => 'jobs',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Title', 'tractor' ),
					'type'        => 'textfield',
					'param_name'  => 'title',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Text', 'tractor' ),
					'type'       => 'textfield',
					'param_name' => 'text',
				),
				array(
					'heading'    => esc_html__( 'Link', 'tractor' ),
					'type'       => 'vc_link',
					'param_name' => 'link',
				),
			),
		),
	), Tractor_VC::get_vc_spacing_tab() ),
) );

