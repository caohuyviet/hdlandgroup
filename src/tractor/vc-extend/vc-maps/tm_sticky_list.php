<?php

class WPBakeryShortCode_TM_Sticky_List extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $tractor_shortcode_lg_css;

		$bg_tmp = Tractor_Helper::get_shortcode_css_color_inherit( 'background-color', $atts['background_color'], $atts['custom_background_color'] );

		if ( $bg_tmp !== '' ) {
			$tractor_shortcode_lg_css .= "$selector { $bg_tmp }";
		}

		$heading_tmp = Tractor_Helper::get_shortcode_css_color_inherit( 'color', $atts['title_color'], $atts['custom_title_color'] );

		if ( $heading_tmp !== '' ) {
			$tractor_shortcode_lg_css .= "$selector a { $heading_tmp }";
		}

		$hover_heading_tmp = Tractor_Helper::get_shortcode_css_color_inherit( 'color', $atts['title_hover_color'], $atts['custom_title_hover_color'] );

		if ( $hover_heading_tmp !== '' ) {
			$tractor_shortcode_lg_css .= "$selector a:hover { $hover_heading_tmp }";
		}

		Tractor_VC::get_responsive_css( array(
			'element' => "$selector a",
			'atts'    => array(
				'font-size' => array(
					'media_str' => $atts['heading_font_size'],
					'unit'      => 'px',
				),
			),
		) );

		Tractor_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$styling_tab = esc_html__( 'Styling', 'tractor' );

vc_map( array(
	'name'                      => esc_html__( 'Sticky List', 'tractor' ),
	'base'                      => 'tm_sticky_list',
	'category'                  => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-list',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'group'      => esc_html__( 'Items', 'tractor' ),
			'heading'    => esc_html__( 'Items', 'tractor' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Title', 'tractor' ),
					'type'        => 'textfield',
					'param_name'  => 'item_title',
					'admin_label' => true,
				),
				array(
					'heading'     => esc_html__( 'Link', 'tractor' ),
					'type'        => 'textfield',
					'param_name'  => 'item_link',
					'admin_label' => true,
				),
			),
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Background Color', 'tractor' ),
			'type'             => 'dropdown',
			'param_name'       => 'background_color',
			'value'            => array(
				esc_html__( 'Default Color', 'tractor' )   => '',
				esc_html__( 'Primary Color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary Color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom Color', 'tractor' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Background Color', 'tractor' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_background_color',
			'dependency'       => array(
				'element' => 'background_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Title Color', 'tractor' ),
			'type'             => 'dropdown',
			'param_name'       => 'title_color',
			'value'            => array(
				esc_html__( 'Default Color', 'tractor' )   => '',
				esc_html__( 'Primary Color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary Color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom Color', 'tractor' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Title Color', 'tractor' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_title_color',
			'dependency'       => array(
				'element' => 'title_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Title Hover Color', 'tractor' ),
			'type'             => 'dropdown',
			'param_name'       => 'title_hover_color',
			'value'            => array(
				esc_html__( 'Default Color', 'tractor' )   => '',
				esc_html__( 'Primary Color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary Color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom Color', 'tractor' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_tab,
			'heading'          => esc_html__( 'Custom Title Hover Color', 'tractor' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_title_hover_color',
			'dependency'       => array(
				'element' => 'title_hover_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'       => $styling_tab,
			'heading'     => esc_html__( 'Heading Font Size', 'tractor' ),
			'type'        => 'number_responsive',
			'param_name'  => 'heading_font_size',
			'min'         => 8,
			'suffix'      => 'px',
			'media_query' => array(
				'lg' => '',
				'md' => '',
				'sm' => '',
				'xs' => '',
			),
		),
	), array(
			Tractor_VC::get_animation_field(),
			Tractor_VC::extra_class_field(),
		),
		Tractor_VC::get_vc_spacing_tab()
	),
) );
