<?php

class WPBakeryShortCode_TM_Timeline extends WPBakeryShortCode {
	public function get_inline_css( $selector = '', $atts ) {
		Tractor_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Timeline', 'tractor' ),
	'base'                      => 'tm_timeline',
	'category'                  => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-timeline',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge(
		array(
			array(
				'heading'     => esc_html__( 'Style', 'tractor' ),
				'type'        => 'dropdown',
				'param_name'  => 'style',
				'admin_label' => true,
				'value'       => array(
					esc_html__( '01', 'tractor' ) => '01',
					esc_html__( '02', 'tractor' ) => '02',
				),
				'std'         => '01',
			),
			array(
				'group'      => esc_html__( 'Items', 'tractor' ),
				'heading'    => esc_html__( 'Items', 'tractor' ),
				'type'       => 'param_group',
				'param_name' => 'items',
				'params'     => array(
					array(
						'heading'    => esc_html__( 'Image', 'tractor' ),
						'type'       => 'attach_image',
						'param_name' => 'image',
					),
					array(
						'heading'     => esc_html__( 'Title', 'tractor' ),
						'type'        => 'textfield',
						'param_name'  => 'title',
						'admin_label' => true,
					),
					array(
						'heading'     => esc_html__( 'Date Time', 'tractor' ),
						'description' => esc_html__( 'Date and time format (yyyy/mm/dd hh:mm).', 'tractor' ),
						'type'        => 'datetimepicker',
						'param_name'  => 'datetime',
						'value'       => '',
						'admin_label' => true,
					),
					array(
						'heading'    => esc_html__( 'Text', 'tractor' ),
						'type'       => 'textarea',
						'param_name' => 'text',
					),
				),

			),
		), Tractor_VC::get_vc_spacing_tab()
	),
) );
