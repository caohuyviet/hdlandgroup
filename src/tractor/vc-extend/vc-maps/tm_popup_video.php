<?php

class WPBakeryShortCode_TM_Popup_Video extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $tractor_shortcode_lg_css;
		Tractor_VC::get_vc_spacing_css( $selector, $atts );
		if ( $atts['bg_image'] !== '' ) {
			$tractor_shortcode_lg_css .= "$selector .video-play {background-image: url(" . wp_get_attachment_image_url( $atts['bg_image'], 'full' ) . ")};";
		}
	}
}

$posters_style = array(
	'poster-01',
	'poster-02',
	'poster-03',
	'poster-04',
	'poster-05',
	'poster-06',
);

vc_map( array(
	'name'                      => esc_html__( 'Popup Video', 'tractor' ),
	'base'                      => 'tm_popup_video',
	'category'                  => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-video',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Poster Style 01', 'tractor' ) => 'poster-01',
				esc_html__( 'Poster Style 02', 'tractor' ) => 'poster-02',
				esc_html__( 'Poster Style 03', 'tractor' ) => 'poster-03',
				esc_html__( 'Poster Style 04', 'tractor' ) => 'poster-04',
				esc_html__( 'Poster Style 05', 'tractor' ) => 'poster-05',
				esc_html__( 'Poster Style 06', 'tractor' ) => 'poster-06',
				esc_html__( 'Button Style 01', 'tractor' ) => 'button',
				esc_html__( 'Button Style 02', 'tractor' ) => 'button-02',
				esc_html__( 'Button Style 03', 'tractor' ) => 'button-03',
				esc_html__( 'Button Style 04', 'tractor' ) => 'button-04',
				esc_html__( 'Button Style 05', 'tractor' ) => 'button-05',
				esc_html__( 'Button Style 06', 'tractor' ) => 'button-06',
			),
			'std'         => 'poster-01',
		),
		array(
			'heading'     => esc_html__( 'Video Url', 'tractor' ),
			'description' => esc_html__( 'E.g "https://www.youtube.com/watch?v=9No-FiEInLA"', 'tractor' ),
			'type'        => 'textfield',
			'param_name'  => 'video',
		),
		array(
			'heading'    => esc_html__( 'Video Text', 'tractor' ),
			'type'       => 'textfield',
			'param_name' => 'video_text',
			'std'        => esc_html__( 'Play Video', 'tractor' ),
			'dependency' => array(
				'element'            => 'style',
				'value_not_equal_to' => array(
					'poster-02',
				),
			),
		),
		array(
			'heading'    => esc_html__( 'Button Background Image', 'tractor' ),
			'type'       => 'attach_image',
			'param_name' => 'bg_image',
			'dependency' => array( 'element' => 'style', 'value' => array( 'button-06' ) ),
		),
		array(
			'heading'    => esc_html__( 'Poster Image', 'tractor' ),
			'type'       => 'attach_image',
			'param_name' => 'poster',
			'dependency' => array( 'element' => 'style', 'value' => $posters_style ),
		),
		array(
			'heading'     => esc_html__( 'Poster Image Size', 'tractor' ),
			'description' => esc_html__( 'Controls the size of poster image.', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'image_size',
			'value'       => array(
				esc_html__( '670x455', 'tractor' ) => '670x455',
				esc_html__( '600x420', 'tractor' ) => '600x420',
				esc_html__( '570x364', 'tractor' ) => '570x364',
				esc_html__( '470x320', 'tractor' ) => '470x320',
				esc_html__( 'Full', 'tractor' )    => 'full',
				esc_html__( 'Custom', 'tractor' )  => 'custom',
			),
			'std'         => '670x455',
			'dependency'  => array( 'element' => 'style', 'value' => $posters_style ),
		),
		array(
			'heading'          => esc_html__( 'Image Width', 'tractor' ),
			'type'             => 'number',
			'param_name'       => 'image_size_width',
			'min'              => 0,
			'max'              => 1920,
			'step'             => 10,
			'suffix'           => 'px',
			'dependency'       => array(
				'element' => 'image_size',
				'value'   => array( 'custom' ),
			),
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'heading'          => esc_html__( 'Image Height', 'tractor' ),
			'type'             => 'number',
			'param_name'       => 'image_size_height',
			'min'              => 0,
			'max'              => 1920,
			'step'             => 10,
			'suffix'           => 'px',
			'dependency'       => array(
				'element' => 'image_size',
				'value'   => array( 'custom' ),
			),
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		Tractor_VC::extra_class_field(),
	), Tractor_VC::get_vc_spacing_tab(), Tractor_VC::get_custom_style_tab() ),
) );
