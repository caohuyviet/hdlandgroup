<?php

class WPBakeryShortCode_TM_Cta_Box extends WPBakeryShortCode {
}

vc_map( array(
	'name'                      => esc_html__( 'CTA Box', 'tractor' ),
	'base'                      => 'tm_cta_box',
	'category'                  => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-icons',
	'allowed_container_element' => 'vc_row',
	'params'                    => array(
		array(
			'heading'     => esc_html__( 'Image', 'tractor' ),
			'type'        => 'attach_image',
			'param_name'  => 'image',
			'admin_label' => true,
		),
		array(
			'heading'     => esc_html__( 'Heading', 'tractor' ),
			'type'        => 'textfield',
			'param_name'  => 'heading',
			'admin_label' => true,
		),
		array(
			'heading'    => esc_html__( 'Link', 'tractor' ),
			'type'       => 'vc_link',
			'param_name' => 'link',
		),
	),
) );
