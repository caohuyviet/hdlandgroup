<?php
add_filter( 'vc_autocomplete_tm_service_feature_items_page_callback', 'tractor_tm_service_feature_page_field_callback', 10, 1 );

add_filter( 'vc_autocomplete_tm_service_feature_items_page_render', 'tractor_tm_service_feature_page_field_render', 10, 1 );

function tractor_tm_service_feature_page_field_render( $term ) {
	$args = array(
		'post_type'      => 'service',
		'posts_per_page' => - 1,
		'post_status'    => 'publish',
		'name'           => $term['value'],
	);

	$query = new WP_Query( $args );
	$data  = false;
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) :
			$query->the_post();
			global $post;

			$data = array(
				'label' => get_the_title(),
				'value' => $post->post_name,
			);
		endwhile;
		wp_reset_postdata();
	}

	return $data;
}

function tractor_tm_service_feature_page_field_callback( $search_string ) {
	$data = array();
	$args = array(
		'post_type'      => 'service',
		'posts_per_page' => - 1,
		'post_status'    => 'publish',
		's'              => $search_string,
	);

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) :
			$query->the_post();
			global $post;

			$data[] = array(
				'label' => get_the_title(),
				'value' => $post->post_name,
			);
		endwhile;
	}

	return $data;
}

class WPBakeryShortCode_TM_Service_Feature extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		Tractor_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'     => esc_html__( 'Service Feature', 'tractor' ),
	'base'     => 'tm_service_feature',
	'category' => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-grid',
	'params'   => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Style 01', 'tractor' ) => '01',
			),
			'std'         => '01',
		),
		Tractor_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Items', 'tractor' ),
			'heading'    => esc_html__( 'Items', 'tractor' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array_merge( array(
				array(
					'heading'     => esc_html__( 'Service Page', 'tractor' ),
					'type'        => 'autocomplete',
					'param_name'  => 'page',
					'admin_label' => true,
				),
			), Tractor_VC::icon_libraries() ),
		),
	), Tractor_VC::get_vc_spacing_tab(), Tractor_VC::get_custom_style_tab() ),
) );

