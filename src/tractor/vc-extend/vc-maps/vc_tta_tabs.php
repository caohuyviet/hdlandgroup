<?php
$_color_field                                                = WPBMap::getParam( 'vc_tta_tabs', 'color' );
$_color_field['value'][ esc_html__( 'Primary', 'tractor' ) ] = 'primary';
$_color_field['std']                                         = 'primary';
vc_update_shortcode_param( 'vc_tta_tabs', $_color_field );

vc_update_shortcode_param( 'vc_tta_tabs', array(
	'param_name' => 'style',
	'value'      => array(
		esc_html__( 'Tractor 01', 'tractor' ) => 'tractor-01',
		esc_html__( 'Tractor 02', 'tractor' ) => 'tractor-02',
		esc_html__( 'Tractor 03', 'tractor' ) => 'tractor-03',
		esc_html__( 'Classic', 'tractor' )    => 'classic',
		esc_html__( 'Modern', 'tractor' )     => 'modern',
		esc_html__( 'Flat', 'tractor' )       => 'flat',
		esc_html__( 'Outline', 'tractor' )    => 'outline',
	),
) );
