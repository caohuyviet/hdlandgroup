<?php

class WPBakeryShortCode_TM_Pricing extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		Tractor_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Pricing Table', 'tractor' ),
	'base'                      => 'tm_pricing',
	'category'                  => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-pricing',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( '01', 'tractor' ) => '1',
				esc_html__( '02', 'tractor' ) => '2',
			),
			'std'         => '1',
		),
		array(
			'heading'     => esc_html__( 'Featured', 'tractor' ),
			'description' => esc_html__( 'Checked the box if you want make this item featured', 'tractor' ),
			'type'        => 'checkbox',
			'param_name'  => 'featured',
			'value'       => array( esc_html__( 'Yes', 'tractor' ) => '1' ),
		),
		array(
			'heading'     => esc_html__( 'Title', 'tractor' ),
			'type'        => 'textfield',
			'admin_label' => true,
			'param_name'  => 'title',
		),
		array(
			'heading'     => esc_html__( 'Description', 'tractor' ),
			'description' => esc_html__( 'Controls the text that display under price', 'tractor' ),
			'type'        => 'textarea',
			'param_name'  => 'desc',
		),
		array(
			'heading'          => esc_html__( 'Currency', 'tractor' ),
			'type'             => 'textfield',
			'param_name'       => 'currency',
			'value'            => '$',
			'edit_field_class' => 'vc_col-sm-4',
		),
		array(
			'heading'          => esc_html__( 'Price', 'tractor' ),
			'type'             => 'textfield',
			'param_name'       => 'price',
			'edit_field_class' => 'vc_col-sm-4',
		),
		array(
			'heading'          => esc_html__( 'Period', 'tractor' ),
			'type'             => 'textfield',
			'param_name'       => 'period',
			'value'            => 'per monthly',
			'edit_field_class' => 'vc_col-sm-4',
		),
		array(
			'type'       => 'vc_link',
			'heading'    => esc_html__( 'Button', 'tractor' ),
			'param_name' => 'button',
		),
		Tractor_VC::get_animation_field(),
		Tractor_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Items', 'tractor' ),
			'heading'    => esc_html__( 'Items', 'tractor' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array(
				array(
					'heading'    => esc_html__( 'Icon', 'tractor' ),
					'type'       => 'iconpicker',
					'param_name' => 'icon',
					'settings'   => array(
						'emptyIcon'    => true,
						'type'         => 'ion',
						'iconsPerPage' => 400,
					),
					'value'      => '',
				),
				array(
					'heading'     => esc_html__( 'Text', 'tractor' ),
					'type'        => 'textfield',
					'param_name'  => 'text',
					'admin_label' => true,
				),
			),
		),
	), Tractor_VC::get_vc_spacing_tab() ),
) );
