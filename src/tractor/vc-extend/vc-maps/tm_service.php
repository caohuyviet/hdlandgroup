<?php

add_filter( 'vc_autocomplete_tm_service_taxonomies_callback', array(
	'WPBakeryShortCode_TM_Service',
	'autocomplete_taxonomies_field_search',
), 10, 1 );

add_filter( 'vc_autocomplete_tm_service_taxonomies_render', array(
	Tractor_VC::instance(),
	'autocomplete_taxonomies_field_render',
), 10, 1 );

add_filter( 'vc_autocomplete_tm_service_filter_by_callback', array(
	'WPBakeryShortCode_TM_Service',
	'autocomplete_taxonomies_field_search',
), 10, 1 );

add_filter( 'vc_autocomplete_tm_service_filter_by_render', array(
	Tractor_VC::instance(),
	'autocomplete_taxonomies_field_render',
), 10, 1 );

class WPBakeryShortCode_TM_Service extends WPBakeryShortCode {

	/**
	 * @param $search_string
	 *
	 * @return array|bool
	 */
	function autocomplete_taxonomies_field_search( $search_string ) {
		$data = Tractor_VC::instance()->autocomplete_get_data_from_post_type( $search_string, 'service' );

		return $data;
	}

	public function get_inline_css( $selector = '', $atts ) {
		Tractor_VC::get_grid_css( $selector, $atts );

		Tractor_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$carousel_tab   = esc_html__( 'Carousel Settings', 'tractor' );
$carousel_types = array(
	'carousel',
	'carousel_02',
	'carousel_03',
	'carousel_04',
	'carousel_05',
	'carousel_06',
	'carousel_07',
);
$grid_types     = array(
	'grid_classic_01',
	'grid_classic_02',
	'grid_classic_03',
	'grid_classic_04',
	'grid_classic_05',
);

vc_map( array(
	'name'     => esc_html__( 'Service', 'tractor' ),
	'base'     => 'tm_service',
	'category' => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-grid',
	'params'   => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Grid 01', 'tractor' )     => 'grid_classic_01',
				esc_html__( 'Grid 02', 'tractor' )     => 'grid_classic_02',
				esc_html__( 'Grid 03', 'tractor' )     => 'grid_classic_03',
				esc_html__( 'Grid 04', 'tractor' )     => 'grid_classic_04',
				esc_html__( 'Grid 05', 'tractor' )     => 'grid_classic_05',
				esc_html__( 'Carousel 01', 'tractor' ) => 'carousel',
				esc_html__( 'Carousel 02', 'tractor' ) => 'carousel_02',
				esc_html__( 'Carousel 03', 'tractor' ) => 'carousel_03',
				esc_html__( 'Carousel 04', 'tractor' ) => 'carousel_04',
				esc_html__( 'Carousel 05', 'tractor' ) => 'carousel_05',
				esc_html__( 'Carousel 06', 'tractor' ) => 'carousel_06',
				esc_html__( 'Carousel 07', 'tractor' ) => 'carousel_07',
			),
			'std'         => 'grid_classic_01',
		),
		array(
			'heading'     => esc_html__( 'Columns', 'tractor' ),
			'type'        => 'number_responsive',
			'param_name'  => 'columns',
			'min'         => 1,
			'max'         => 6,
			'step'        => 1,
			'suffix'      => '',
			'media_query' => array(
				'lg' => '3',
				'md' => '',
				'sm' => '2',
				'xs' => '1',
			),
			'dependency'  => array(
				'element' => 'style',
				'value'   => $grid_types
			),
		),
		array(
			'heading'     => esc_html__( 'Columns Gutter', 'tractor' ),
			'description' => esc_html__( 'Controls the gutter of grid columns.', 'tractor' ),
			'type'        => 'number',
			'param_name'  => 'gutter',
			'std'         => 30,
			'min'         => 0,
			'max'         => 100,
			'step'        => 1,
			'suffix'      => 'px',
			'dependency'  => array(
				'element' => 'style',
				'value'   => $grid_types
			),
		),
		array(
			'heading'     => esc_html__( 'Rows Gutter', 'tractor' ),
			'description' => esc_html__( 'Controls the gutter of grid rows.', 'tractor' ),
			'type'        => 'number',
			'param_name'  => 'row_gutter',
			'std'         => 30,
			'min'         => 0,
			'max'         => 100,
			'step'        => 1,
			'suffix'      => 'px',
			'dependency'  => array(
				'element' => 'style',
				'value'   => $grid_types
			),
		),
		array(
			'heading'    => esc_html__( 'Image Size', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'image_size',
			'value'      => array(
				esc_html__( '480x480', 'tractor' ) => '480x480',
				esc_html__( '340x400', 'tractor' ) => '340x400',
				esc_html__( '370x220', 'tractor' ) => '370x220',
				esc_html__( '370x250', 'tractor' ) => '370x250',
				esc_html__( '370x274', 'tractor' ) => '370x274',
				esc_html__( '370x460', 'tractor' ) => '370x460',
				esc_html__( '570x385', 'tractor' ) => '570x385',
				esc_html__( '960x430', 'tractor' ) => '960x430',
			),
			'std'        => '370x220',
		),
		Tractor_VC::get_animation_field( array(
			'std' => 'move-up',
		) ),
		Tractor_VC::extra_class_field(),
		array(
			'group'       => $carousel_tab,
			'heading'     => esc_html__( 'Auto Play', 'tractor' ),
			'description' => esc_html__( 'Delay between transitions (in ms), e.g 3000. Leave blank to disabled.', 'tractor' ),
			'type'        => 'number',
			'suffix'      => 'ms',
			'param_name'  => 'carousel_auto_play',
			'dependency'  => array(
				'element' => 'style',
				'value'   => $carousel_types,
			),
		),
		array(
			'group'      => $carousel_tab,
			'heading'    => esc_html__( 'Loop', 'tractor' ),
			'type'       => 'checkbox',
			'param_name' => 'carousel_loop',
			'value'      => array( esc_html__( 'Yes', 'tractor' ) => '1' ),
			'std'        => '1',
			'dependency' => array(
				'element' => 'style',
				'value'   => $carousel_types,
			),
		),
		array(
			'group'      => $carousel_tab,
			'heading'    => esc_html__( 'Navigation', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'carousel_nav',
			'value'      => Tractor_VC::get_slider_navs(),
			'std'        => '',
			'dependency' => array(
				'element' => 'style',
				'value'   => $carousel_types,
			),
		),
		Tractor_VC::extra_id_field( array(
			'group'      => $carousel_tab,
			'heading'    => esc_html__( 'Slider Button ID', 'tractor' ),
			'param_name' => 'slider_button_id',
			'dependency' => array(
				'element' => 'carousel_nav',
				'value'   => array(
					'custom',
				),
			),
		) ),
		array(
			'group'      => $carousel_tab,
			'heading'    => esc_html__( 'Pagination', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'carousel_pagination',
			'value'      => Tractor_VC::get_slider_dots(),
			'std'        => '',
			'dependency' => array(
				'element' => 'style',
				'value'   => $carousel_types,
			),
		),
		array(
			'group'       => $carousel_tab,
			'heading'     => esc_html__( 'Items Display', 'tractor' ),
			'type'        => 'number_responsive',
			'param_name'  => 'carousel_items_display',
			'min'         => 1,
			'max'         => 10,
			'suffix'      => 'item (s)',
			'media_query' => array(
				'lg' => 3,
				'md' => 3,
				'sm' => 2,
				'xs' => 1,
			),
			'dependency'  => array(
				'element' => 'style',
				'value'   => $carousel_types,
			),
		),
		array(
			'group'      => $carousel_tab,
			'heading'    => esc_html__( 'Gutter', 'tractor' ),
			'type'       => 'number',
			'param_name' => 'carousel_gutter',
			'std'        => 30,
			'min'        => 0,
			'max'        => 50,
			'step'       => 1,
			'suffix'     => 'px',
			'dependency' => array(
				'element' => 'style',
				'value'   => $carousel_types,
			),
		),
		array(
			'group'      => esc_html__( 'Data Settings', 'tractor' ),
			'type'       => 'hidden',
			'param_name' => 'main_query',
			'std'        => '',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'tractor' ),
			'heading'     => esc_html__( 'Items per page', 'tractor' ),
			'description' => esc_html__( 'Number of items to show per page.', 'tractor' ),
			'type'        => 'number',
			'param_name'  => 'number',
			'std'         => 9,
			'min'         => 1,
			'max'         => 100,
			'step'        => 1,
		),
		array(
			'group'              => esc_html__( 'Data Settings', 'tractor' ),
			'heading'            => esc_html__( 'Narrow data source', 'tractor' ),
			'description'        => esc_html__( 'Enter categories, tags or custom taxonomies.', 'tractor' ),
			'type'               => 'autocomplete',
			'param_name'         => 'taxonomies',
			'settings'           => array(
				'multiple'       => true,
				'min_length'     => 1,
				'groups'         => true,
				// In UI show results grouped by groups, default false.
				'unique_values'  => true,
				// In UI show results except selected. NB! You should manually check values in backend, default false.
				'display_inline' => true,
				// In UI show results inline view, default false (each value in own line).
				'delay'          => 500,
				// delay for search. default 500.
				'auto_focus'     => true,
				// auto focus input, default true.
			),
			'param_holder_class' => 'vc_not-for-custom',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'tractor' ),
			'heading'     => esc_html__( 'Order by', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'orderby',
			'value'       => array(
				esc_html__( 'Date', 'tractor' )                  => 'date',
				esc_html__( 'Post ID', 'tractor' )               => 'ID',
				esc_html__( 'Author', 'tractor' )                => 'author',
				esc_html__( 'Title', 'tractor' )                 => 'title',
				esc_html__( 'Last modified date', 'tractor' )    => 'modified',
				esc_html__( 'Post/page parent ID', 'tractor' )   => 'parent',
				esc_html__( 'Number of comments', 'tractor' )    => 'comment_count',
				esc_html__( 'Menu order/Page Order', 'tractor' ) => 'menu_order',
				esc_html__( 'Meta value', 'tractor' )            => 'meta_value',
				esc_html__( 'Meta value number', 'tractor' )     => 'meta_value_num',
				esc_html__( 'Random order', 'tractor' )          => 'rand',
			),
			'description' => esc_html__( 'Select order type. If "Meta value" or "Meta value Number" is chosen then meta key is required.', 'tractor' ),
			'std'         => 'date',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'tractor' ),
			'heading'     => esc_html__( 'Sort order', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'order',
			'value'       => array(
				esc_html__( 'Descending', 'tractor' ) => 'DESC',
				esc_html__( 'Ascending', 'tractor' )  => 'ASC',
			),
			'description' => esc_html__( 'Select sorting order.', 'tractor' ),
			'std'         => 'DESC',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'tractor' ),
			'heading'     => esc_html__( 'Meta key', 'tractor' ),
			'description' => esc_html__( 'Input meta key for grid ordering.', 'tractor' ),
			'type'        => 'textfield',
			'param_name'  => 'meta_key',
			'dependency'  => array(
				'element' => 'orderby',
				'value'   => array(
					'meta_value',
					'meta_value_num',
				),
			),
		),
	), Tractor_VC::get_grid_filter_fields(), Tractor_VC::get_grid_pagination_fields(), Tractor_VC::get_vc_spacing_tab(), Tractor_VC::get_custom_style_tab() ),
) );

