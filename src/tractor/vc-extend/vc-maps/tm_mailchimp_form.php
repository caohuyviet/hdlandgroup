<?php

class WPBakeryShortCode_TM_Mailchimp_Form extends WPBakeryShortCode {

}

vc_map( array(
	'name'                      => esc_html__( 'Mailchimp Form', 'tractor' ),
	'base'                      => 'tm_mailchimp_form',
	'category'                  => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-mailchimp-form',
	'allowed_container_element' => 'vc_row',
	'params'                    => array(
		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Widget title', 'tractor' ),
			'param_name'  => 'title',
			'description' => esc_html__( 'What text use as a widget title. Leave blank to use default widget title.', 'tractor' ),
		),
		array(
			'heading'     => esc_html__( 'Form ID', 'tractor' ),
			'description' => esc_html__( 'Input the id of form. Leave blank to show default form.', 'tractor' ),
			'type'        => 'textfield',
			'param_name'  => 'form_id',
		),
		array(
			'heading'     => esc_html__( 'Style', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( '1', 'tractor' )  => '1',
				esc_html__( '2', 'tractor' )  => '2',
				esc_html__( '3', 'tractor' )  => '3',
				esc_html__( '4', 'tractor' )  => '4',
				esc_html__( '5', 'tractor' )  => '5',
				esc_html__( '6', 'tractor' )  => '6',
				esc_html__( '7', 'tractor' )  => '7',
				esc_html__( '8', 'tractor' )  => '8',
				esc_html__( '9', 'tractor' )  => '9',
				esc_html__( '10', 'tractor' ) => '10',
				esc_html__( '11', 'tractor' ) => '11',
			),
			'std'         => '1',
		),
		Tractor_VC::extra_class_field(),
	),
) );
