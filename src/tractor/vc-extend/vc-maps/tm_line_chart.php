<?php

class WPBakeryShortCode_TM_Line_Chart extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		Tractor_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$legend_tab = esc_html__( 'Tooltips and Legends', 'tractor' );

vc_map( array(
	'name'                      => esc_html__( 'Line Chart', 'tractor' ),
	'base'                      => 'tm_line_chart',
	'category'                  => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-accordion',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'X axis labels', 'tractor' ),
			'description' => esc_html__( 'List of labels for X axis (separate labels with ";").', 'tractor' ),
			'type'        => 'textfield',
			'param_name'  => 'labels',
			'std'         => 'Jul; Aug; Sep; Oct; Nov; Dec',
		),
		array(
			'heading'    => esc_html__( 'Datasets', 'tractor' ),
			'type'       => 'param_group',
			'param_name' => 'datasets',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Title', 'tractor' ),
					'description' => esc_html__( 'Dataset title used in tooltips and legends.', 'tractor' ),
					'type'        => 'textfield',
					'param_name'  => 'title',
					'admin_label' => true,
				),
				array(
					'heading'     => esc_html__( 'Values', 'tractor' ),
					'description' => esc_html__( 'text format for the tooltip (available placeholders: {d} dataset title, {x} X axis label, {y} Y axis value)', 'tractor' ),
					'type'        => 'textfield',
					'param_name'  => 'values',
				),
				array(
					'heading'    => esc_html__( 'Dataset Color', 'tractor' ),
					'type'       => 'colorpicker',
					'param_name' => 'color',
				),
				array(
					'heading'     => esc_html__( 'Area filling', 'tractor' ),
					'description' => esc_html__( 'How to fill the area below the line', 'tractor' ),
					'type'        => 'dropdown',
					'param_name'  => 'fill',
					'value'       => array(
						esc_html__( 'Custom', 'tractor' ) => 'custom',
						esc_html__( 'None', 'tractor' )   => 'none',
					),
					'std'         => 'none',
				),
				array(
					'heading'    => esc_html__( 'Fill Color', 'tractor' ),
					'type'       => 'colorpicker',
					'param_name' => 'fill_color',
					'dependency' => array(
						'element' => 'fill',
						'value'   => array( 'custom' ),
					),
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'point_style',
					'heading'    => esc_html__( 'Point Style', 'tractor' ),
					'value'      => array(
						esc_html__( 'none', 'tractor' )              => 'none',
						esc_html__( 'circle', 'tractor' )            => 'circle',
						esc_html__( 'triangle', 'tractor' )          => 'triangle',
						esc_html__( 'rectangle', 'tractor' )         => 'rect',
						esc_html__( 'rotated rectangle', 'tractor' ) => 'rectRot',
						esc_html__( 'cross', 'tractor' )             => 'cross',
						esc_html__( 'rotated cross', 'tractor' )     => 'crossRot',
						esc_html__( 'star', 'tractor' )              => 'star',
					),
					'std'        => 'circle',
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'line_type',
					'heading'    => esc_html__( 'Line type', 'tractor' ),
					'value'      => array(
						esc_html__( 'normal', 'tractor' )  => 'normal',
						esc_html__( 'stepped', 'tractor' ) => 'step',
					),
					'std'        => 'normal',
				),
				array(
					'type'       => 'dropdown',
					'param_name' => 'line_style',
					'heading'    => esc_html__( 'Line style', 'tractor' ),
					'value'      => array(
						esc_html__( 'solid', 'tractor' )  => 'solid',
						esc_html__( 'dashed', 'tractor' ) => 'dashed',
						esc_html__( 'dotted', 'tractor' ) => 'dotted',
					),
					'std'        => 'solid',
				),
				array(
					'heading'     => esc_html__( 'Thickness', 'tractor' ),
					'description' => esc_html__( 'line and points thickness', 'tractor' ),
					'type'        => 'dropdown',
					'param_name'  => 'thickness',
					'value'       => array(
						esc_html__( 'thin', 'tractor' )    => 'thin',
						esc_html__( 'normal', 'tractor' )  => 'normal',
						esc_html__( 'thick', 'tractor' )   => 'thick',
						esc_html__( 'thicker', 'tractor' ) => 'thicker',
					),
					'std'         => 'normal',
				),
				array(
					'heading'     => esc_html__( 'Line tension', 'tractor' ),
					'description' => esc_html__( 'tension of the line ( 100 for a straight line )', 'tractor' ),
					'type'        => 'number',
					'param_name'  => 'line_tension',
					'std'         => 10,
					'min'         => 0,
					'max'         => 100,
					'step'        => 1,
				),
			),
			'value'      => rawurlencode( wp_json_encode( array(
				array(
					'title'        => esc_html__( 'Item 01', 'tractor' ),
					'values'       => '15; 10; 22; 19; 23; 17',
					'color'        => 'rgba(105, 59, 255, 0.55)',
					'fill'         => 'none',
					'thickness'    => 'normal',
					'point_style'  => 'circle',
					'line_style'   => 'solid',
					'line_tension' => 10,

				),
				array(
					'title'        => esc_html__( 'Item 02', 'tractor' ),
					'values'       => '34; 38; 35; 33; 37; 40',
					'color'        => 'rgba(0, 110, 253, 0.56)',
					'fill'         => 'none',
					'thickness'    => 'normal',
					'point_style'  => 'circle',
					'line_style'   => 'solid',
					'line_tension' => 10,
				),
			) ) ),
		),
		Tractor_VC::extra_class_field(),
		array(
			'group'      => $legend_tab,
			'heading'    => esc_html__( 'Enable legends', 'tractor' ),
			'type'       => 'checkbox',
			'param_name' => 'legend',
			'value'      => array(
				esc_html__( 'Yes', 'tractor' ) => '1',
			),
			'std'        => '1',
		),
		array(
			'group'      => $legend_tab,
			'heading'    => esc_html__( 'Legends Style', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'legend_style',
			'value'      => array(
				esc_html__( 'Normal', 'tractor' )          => 'normal',
				esc_html__( 'Use Point Style', 'tractor' ) => 'point',
			),
			'std'        => 'normal',
		),
		array(
			'group'      => $legend_tab,
			'heading'    => esc_html__( 'Legends Position', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'legend_position',
			'value'      => array(
				esc_html__( 'Top', 'tractor' )    => 'top',
				esc_html__( 'Right', 'tractor' )  => 'right',
				esc_html__( 'Bottom', 'tractor' ) => 'bottom',
				esc_html__( 'Left', 'tractor' )   => 'left',
			),
			'std'        => 'bottom',
		),
		array(
			'group'       => $legend_tab,
			'heading'     => esc_html__( 'Click on legends', 'tractor' ),
			'description' => esc_html__( 'Hide dataset on click on legend', 'tractor' ),
			'type'        => 'checkbox',
			'param_name'  => 'legend_onclick',
			'value'       => array(
				esc_html__( 'Yes', 'tractor' ) => '1',
			),
			'std'         => '1',
		),
		array(
			'group'      => esc_html__( 'Chart Options', 'tractor' ),
			'heading'    => esc_html__( 'Aspect Ratio', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'aspect_ratio',
			'value'      => array(
				'1:1'  => '1:1',
				'21:9' => '21:9',
				'16:9' => '16:9',
				'4:3'  => '4:3',
				'3:4'  => '3:4',
				'9:16' => '9:16',
				'9:21' => '9:21',
			),
			'std'        => '4:3',
		),
	), Tractor_VC::get_vc_spacing_tab() ),
) );
