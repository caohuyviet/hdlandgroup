<?php
vc_update_shortcode_param( 'vc_tta_tour', array(
	'param_name' => 'style',
	'value'      => array(
		esc_html__( 'Tractor 01', 'tractor' ) => 'tractor-tour-01',
		esc_html__( 'Tractor 02', 'tractor' ) => 'tractor-tour-02',
		esc_html__( 'Tractor 03', 'tractor' ) => 'tractor-tour-03',
		esc_html__( 'Classic', 'tractor' )    => 'classic',
		esc_html__( 'Modern', 'tractor' )     => 'modern',
		esc_html__( 'Flat', 'tractor' )       => 'flat',
		esc_html__( 'Outline', 'tractor' )    => 'outline',
	),
) );
