<?php

class WPBakeryShortCode_TM_Social_Networks extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $tractor_shortcode_lg_css;
		global $tractor_shortcode_md_css;
		global $tractor_shortcode_sm_css;
		global $tractor_shortcode_xs_css;

		$tmp = $link_css = $link_hover_css = $icon_css = $icon_hover_css = $text_css = $text_hover_css = '';
		extract( $atts );

		$icon_css .= Tractor_Helper::get_shortcode_css_color_inherit( 'color', $atts['icon_color'], $atts['custom_icon_color'] );
		$icon_hover_css .= Tractor_Helper::get_shortcode_css_color_inherit( 'color', $atts['icon_hover_color'], $atts['custom_icon_hover_color'] );
		$text_css .= Tractor_Helper::get_shortcode_css_color_inherit( 'color', $atts['text_color'], $atts['custom_text_color'] );
		$text_hover_css .= Tractor_Helper::get_shortcode_css_color_inherit( 'color', $atts['text_hover_color'], $atts['custom_text_hover_color'] );
		$link_css .= Tractor_Helper::get_shortcode_css_color_inherit( 'border-color', $atts['border_color'], $atts['custom_border_color'] );
		$link_hover_css .= Tractor_Helper::get_shortcode_css_color_inherit( 'border-color', $atts['border_hover_color'], $atts['custom_border_hover_color'] );
		$link_css .= Tractor_Helper::get_shortcode_css_color_inherit( 'background-color', $atts['background_color'], $atts['custom_background_color'] );
		$link_hover_css .= Tractor_Helper::get_shortcode_css_color_inherit( 'background-color', $atts['background_hover_color'], $atts['custom_background_hover_color'] );

		if ( $atts['align'] !== '' ) {
			$tmp .= "text-align: {$atts['align']};";
		}

		if ( $atts['md_align'] !== '' ) {
			$tractor_shortcode_md_css .= "$selector { text-align: {$atts['md_align']} }";
		}

		if ( $atts['sm_align'] !== '' ) {
			$tractor_shortcode_sm_css .= "$selector { text-align: {$atts['sm_align']} }";
		}

		if ( $atts['xs_align'] !== '' ) {
			$tractor_shortcode_xs_css .= "$selector { text-align: {$atts['xs_align']} }";
		}

		if ( $tmp !== '' ) {
			$tractor_shortcode_lg_css .= "$selector { $tmp }";
		}

		if ( $icon_css !== '' ) {
			$tractor_shortcode_lg_css .= "$selector .link-icon { $icon_css }";
		}

		if ( $icon_hover_css !== '' ) {
			$tractor_shortcode_lg_css .= "$selector .item:hover .link-icon { $icon_hover_css }";
		}

		if ( $text_css !== '' ) {
			$tractor_shortcode_lg_css .= "$selector .link-text { $text_css }";
		}

		if ( $text_hover_css !== '' ) {
			$tractor_shortcode_lg_css .= "$selector .item:hover .link-text { $text_hover_css }";
		}

		if ( $link_css !== '' ) {
			$tractor_shortcode_lg_css .= "$selector .link { $link_css }";
		}

		if ( $link_hover_css !== '' ) {
			$tractor_shortcode_lg_css .= "$selector .item:hover .link { $link_hover_css }";
		}
	}
}

$color_tab = esc_html__( 'Color', 'tractor' );

vc_map( array(
	'name'                      => esc_html__( 'Social Networks', 'tractor' ),
	'base'                      => 'tm_social_networks',
	'category'                  => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-social-networks',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Icons', 'tractor' )                 => 'icons',
				esc_html__( 'Large Icons', 'tractor' )           => 'large-icons',
				esc_html__( 'Solid Square Icon', 'tractor' )     => 'solid-square-icon',
				esc_html__( 'Solid Rounded Icon', 'tractor' )    => 'solid-rounded-icon',
				esc_html__( 'Solid Rounded Icon 02', 'tractor' ) => 'solid-rounded-icon-02',
				esc_html__( 'Title', 'tractor' )                 => 'title',
				esc_html__( 'Icon + Title', 'tractor' )          => 'icon-title',
				esc_html__( 'Rounded Icon + Title', 'tractor' )  => 'rounded-icon-title',
			),
			'std'         => 'icons',
		),
		array(
			'heading'     => esc_html__( 'Layout', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'layout',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Inline', 'tractor' )    => 'inline',
				esc_html__( 'List', 'tractor' )      => 'list',
				esc_html__( '2 Columns', 'tractor' ) => 'two-columns',
			),
			'std'         => 'inline',
		),

	), Tractor_VC::get_alignment_fields(), array(
		array(
			'heading'    => esc_html__( 'Open link in a new tab', 'tractor' ),
			'type'       => 'checkbox',
			'param_name' => 'target',
			'value'      => array(
				esc_html__( 'Yes', 'tractor' ) => '1',
			),
			'std'        => '1',
		),
		array(
			'heading'    => esc_html__( 'Show tooltip as item title', 'tractor' ),
			'type'       => 'checkbox',
			'param_name' => 'tooltip_enable',
			'value'      => array(
				esc_html__( 'Yes', 'tractor' ) => '1',
			),
		),
		array(
			'heading'    => esc_html__( 'Tooltip position', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'tooltip_position',
			'value'      => array(
				esc_html__( 'Top', 'tractor' )    => 'top',
				esc_html__( 'Right', 'tractor' )  => 'right',
				esc_html__( 'Bottom', 'tractor' ) => 'bottom',
				esc_html__( 'Left', 'tractor' )   => 'left',
			),
			'std'        => 'top',
			'dependency' => array(
				'element' => 'tooltip_enable',
				'value'   => '1',
			),
		),
		array(
			'heading'    => esc_html__( 'Tooltip skin', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'tooltip_skin',
			'value'      => array(
				esc_html__( 'Default', 'tractor' ) => '',
				esc_html__( 'Primary', 'tractor' ) => 'primary',
			),
			'std'        => '',
			'dependency' => array(
				'element' => 'tooltip_enable',
				'value'   => '1',
			),
		),
		Tractor_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Items', 'tractor' ),
			'heading'    => esc_html__( 'Items', 'tractor' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array_merge( array(
				array(
					'heading'     => esc_html__( 'Title', 'tractor' ),
					'type'        => 'textfield',
					'param_name'  => 'title',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Link', 'tractor' ),
					'type'       => 'textfield',
					'param_name' => 'link',
				),
			), Tractor_VC::icon_libraries_no_depend() ),

			'value' => rawurlencode( wp_json_encode( array(
				array(
					'title'     => esc_html__( 'Twitter', 'tractor' ),
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-twitter',
				),
				array(
					'title'     => esc_html__( 'Facebook', 'tractor' ),
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-facebook',
				),
				array(
					'title'     => esc_html__( 'Vimeo', 'tractor' ),
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-vimeo',
				),
				array(
					'title'     => esc_html__( 'Linkedin', 'tractor' ),
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-linkedin',
				),
				array(
					'title'     => esc_html__( 'Pinterest', 'tractor' ),
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-pinterest',
				),
				array(
					'title'     => esc_html__( 'Google Plus', 'tractor' ),
					'link'      => '#',
					'icon_type' => 'ion',
					'icon_ion'  => 'ion-social-googleplus',
				),
			) ) ),

		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Icon color', 'tractor' ),
			'type'             => 'dropdown',
			'param_name'       => 'icon_color',
			'value'            => array(
				esc_html__( 'Default color', 'tractor' )   => '',
				esc_html__( 'Primary color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom color', 'tractor' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Custom icon color', 'tractor' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_icon_color',
			'dependency'       => array(
				'element' => 'icon_color',
				'value'   => 'custom',
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Icon hover color', 'tractor' ),
			'type'             => 'dropdown',
			'param_name'       => 'icon_hover_color',
			'value'            => array(
				esc_html__( 'Default color', 'tractor' )   => '',
				esc_html__( 'Primary color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom color', 'tractor' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Custom icon hover color', 'tractor' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_icon_hover_color',
			'dependency'       => array(
				'element' => 'icon_hover_color',
				'value'   => 'custom',
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Text color', 'tractor' ),
			'type'             => 'dropdown',
			'param_name'       => 'text_color',
			'value'            => array(
				esc_html__( 'Default color', 'tractor' )   => '',
				esc_html__( 'Primary color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom color', 'tractor' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Custom text color', 'tractor' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_text_color',
			'dependency'       => array(
				'element' => 'text_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Text hover color', 'tractor' ),
			'type'             => 'dropdown',
			'param_name'       => 'text_hover_color',
			'value'            => array(
				esc_html__( 'Default color', 'tractor' )   => '',
				esc_html__( 'Primary color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom color', 'tractor' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Custom text hover color', 'tractor' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_text_hover_color',
			'dependency'       => array(
				'element' => 'text_hover_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Border color', 'tractor' ),
			'type'             => 'dropdown',
			'param_name'       => 'border_color',
			'value'            => array(
				esc_html__( 'Default color', 'tractor' )   => '',
				esc_html__( 'Primary color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom color', 'tractor' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Custom border color', 'tractor' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_border_color',
			'dependency'       => array(
				'element' => 'border_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Border hover color', 'tractor' ),
			'type'             => 'dropdown',
			'param_name'       => 'border_hover_color',
			'value'            => array(
				esc_html__( 'Default color', 'tractor' )   => '',
				esc_html__( 'Primary color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom color', 'tractor' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Custom border hover color', 'tractor' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_border_hover_color',
			'dependency'       => array(
				'element' => 'border_hover_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Background color', 'tractor' ),
			'type'             => 'dropdown',
			'param_name'       => 'background_color',
			'value'            => array(
				esc_html__( 'Default color', 'tractor' )   => '',
				esc_html__( 'Primary color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom color', 'tractor' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Custom background color', 'tractor' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_background_color',
			'dependency'       => array(
				'element' => 'background_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Background hover color', 'tractor' ),
			'type'             => 'dropdown',
			'param_name'       => 'background_hover_color',
			'value'            => array(
				esc_html__( 'Default color', 'tractor' )   => '',
				esc_html__( 'Primary color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom color', 'tractor' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $color_tab,
			'heading'          => esc_html__( 'Custom background hover color', 'tractor' ),
			'type'             => 'colorpicker',
			'param_name'       => 'custom_background_hover_color',
			'dependency'       => array(
				'element' => 'background_hover_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
	) ),
) );
