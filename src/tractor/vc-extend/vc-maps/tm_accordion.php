<?php

class WPBakeryShortCode_TM_Accordion extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		Tractor_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'                      => esc_html__( 'Accordion', 'tractor' ),
	'base'                      => 'tm_accordion',
	'category'                  => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-accordion',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Style 01', 'tractor' ) => '1',
				esc_html__( 'Style 02', 'tractor' ) => '2',
				esc_html__( 'Style 03', 'tractor' ) => '3',
			),
			'std'         => '1',
		),
		array(
			'heading'    => esc_html__( 'Multi Open', 'tractor' ),
			'type'       => 'checkbox',
			'param_name' => 'multi_open',
			'value'      => array( esc_html__( 'Yes', 'tractor' ) => '1' ),
		),
		Tractor_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Items', 'tractor' ),
			'heading'    => esc_html__( 'Items', 'tractor' ),
			'type'       => 'param_group',
			'param_name' => 'items',
			'params'     => array(
				array(
					'heading'     => esc_html__( 'Title', 'tractor' ),
					'type'        => 'textfield',
					'param_name'  => 'title',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Content', 'tractor' ),
					'type'       => 'textarea',
					'param_name' => 'content',
				),
			),
		),
	), Tractor_VC::get_vc_spacing_tab() ),
) );
