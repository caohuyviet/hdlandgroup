<?php

class WPBakeryShortCode_TM_Attribute_List extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		Tractor_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'     => esc_html__( 'Attribute List', 'tractor' ),
	'base'     => 'tm_attribute_list',
	'category' => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'     => 'insight-i insight-i-grid',
	'params'   => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Style 01', 'tractor' ) => '01',
			),
			'std'         => '01',
		),
		Tractor_VC::extra_class_field(),
		array(
			'group'      => esc_html__( 'Attributes', 'tractor' ),
			'heading'    => esc_html__( 'Attributes', 'tractor' ),
			'type'       => 'param_group',
			'param_name' => 'attributes',
			'params'     => array(
				array(
					'type'        => 'iconpicker',
					'heading'     => esc_html__( 'Tractor', 'tractor' ),
					'param_name'  => 'icon_tractor',
					'value'       => 'tractor-icon-blueprint',
					'settings'    => array(
						'emptyIcon'    => true,
						'type'         => 'tractor',
						'iconsPerPage' => 400,
					),
					'description' => esc_html__( 'Select icon from library.', 'tractor' ),
				),
				array(
					'heading'     => esc_html__( 'Name', 'tractor' ),
					'type'        => 'textfield',
					'param_name'  => 'name',
					'admin_label' => true,
				),
				array(
					'heading'    => esc_html__( 'Value', 'tractor' ),
					'type'       => 'textarea',
					'param_name' => 'value',
				),
			),
		),
	), Tractor_VC::get_vc_spacing_tab() ),
) );

