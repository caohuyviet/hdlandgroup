<?php

class WPBakeryShortCode_TM_Grid extends WPBakeryShortCodesContainer {

	public function get_inline_css( $selector = '', $atts ) {
		global $tractor_shortcode_lg_css;

		Tractor_VC::get_grid_css( $selector, $atts );

		if ( isset( $atts['item_max_width'] ) && $atts['item_max_width'] !== '' ) {
			Tractor_VC::get_responsive_css( array(
				'element' => "$selector .grid-item",
				'atts'    => array(
					'max-width' => array(
						'media_str' => $atts['item_max_width'],
						'unit'      => 'px',
					),
				),
			) );

			$tractor_shortcode_lg_css .= "$selector .grid-item { margin-left: auto; margin-right: auto; }";
		}

		Tractor_VC::get_vc_spacing_css( $selector, $atts );
	}
}

vc_map( array(
	'name'            => esc_html__( 'Grid', 'tractor' ),
	'base'            => 'tm_grid',
	'category'        => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'            => 'insight-i insight-i-grid',
	'as_parent'       => array( 'only' => array( 'tm_box_icon', 'tm_card' ) ),
	'content_element' => true,
	'is_container'    => true,
	'js_view'         => 'VcColumnView',
	'params'          => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'None', 'tractor' )                => '',
				esc_html__( 'With separator', 'tractor' )      => 'border',
				esc_html__( 'With Light Border', 'tractor' )   => 'light-border',
				esc_html__( 'With Dark Border', 'tractor' )    => 'dark-border',
				esc_html__( 'Rounded With Border', 'tractor' ) => 'rounded-separator',
			),
			'std'         => '',
		),
		array(
			'heading'     => esc_html__( 'Columns', 'tractor' ),
			'type'        => 'number_responsive',
			'param_name'  => 'columns',
			'min'         => 1,
			'max'         => 6,
			'step'        => 1,
			'suffix'      => '',
			'media_query' => array(
				'lg' => '4',
				'md' => '3',
				'sm' => '2',
				'xs' => '1',
			),
		),
		array(
			'heading'     => esc_html__( 'Columns Gutter', 'tractor' ),
			'description' => esc_html__( 'Controls the gutter of grid columns.', 'tractor' ),
			'type'        => 'number',
			'param_name'  => 'gutter',
			'std'         => 30,
			'min'         => 0,
			'max'         => 100,
			'step'        => 1,
			'suffix'      => 'px',
		),
		array(
			'heading'     => esc_html__( 'Rows Gutter', 'tractor' ),
			'description' => esc_html__( 'Controls the gutter of grid rows.', 'tractor' ),
			'type'        => 'number',
			'param_name'  => 'row_gutter',
			'std'         => 30,
			'min'         => 0,
			'max'         => 100,
			'step'        => 1,
			'suffix'      => 'px',
		),
		array(
			'heading'     => esc_html__( 'Grid Items Max Width', 'tractor' ),
			'description' => esc_html__( 'Controls the max width of items, and centered.', 'tractor' ),
			'type'        => 'number_responsive',
			'param_name'  => 'item_max_width',
			'min'         => 1,
			'max'         => 1000,
			'step'        => 1,
			'suffix'      => 'px',
			'media_query' => array(
				'lg' => '',
				'md' => '',
				'sm' => '',
				'xs' => '',
			),
		),
		Tractor_VC::get_animation_field( array(
			'std' => 'move-up',
		) ),
		Tractor_VC::extra_class_field(),
	), Tractor_VC::get_vc_spacing_tab() ),
) );

