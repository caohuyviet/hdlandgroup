<?php

class WPBakeryShortCode_TM_Box_Icon extends WPBakeryShortCode {

	public function get_inline_css( $selector = '', $atts ) {
		global $tractor_shortcode_lg_css;
		global $tractor_shortcode_md_css;
		global $tractor_shortcode_sm_css;
		global $tractor_shortcode_xs_css;
		$btn_tmp = '';

		$tmp      = Tractor_Helper::get_shortcode_css_color_inherit( 'background-color', $atts['background_color'], $atts['custom_background_color'] );
		$icon_tmp = Tractor_Helper::get_shortcode_css_color_inherit( 'color', $atts['icon_color'], $atts['custom_icon_color'] );
		$icon_tmp .= Tractor_Helper::get_shortcode_css_color_inherit( 'border-color', $atts['icon_color'], $atts['custom_icon_color'] );
		$heading_tmp = Tractor_Helper::get_shortcode_css_color_inherit( 'color', $atts['heading_color'], $atts['custom_heading_color'] );
		$text_tmp    = Tractor_Helper::get_shortcode_css_color_inherit( 'color', $atts['text_color'], $atts['custom_text_color'] );
		$line_tmp    = Tractor_Helper::get_shortcode_css_color_inherit( 'background-color', $atts['heading_color'], $atts['custom_heading_color'] );

		if ( $atts['background_image'] !== '' ) {
			$_url = wp_get_attachment_image_url( $atts['background_image'], 'full' );
			if ( $_url !== false ) {
				$tmp .= "background-image: url( $_url );";

				if ( $atts['background_size'] !== 'auto' ) {
					$tmp .= "background-size: {$atts['background_size']};";
				}

				$tmp .= "background-repeat: {$atts['background_repeat']};";
				if ( $atts['background_position'] !== '' ) {
					$tmp .= "background-position: {$atts['background_position']};";
				}
			}
		}

		if ( $tmp !== '' ) {
			$tractor_shortcode_lg_css .= "$selector{ $tmp }";
		}

		if ( isset( $atts['icon_font_size'] ) ) {
			Tractor_VC::get_responsive_css( array(
				'element' => "$selector .icon",
				'atts'    => array(
					'font-size' => array(
						'media_str' => $atts['icon_font_size'],
						'unit'      => 'px',
					),
				),
			) );
		}

		if ( $atts['text_width'] !== '' ) {
			$text_tmp .= "max-width: {$atts['text_width']};";
		}

		if ( $icon_tmp !== '' ) {
			$tractor_shortcode_lg_css .= "$selector .icon{ $icon_tmp }";
		}

		if ( $heading_tmp !== '' ) {
			$tractor_shortcode_lg_css .= "$selector .heading { $heading_tmp }";
		}

		if ( $line_tmp !== '' ) {
			$tractor_shortcode_lg_css .= "$selector .icon:before { $line_tmp }";
		}

		if ( $text_tmp !== '' ) {
			$tractor_shortcode_lg_css .= "$selector .text{ $text_tmp }";
		}

		if ( $atts['text'] === '' && $atts['heading'] === '' ) {
			$tractor_shortcode_lg_css .= "$selector .image { margin-bottom: 0; }";
		}

		if ( $atts['button_color'] === 'custom' ) {
			$btn_tmp .= "color: {$atts['custom_button_color']}; ";
		}

		if ( $btn_tmp !== '' ) {
			$tractor_shortcode_lg_css .= "$selector .tm-button .button-text{ $btn_tmp }";
		}

		$tmp = "text-align: {$atts['align']}; ";
		if ( $atts['align'] === 'left' ) {
			$tmp .= 'align-items: flex-start';
		} elseif ( $atts['align'] === 'center' ) {
			$tmp .= 'align-items: center;';
		} elseif ( $atts['align'] === 'right' ) {
			$tmp .= 'align-items: flex-end;';
		}

		$tractor_shortcode_lg_css .= "$selector .content-wrap { $tmp }";

		$tmp = '';
		if ( $atts['md_align'] !== '' ) {
			$tmp .= "text-align: {$atts['md_align']};";

			if ( $atts['md_align'] === 'left' ) {
				$tmp .= 'align-items: flex-start';
			} elseif ( $atts['md_align'] === 'center' ) {
				$tmp .= 'align-items: center;';
			} elseif ( $atts['md_align'] === 'right' ) {
				$tmp .= 'align-items: flex-end;';
			}

			$tractor_shortcode_md_css .= "$selector .content-wrap { $tmp }";
		}

		$tmp = '';
		if ( $atts['sm_align'] !== '' ) {
			$tmp .= "text-align: {$atts['sm_align']};";

			if ( $atts['sm_align'] === 'left' ) {
				$tmp .= 'align-items: flex-start';
			} elseif ( $atts['sm_align'] === 'center' ) {
				$tmp .= 'align-items: center;';
			} elseif ( $atts['sm_align'] === 'right' ) {
				$tmp .= 'align-items: flex-end;';
			}

			$tractor_shortcode_sm_css .= "$selector .content-wrap { $tmp }";
		}

		$tmp = '';
		if ( $atts['xs_align'] !== '' ) {
			$tmp .= "text-align: {$atts['xs_align']};";

			if ( $atts['xs_align'] === 'left' ) {
				$tmp .= 'align-items: flex-start';
			} elseif ( $atts['xs_align'] === 'center' ) {
				$tmp .= 'align-items: center;';
			} elseif ( $atts['xs_align'] === 'right' ) {
				$tmp .= 'align-items: flex-end;';
			}

			$tractor_shortcode_xs_css .= "$selector .content-wrap { $tmp }";
		}

		Tractor_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$content_tab = esc_html__( 'Content', 'tractor' );
$styling_tab = esc_html__( 'Styling', 'tractor' );

vc_map( array(
	'name'                      => esc_html__( 'Box Icon', 'tractor' ),
	'base'                      => 'tm_box_icon',
	'category'                  => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-icons',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'tractor' ),
			'description' => esc_html__( 'Select style for box icon.', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'value'       => array(
				esc_html__( 'Style 01', 'tractor' ) => '1',
				esc_html__( 'Style 02', 'tractor' ) => '2',
				esc_html__( 'Style 03', 'tractor' ) => '3',
				esc_html__( 'Style 04', 'tractor' ) => '4',
				esc_html__( 'Style 05', 'tractor' ) => '5',
				esc_html__( 'Style 06', 'tractor' ) => '6',
				esc_html__( 'Style 07', 'tractor' ) => '7',
				esc_html__( 'Style 08', 'tractor' ) => '8',
				esc_html__( 'Style 09', 'tractor' ) => '9',
				esc_html__( 'Style 10', 'tractor' ) => '10',
				esc_html__( 'Style 11', 'tractor' ) => '11',
			),
			'admin_label' => true,
			'std'         => '1',
		),
		array(
			'group'      => $content_tab,
			'heading'    => esc_html__( 'Image', 'tractor' ),
			'type'       => 'attach_image',
			'param_name' => 'image',
		),
	), Tractor_VC::get_alignment_fields(), Tractor_VC::icon_libraries( array(
		'group'      => $content_tab,
		'allow_none' => true,
		'allow_svg'  => true,
	) ), array(
		array(
			'group'       => $content_tab,
			'heading'     => esc_html__( 'Heading', 'tractor' ),
			'type'        => 'textfield',
			'param_name'  => 'heading',
			'admin_label' => true,
		),
		array(
			'group'       => $content_tab,
			'heading'     => esc_html__( 'Link', 'tractor' ),
			'description' => esc_html__( 'Add a link to heading.', 'tractor' ),
			'type'        => 'vc_link',
			'param_name'  => 'link',
		),
		array(
			'group'      => $content_tab,
			'heading'    => esc_html__( 'Text', 'tractor' ),
			'type'       => 'textarea',
			'param_name' => 'text',
		),
		array(
			'group'       => $content_tab,
			'heading'     => esc_html__( 'Text Width', 'tractor' ),
			'description' => esc_html__( 'Input width of box text, e.g 300px', 'tractor' ),
			'type'        => 'textfield',
			'param_name'  => 'text_width',
		),
		array(
			'group'      => $content_tab,
			'heading'    => esc_html__( 'Button', 'tractor' ),
			'type'       => 'vc_link',
			'param_name' => 'button',
		),
		Tractor_VC::get_animation_field(),
		Tractor_VC::extra_class_field(),
		array(
			'group'       => $styling_tab,
			'heading'     => esc_html__( 'Icon Font Size', 'tractor' ),
			'type'        => 'number_responsive',
			'param_name'  => 'icon_font_size',
			'min'         => 8,
			'suffix'      => 'px',
			'media_query' => array(
				'lg' => '',
				'md' => '',
				'sm' => '',
				'xs' => '',
			),
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Heading Color', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'heading_color',
			'value'      => array(
				esc_html__( 'Default Color', 'tractor' )   => '',
				esc_html__( 'Primary Color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary Color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom Color', 'tractor' )    => 'custom',
			),
			'std'        => '',
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Custom Heading Color', 'tractor' ),
			'type'       => 'colorpicker',
			'param_name' => 'custom_heading_color',
			'dependency' => array(
				'element' => 'heading_color',
				'value'   => array( 'custom' ),
			),
			'std'        => '#222',
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Icon Color', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'icon_color',
			'value'      => array(
				esc_html__( 'Default Color', 'tractor' )   => '',
				esc_html__( 'Primary Color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary Color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom Color', 'tractor' )    => 'custom',
			),
			'std'        => '',
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Custom Icon Color', 'tractor' ),
			'type'       => 'colorpicker',
			'param_name' => 'custom_icon_color',
			'dependency' => array(
				'element' => 'icon_color',
				'value'   => 'custom',
			),
			'std'        => '#999',
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Text Color', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'text_color',
			'value'      => array(
				esc_html__( 'Default Color', 'tractor' )   => '',
				esc_html__( 'Primary Color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary Color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom Color', 'tractor' )    => 'custom',
			),
			'std'        => '',
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Custom Text Color', 'tractor' ),
			'type'       => 'colorpicker',
			'param_name' => 'custom_text_color',
			'dependency' => array(
				'element' => 'text_color',
				'value'   => array( 'custom' ),
			),
			'std'        => '#999',
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Button Color', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'button_color',
			'value'      => array(
				esc_html__( 'Default Color', 'tractor' )   => '',
				esc_html__( 'Primary Color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary Color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom Color', 'tractor' )    => 'custom',
			),
			'std'        => '',
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Custom Button Color', 'tractor' ),
			'type'       => 'colorpicker',
			'param_name' => 'custom_button_color',
			'dependency' => array(
				'element' => 'button_color',
				'value'   => array( 'custom' ),
			),
			'std'        => '#fff',
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Background Color', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'background_color',
			'value'      => array(
				esc_html__( 'None', 'tractor' )            => '',
				esc_html__( 'Primary Color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary Color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom Color', 'tractor' )    => 'custom',
			),
			'std'        => '',
		),

		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Custom Background Color', 'tractor' ),
			'type'       => 'colorpicker',
			'param_name' => 'custom_background_color',
			'dependency' => array(
				'element' => 'background_color',
				'value'   => array( 'custom' ),
			),
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Background Image', 'tractor' ),
			'type'       => 'attach_image',
			'param_name' => 'background_image',
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Background Repeat', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'background_repeat',
			'value'      => array(
				esc_html__( 'No repeat', 'tractor' )         => 'no-repeat',
				esc_html__( 'Tile', 'tractor' )              => 'repeat',
				esc_html__( 'Tile Horizontally', 'tractor' ) => 'repeat-x',
				esc_html__( 'Tile Vertically', 'tractor' )   => 'repeat-y',
			),
			'std'        => 'no-repeat',
			'dependency' => array(
				'element'   => 'background_image',
				'not_empty' => true,
			),
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Background Size', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'background_size',
			'value'      => array(
				esc_html__( 'Auto', 'tractor' )    => 'auto',
				esc_html__( 'Cover', 'tractor' )   => 'cover',
				esc_html__( 'Contain', 'tractor' ) => 'contain',
			),
			'std'        => 'cover',
			'dependency' => array(
				'element'   => 'background_image',
				'not_empty' => true,
			),
		),
		array(
			'group'       => $styling_tab,
			'heading'     => esc_html__( 'Background Position', 'tractor' ),
			'description' => esc_html__( 'E.g left center', 'tractor' ),
			'type'        => 'textfield',
			'param_name'  => 'background_position',
			'dependency'  => array(
				'element'   => 'background_image',
				'not_empty' => true,
			),
		),
		array(
			'group'       => $styling_tab,
			'heading'     => esc_html__( 'Background Overlay', 'tractor' ),
			'description' => esc_html__( 'Choose an overlay background color.', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'overlay_background',
			'value'       => array(
				esc_html__( 'None', 'tractor' )          => '',
				esc_html__( 'Primary Color', 'tractor' ) => 'primary',
				esc_html__( 'Custom Color', 'tractor' )  => 'overlay_custom_background',
			),
		),
		array(
			'group'       => $styling_tab,
			'heading'     => esc_html__( 'Custom Background Overlay', 'tractor' ),
			'description' => esc_html__( 'Choose an custom background color overlay.', 'tractor' ),
			'type'        => 'colorpicker',
			'param_name'  => 'overlay_custom_background',
			'std'         => '#000000',
			'dependency'  => array(
				'element' => 'overlay_background',
				'value'   => array( 'overlay_custom_background' ),
			),
		),
		array(
			'group'      => $styling_tab,
			'heading'    => esc_html__( 'Opacity', 'tractor' ),
			'type'       => 'number',
			'param_name' => 'overlay_opacity',
			'value'      => 100,
			'min'        => 0,
			'max'        => 100,
			'step'       => 1,
			'suffix'     => '%',
			'std'        => 80,
			'dependency' => array(
				'element'   => 'overlay_background',
				'not_empty' => true,
			),
		),
	), Tractor_VC::get_vc_spacing_tab(), Tractor_VC::get_custom_style_tab() ),

) );
