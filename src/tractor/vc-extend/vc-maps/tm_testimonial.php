<?php

add_filter( 'vc_autocomplete_tm_testimonial_taxonomies_callback', array(
	'WPBakeryShortCode_TM_Testimonial',
	'autocomplete_taxonomies_field_search',
), 10, 1 );

add_filter( 'vc_autocomplete_tm_testimonial_taxonomies_render', array(
	Tractor_VC::instance(),
	'autocomplete_taxonomies_field_render',
), 10, 1 );

class WPBakeryShortCode_TM_Testimonial extends WPBakeryShortCode {

	/**
	 * @param $search_string
	 *
	 * @return array|bool
	 */
	public static function autocomplete_taxonomies_field_search( $search_string ) {
		$data = Tractor_VC::instance()->autocomplete_get_data_from_post_type( $search_string, 'testimonial' );

		return $data;
	}

	public function get_inline_css( $selector = '', $atts ) {
		global $tractor_shortcode_lg_css;

		$text_tmp    = Tractor_Helper::get_shortcode_css_color_inherit( 'color', $atts['text_color'], $atts['custom_text_color'] );
		$name_tmp    = Tractor_Helper::get_shortcode_css_color_inherit( 'color', $atts['name_color'], $atts['custom_name_color'] );
		$by_line_tmp = Tractor_Helper::get_shortcode_css_color_inherit( 'color', $atts['by_line_color'], $atts['custom_by_line_color'] );

		if ( $text_tmp !== '' ) {
			$tractor_shortcode_lg_css .= "$selector .testimonial-desc { $text_tmp }";
		}

		if ( $name_tmp !== '' ) {
			$tractor_shortcode_lg_css .= "$selector .testimonial-name { $name_tmp }";
		}

		if ( $by_line_tmp !== '' ) {
			$tractor_shortcode_lg_css .= "$selector .testimonial-by-line { $by_line_tmp }";
		}

		Tractor_VC::get_vc_spacing_css( $selector, $atts );
	}
}

$carousel_tab  = esc_html__( 'Carousel Settings', 'tractor' );
$styling_group = esc_html__( 'Styling', 'tractor' );

vc_map( array(
	'name'                      => esc_html__( 'Testimonials', 'tractor' ),
	'base'                      => 'tm_testimonial',
	'category'                  => TRACTOR_VC_SHORTCODE_CATEGORY,
	'icon'                      => 'insight-i insight-i-testimonials',
	'allowed_container_element' => 'vc_row',
	'params'                    => array_merge( array(
		array(
			'heading'     => esc_html__( 'Style', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'style',
			'admin_label' => true,
			'value'       => array(
				esc_html__( 'Style 01', 'tractor' ) => '1',
				esc_html__( 'Style 02', 'tractor' ) => '2',
				esc_html__( 'Style 03', 'tractor' ) => '3',
				esc_html__( 'Style 04', 'tractor' ) => '4',
				esc_html__( 'Style 05', 'tractor' ) => '5',
				esc_html__( 'Style 06', 'tractor' ) => '6',
				esc_html__( 'Style 07', 'tractor' ) => '7',
				esc_html__( 'Style 08', 'tractor' ) => '8',
				esc_html__( 'Style 09', 'tractor' ) => '9',
			),
			'std'         => '1',
		),
		Tractor_VC::extra_class_field(),
		array(
			'group'       => esc_html__( 'Data Settings', 'tractor' ),
			'heading'     => esc_html__( 'Number', 'tractor' ),
			'description' => esc_html__( 'Number of items to show.', 'tractor' ),
			'type'        => 'number',
			'param_name'  => 'number',
			'std'         => 9,
			'min'         => 1,
			'max'         => 100,
			'step'        => 1,
		),
		array(
			'group'              => esc_html__( 'Data Settings', 'tractor' ),
			'heading'            => esc_html__( 'Narrow data source', 'tractor' ),
			'description'        => esc_html__( 'Enter categories, tags or custom taxonomies.', 'tractor' ),
			'type'               => 'autocomplete',
			'param_name'         => 'taxonomies',
			'settings'           => array(
				'multiple'       => true,
				'min_length'     => 1,
				'groups'         => true,
				// In UI show results grouped by groups, default false.
				'unique_values'  => true,
				// In UI show results except selected. NB! You should manually check values in backend, default false.
				'display_inline' => true,
				// In UI show results inline view, default false (each value in own line).
				'delay'          => 500,
				// delay for search. default 500.
				'auto_focus'     => true,
				// auto focus input, default true.
			),
			'param_holder_class' => 'vc_not-for-custom',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'tractor' ),
			'heading'     => esc_html__( 'Order by', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'orderby',
			'value'       => array(
				esc_html__( 'Date', 'tractor' )                  => 'date',
				esc_html__( 'Post ID', 'tractor' )               => 'ID',
				esc_html__( 'Author', 'tractor' )                => 'author',
				esc_html__( 'Title', 'tractor' )                 => 'title',
				esc_html__( 'Last modified date', 'tractor' )    => 'modified',
				esc_html__( 'Post/page parent ID', 'tractor' )   => 'parent',
				esc_html__( 'Number of comments', 'tractor' )    => 'comment_count',
				esc_html__( 'Menu order/Page Order', 'tractor' ) => 'menu_order',
				esc_html__( 'Meta value', 'tractor' )            => 'meta_value',
				esc_html__( 'Meta value number', 'tractor' )     => 'meta_value_num',
				esc_html__( 'Random order', 'tractor' )          => 'rand',
			),
			'description' => esc_html__( 'Select order type. If "Meta value" or "Meta value Number" is chosen then meta key is required.', 'tractor' ),
			'std'         => 'date',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'tractor' ),
			'heading'     => esc_html__( 'Sort order', 'tractor' ),
			'type'        => 'dropdown',
			'param_name'  => 'order',
			'value'       => array(
				esc_html__( 'Descending', 'tractor' ) => 'DESC',
				esc_html__( 'Ascending', 'tractor' )  => 'ASC',
			),
			'description' => esc_html__( 'Select sorting order.', 'tractor' ),
			'std'         => 'DESC',
		),
		array(
			'group'       => esc_html__( 'Data Settings', 'tractor' ),
			'heading'     => esc_html__( 'Meta key', 'tractor' ),
			'description' => esc_html__( 'Input meta key for grid ordering.', 'tractor' ),
			'type'        => 'textfield',
			'param_name'  => 'meta_key',
			'dependency'  => array(
				'element' => 'orderby',
				'value'   => array(
					'meta_value',
					'meta_value_num',
				),
			),
		),
		array(
			'group'            => $styling_group,
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'Text Color', 'tractor' ),
			'param_name'       => 'text_color',
			'value'            => array(
				esc_html__( 'Default Color', 'tractor' )   => '',
				esc_html__( 'Primary Color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary Color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom Color', 'tractor' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_group,
			'type'             => 'colorpicker',
			'heading'          => esc_html__( 'Custom Text Color', 'tractor' ),
			'param_name'       => 'custom_text_color',
			'dependency'       => array(
				'element' => 'text_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_group,
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'Name Color', 'tractor' ),
			'param_name'       => 'name_color',
			'value'            => array(
				esc_html__( 'Default Color', 'tractor' )   => '',
				esc_html__( 'Primary Color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary Color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom Color', 'tractor' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_group,
			'type'             => 'colorpicker',
			'heading'          => esc_html__( 'Custom Name Color', 'tractor' ),
			'param_name'       => 'custom_name_color',
			'dependency'       => array(
				'element' => 'name_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'group'            => $styling_group,
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'By Line Color', 'tractor' ),
			'param_name'       => 'by_line_color',
			'value'            => array(
				esc_html__( 'Default Color', 'tractor' )   => '',
				esc_html__( 'Primary Color', 'tractor' )   => 'primary',
				esc_html__( 'Secondary Color', 'tractor' ) => 'secondary',
				esc_html__( 'Custom Color', 'tractor' )    => 'custom',
			),
			'std'              => '',
			'edit_field_class' => 'vc_col-sm-6 col-break',
		),
		array(
			'group'            => $styling_group,
			'type'             => 'colorpicker',
			'heading'          => esc_html__( 'Custom By Line Color', 'tractor' ),
			'param_name'       => 'custom_by_line_color',
			'dependency'       => array(
				'element' => 'by_line_color',
				'value'   => array( 'custom' ),
			),
			'std'              => '#fff',
			'edit_field_class' => 'vc_col-sm-6',
		),
		array(
			'heading'    => esc_html__( 'Loop', 'tractor' ),
			'group'      => $carousel_tab,
			'type'       => 'checkbox',
			'param_name' => 'loop',
			'value'      => array( esc_html__( 'Yes', 'tractor' ) => '1' ),
			'std'        => '1',
		),
		array(
			'heading'    => esc_html__( 'Centered', 'tractor' ),
			'group'      => $carousel_tab,
			'type'       => 'checkbox',
			'param_name' => 'centered',
			'value'      => array( esc_html__( 'Yes', 'tractor' ) => '1' ),
			'std'        => '',
		),
		array(
			'group'       => $carousel_tab,
			'heading'     => esc_html__( 'Auto Play', 'tractor' ),
			'description' => esc_html__( 'Delay between transitions (in ms), e.g 3000. Leave blank to disabled.', 'tractor' ),
			'type'        => 'number',
			'suffix'      => 'ms',
			'param_name'  => 'auto_play',
			'std'         => 5000,
		),
		array(
			'group'      => $carousel_tab,
			'heading'    => esc_html__( 'Navigation', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'nav',
			'value'      => Tractor_VC::get_slider_navs(),
			'std'        => '',
		),
		Tractor_VC::extra_id_field( array(
			'group'      => $carousel_tab,
			'heading'    => esc_html__( 'Slider Button ID', 'tractor' ),
			'param_name' => 'slider_button_id',
			'dependency' => array(
				'element' => 'nav',
				'value'   => array(
					'custom',
				),
			),
		) ),
		array(
			'group'      => $carousel_tab,
			'heading'    => esc_html__( 'Pagination', 'tractor' ),
			'type'       => 'dropdown',
			'param_name' => 'pagination',
			'value'      => Tractor_VC::get_slider_dots(),
			'std'        => '',
		),
		array(
			'group'       => $carousel_tab,
			'heading'     => esc_html__( 'Gutter', 'tractor' ),
			'type'        => 'number_responsive',
			'param_name'  => 'carousel_gutter',
			'min'         => 0,
			'step'        => 1,
			'suffix'      => 'px',
			'media_query' => array(
				'lg' => 30,
				'md' => '',
				'sm' => '',
				'xs' => '',
			),
		),
		array(
			'group'       => $carousel_tab,
			'heading'     => esc_html__( 'Items Display', 'tractor' ),
			'type'        => 'number_responsive',
			'param_name'  => 'carousel_items_display',
			'min'         => 1,
			'max'         => 10,
			'suffix'      => 'item (s)',
			'media_query' => array(
				'lg' => 1,
				'md' => '',
				'sm' => '',
				'xs' => 1,
			),
			'dependency'  => array(
				'element'            => 'style',
				'value_not_equal_to' => array(
					'4',
				),
			),
		),
	), Tractor_VC::get_vc_spacing_tab() ),
) );
