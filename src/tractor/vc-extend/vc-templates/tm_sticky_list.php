<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
$list_style = $_global_icon = $_global_icon_class = $marker_color = $custom_marker_color = $title_color = $custom_title_color = $desc_color = $custom_desc_color = $animation = '';
$atts       = vc_map_get_attributes( $this->getShortcode(), $atts );

extract( $atts );
$css_id = uniqid( 'tm-sticky-list-' );
$this->get_inline_css( '#' . $css_id, $atts );
$items = (array) vc_param_group_parse_atts( $items );

if ( count( $items ) < 1 ) {
	return;
}
$el_class  = $this->getExtraClass( $el_class );
$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'tm-sticky-list ' . $el_class, $this->settings['base'], $atts );

$css_class .= Tractor_Helper::get_animation_classes( $animation );
?>


<ul class="<?php echo esc_attr( trim( $css_class ) ); ?>" id="<?php echo esc_attr( $css_id ); ?>">
	<?php
	foreach ( $items as $item ) {
		?>
		<li class="list-item">
			<?php if ( isset( $item['item_title'] ) ) {
				$link = isset( $item['item_link'] ) ? $item['item_link'] : '#';
				?>
				<a href="<?php echo esc_url( $link ); ?>"><?php echo esc_html( $item['item_title'] ); ?></a>
			<?php } ?>
		</li>
	<?php } ?>
</ul>
