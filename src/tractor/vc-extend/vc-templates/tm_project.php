<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
$post_type              = 'project';
$style                  = $el_class = $order = $animation = $filter_by = $filter_wrap = $filter_enable = $filter_type = $filter_align = $filter_counter = $pagination_align = $pagination_button_text = '';
$gutter                 = 0;
$carousel_items_display = $carousel_gutter = $carousel_loop = $carousel_centered = $carousel_pagination = $carousel_nav = $slider_button_id = $carousel_auto_play = '';
$main_query             = '';

$atts   = vc_map_get_attributes( $this->getShortcode(), $atts );
$css_id = uniqid( 'tm-project-' );
$this->get_inline_css( "#$css_id", $atts );
Tractor_VC::get_shortcode_custom_css( "#$css_id", $atts );
extract( $atts );

$tractor_post_args = array(
	'post_type'      => $post_type,
	'posts_per_page' => $number,
	'orderby'        => $orderby,
	'order'          => $order,
	'paged'          => 1,
	'post_status'    => 'publish',
);

if ( in_array( $orderby, array( 'meta_value', 'meta_value_num' ), true ) ) {
	$tractor_post_args['meta_key'] = $meta_key;
}

if ( get_query_var( 'paged' ) ) {
	$tractor_post_args['paged'] = get_query_var( 'paged' );
} elseif ( get_query_var( 'page' ) ) {
	$tractor_post_args['paged'] = get_query_var( 'page' );
}

$tractor_post_args = Tractor_VC::get_tax_query_of_taxonomies( $tractor_post_args, $taxonomies );

if ( $main_query === '1' ) {
	global $wp_query;
	$tractor_query = $wp_query;
} else {
	$tractor_query = new WP_Query( $tractor_post_args );
}

$el_class = $this->getExtraClass( $el_class );

$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'tm-project ' . $el_class, $this->settings['base'], $atts );
$css_class .= " style-$style";

if ( $filter_wrap === '1' ) {
	$css_class .= ' filter-wrap';
}
$is_swiper    = false;
$grid_classes = 'tm-grid';

if ( in_array( $style, array( 'grid-caption', 'feature-left' ), true ) ) {
	$grid_classes .= ' modern-grid';
} elseif ( in_array( $style, array( 'carousel' ), true ) ) {
	$is_swiper = true;
}

if ( $is_swiper ) {
	$grid_classes .= ' swiper-wrapper';
	$slider_classes = 'tm-swiper';
	if ( $carousel_nav !== '' ) {
		$slider_classes .= " nav-style-$carousel_nav";
	}
	if ( $carousel_pagination !== '' ) {
		$slider_classes .= " pagination-style-$carousel_pagination";
	}
}

$grid_classes .= Tractor_Helper::get_grid_animation_classes();
$css_class .= Tractor_Helper::get_animation_classes();
?>
<?php if ( $tractor_query->have_posts() ) : ?>
	<div class="tm-grid-wrapper <?php echo esc_attr( trim( $css_class ) ); ?>" id="<?php echo esc_attr( $css_id ); ?>"
		<?php if ( $pagination !== '' && $tractor_query->found_posts > $number ) : ?>
			data-pagination="<?php echo esc_attr( $pagination ); ?>"
		<?php endif; ?>

		 data-filter-type="<?php echo esc_attr( $filter_type ); ?>"
	>
		<?php
		$count = $tractor_query->post_count;

		$tm_grid_query                  = $tractor_post_args;
		$tm_grid_query['action']        = "{$post_type}_infinite_load";
		$tm_grid_query['max_num_pages'] = $tractor_query->max_num_pages;
		$tm_grid_query['found_posts']   = $tractor_query->found_posts;
		$tm_grid_query['taxonomies']    = $taxonomies;
		$tm_grid_query['style']         = $style;
		$tm_grid_query['image_size']    = $image_size;
		$tm_grid_query['pagination']    = $pagination;
		$tm_grid_query['count']         = $count;
		$tm_grid_query                  = htmlspecialchars( wp_json_encode( $tm_grid_query ) );
		?>

		<?php Tractor_Templates::grid_filters( $post_type, $filter_enable, $filter_align, $filter_counter, $filter_wrap, $tractor_query->found_posts, $filter_by ); ?>

		<input type="hidden" class="tm-grid-query" <?php echo 'value="' . $tm_grid_query . '"'; ?>/>

		<?php if ( $is_swiper ) { ?>
		<div class="<?php echo esc_attr( $slider_classes ); ?>"
			<?php
			if ( $carousel_items_display !== '' ) {
				$arr = explode( ';', $carousel_items_display );
				foreach ( $arr as $value ) {
					$tmp = explode( ':', $value );
					echo ' data-' . $tmp[0] . '-items="' . $tmp[1] . '"';
				}
			}
			?>

			<?php if ( $carousel_gutter > 1 ) : ?>
				data-lg-gutter="<?php echo esc_attr( $carousel_gutter ); ?>"
			<?php endif; ?>

			<?php if ( $carousel_loop === '1' ) : ?>
				data-loop="1"
			<?php endif; ?>

			<?php if ( $carousel_centered === '1' ) : ?>
				data-centered="1"
			<?php endif; ?>

			<?php if ( $carousel_nav !== '' ) : ?>
				data-nav="1"
			<?php endif; ?>

			<?php if ( $carousel_nav === 'custom' ) : ?>
				data-custom-nav="<?php echo esc_attr( $slider_button_id ); ?>"
			<?php endif; ?>

			<?php Tractor_Helper::get_swiper_pagination_attributes( $carousel_pagination ); ?>

			<?php if ( $carousel_auto_play !== '' ) : ?>
				data-autoplay="<?php echo esc_attr( $carousel_auto_play ); ?>"
			<?php endif; ?>
		>
			<div class="swiper-container">
				<?php } ?>

				<div class="<?php echo esc_attr( $grid_classes ); ?>">

					<?php
					set_query_var( 'tractor_query', $tractor_query );
					set_query_var( 'count', $count );
					set_query_var( 'image_size', $image_size );

					get_template_part( 'loop/shortcodes/project/style', $style );
					?>

				</div>

				<?php if ( $is_swiper ) { ?>
			</div>
		</div>
	<?php } ?>

		<?php Tractor_Templates::grid_pagination( $tractor_query, $number, $pagination, $pagination_align, $pagination_button_text ); ?>

	</div>
	<?php
	wp_reset_postdata();
endif; ?>
