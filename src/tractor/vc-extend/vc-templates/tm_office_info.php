<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$el_class = $this->getExtraClass( $el_class );
$el_class .= Tractor_Helper::get_animation_classes( $animation ) . ' tm-office-info';
$link = vc_build_link( $link );
?>
<div class="<?php echo esc_attr( trim( $el_class ) ); ?>">
	<?php
	if ( $image != '' ) {
		$image_url = wp_get_attachment_image_url( $image, 'full' );
		echo '<div class="image"><img src="' . esc_url( $image_url ) . '"/></div>';
	}
	if ( $title != '' ) {
		echo '<div class="title">' . esc_html( $title ) . '</div>';
	}
	?>
	<div class="info">
		<?php
		if ( $address != '' ) {
			echo '<div class="address">' . esc_html( $address ) . '</div>';
		}
		if ( $email != '' ) {
			echo '<div class="email">' . esc_html( $email ) . '</div>';
		}
		if ( $phone != '' ) {
			echo '<div class="phone">' . esc_html( $phone ) . '</div>';
		}
		?>
	</div>
	<?php if ( ( $link['title'] != '' ) && ( $link['url'] != '' ) ) {
		echo '<div class="link"><a href="' . esc_url( $link['url'] ) . '" target="' . esc_attr( $link['target'] ) . '" rel="' . esc_attr( $link['rel'] ) . '">' . esc_html( $link['title'] ) . '</a></div>';
	} ?>
</div>
