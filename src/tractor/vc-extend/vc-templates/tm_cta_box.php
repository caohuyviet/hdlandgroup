<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
?>
<div class="tm-cta-box">
	<?php
	if ( $image ) {
		echo '<div class="image"><img src="' . wp_get_attachment_image_url( $image, 'full' ) . '"/></div>';
	}
	?>
	<div class="info">
		<?php
		if ( $heading != '' ) {
			echo '<div class="heading">' . esc_html( $heading ) . '</div>';
		}
		$link = vc_build_link( $link );
		if ( ( $link['url'] != '' ) && ( $link['title'] != '' ) ) {
			echo '<div class="link"><a href="' . esc_url( $link['url'] ) . '" target="' . esc_attr( $link['target'] ) . '" rel="' . esc_attr( $link['rel'] ) . '">' . esc_html( $link['title'] ) . '</a></div>';
		}
		?>
	</div>
</div>
