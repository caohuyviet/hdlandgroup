<div id="page-close-main-menu" class="page-close-main-menu">
	<span class="ion-ios-close-empty"></span>
</div>
<div class="page-off-canvas-main-menu">
	<div id="page-navigation" <?php Tractor::navigation_class(); ?>>
		<?php Tractor::off_canvas_menu_primary(); ?>
	</div>
</div>
