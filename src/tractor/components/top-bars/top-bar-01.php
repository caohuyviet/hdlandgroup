<div <?php Tractor::top_bar_class(); ?>>
	<div class="container">
		<div class="row row-xs-center">
			<div class="col-md-6">
				<div class="top-bar-wrap top-bar-left">
					<?php Tractor_Templates::top_bar_link_list(); ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="top-bar-wrap top-bar-right">
					<?php Tractor_Templates::top_bar_social_networks(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
