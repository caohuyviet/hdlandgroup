<div <?php Tractor::top_bar_class(); ?>>
	<div class="container-fluid">
		<div class="row row-eq-height">
			<div class="col-md-12">
				<div class="top-bar-wrap">
					<div class="top-bar-left">
						<?php Tractor_Templates::top_bar_info(); ?>
					</div>

					<div class="top-bar-right">
						<?php Tractor_Templates::top_bar_link_list(); ?>
						<?php Tractor_Templates::top_bar_social_networks(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
