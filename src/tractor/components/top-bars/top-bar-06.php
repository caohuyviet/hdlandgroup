<?php
$text = Tractor::setting( 'top_bar_style_06_text' );
?>
<div <?php Tractor::top_bar_class(); ?>>
	<div class="container-fluid">
		<div class="row row-eq-height">
			<div class="col-md-4">
				<div class="top-bar-wrap top-bar-left">
					<?php Tractor_Templates::top_bar_link_list(); ?>
				</div>
			</div>
			<div class="col-md-8">
				<div class="top-bar-wrap top-bar-right">
					<?php Tractor_Templates::top_bar_info(); ?>
					<?php Tractor_Templates::top_bar_language_switcher(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
