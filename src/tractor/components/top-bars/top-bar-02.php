<div <?php Tractor::top_bar_class(); ?>>
	<div class="container">
		<div class="row row-eq-height">
			<div class="col-md-12">
				<div class="top-bar-wrap">
					<?php Tractor_Templates::top_bar_language_switcher(); ?>
					<div class="top-bar-right">
						<?php Tractor_Templates::top_bar_office(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
