<div id="page-title-bar" class="page-title-bar page-title-bar-05">
	<div class="page-title-bar-overlay"></div>

	<div class="page-title-bar-inner">
		<div class="container">
			<div class="row row-xs-center">
				<div class="col-md-12">
					<?php Tractor_Templates::get_title_bar_title(); ?>
				</div>
			</div>
		</div>

		<?php
		$enabled = Tractor::setting( "title_bar_05_breadcrumb_enable" );

		if ( '1' === $enabled ) {
			?>
			<div class="breadcrumb-wrap">
				<div class="container">
					<div class="row row-xs-center">
						<div class="col-md-12">
							<?php get_template_part( 'components/breadcrumb' ); ?>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
		?>
	</div>
</div>
