<?php
$number_post = Tractor::setting( 'single_post_related_number' );
$results     = Tractor_Query::get_related_posts( array(
	'post_id'      => get_the_ID(),
	'number_posts' => $number_post,
) );

if ( $results !== false && $results->have_posts() ) : ?>
	<div class="related-posts">
		<h3 class="related-title">
			<?php esc_html_e( 'Related posts', 'tractor' ); ?>
		</h3>
		<div class="tm-swiper equal-height"
		     data-lg-items="2"
		     data-md-items="2"
		     data-sm-items="1"
		     data-lg-gutter="30"
		     data-pagination="1"
		     data-slides-per-group="inherit"
		>
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<?php while ( $results->have_posts() ) : $results->the_post(); ?>
						<div class="swiper-slide">
							<div class="related-post-item">
								<div class="post-item-wrapper">

									<div class="related-post-feature-wrap">
										<?php get_template_part( 'loop/blog-classic/format' ); ?>
										<div class="related-post-date">
											<?php echo get_the_date(); ?>
										</div>
									</div>

									<div class="related-post-info">
										<?php if ( has_category() ) : ?>
											<div class="related-post-categories">
												<?php the_category( ', ' ); ?>
											</div>
										<?php endif; ?>

										<?php get_template_part( 'loop/blog/title' ); ?>
									</div>

								</div>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif;
wp_reset_postdata();
