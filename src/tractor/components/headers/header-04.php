<header id="page-header" <?php Tractor::header_class(); ?>>
	<div id="page-header-inner" class="page-header-inner" data-sticky="1">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="header-wrap">

						<?php get_template_part( 'components/branding' ); ?>

						<div class="header-right">
							<?php get_template_part( 'components/navigation' ); ?>

							<?php Tractor_Templates::header_search_button(); ?>

							<?php Tractor_Templates::header_open_mobile_menu_button(); ?>

							<?php Tractor_Templates::header_button(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
