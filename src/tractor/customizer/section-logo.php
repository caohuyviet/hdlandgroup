<?php
$section  = 'logo';
$priority = 1;
$prefix   = 'logo_';

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'logo',
	'label'       => esc_html__( 'Default Logo', 'tractor' ),
	'description' => esc_html__( 'Choose default logo.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'logo_dark',
	'choices'     => array(
		'logo_dark'  => esc_html__( 'Dark Logo', 'tractor' ),
		'logo_light' => esc_html__( 'Light Logo', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'image',
	'settings' => 'logo_dark',
	'label'    => esc_html__( 'Dark Logo', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => TRACTOR_THEME_IMAGE_URI . '/logo-dark.png',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'image',
	'settings' => 'logo_dark_retina',
	'label'    => esc_html__( 'Dark Logo Retina', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => TRACTOR_THEME_IMAGE_URI . '/logo-dark-retina.png',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'image',
	'settings' => 'logo_light',
	'label'    => esc_html__( 'Light Logo', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => TRACTOR_THEME_IMAGE_URI . '/logo-dark.png',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'image',
	'settings' => 'logo_light_retina',
	'label'    => esc_html__( 'Light Logo Retina', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => TRACTOR_THEME_IMAGE_URI . '/logo-dark-retina.png',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'dimension',
	'settings'    => $prefix . 'width',
	'label'       => esc_html__( 'Logo Width', 'tractor' ),
	'description' => esc_html__( 'E.g 200px', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '220px',
	'output'      => array(
		array(
			'element'  => '.branding__logo img,
			.error404--header .branding__logo img
			',
			'property' => 'width',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'spacing',
	'settings'    => $prefix . 'padding',
	'label'       => esc_html__( 'Logo Padding', 'tractor' ),
	'description' => esc_html__( 'E.g 30px 0px 30px 0px', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'top'    => '15px',
		'right'  => '0px',
		'bottom' => '15px',
		'left'   => '0px',
	),
	'output'      => array(
		array(
			'element'  => '.branding__logo img',
			'property' => 'padding',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Sticky Logo', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'dimension',
	'settings'    => 'sticky_logo_width',
	'label'       => esc_html__( 'Logo Width', 'tractor' ),
	'description' => esc_html__( 'Controls the width of sticky header logo, e.g 120px', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '150px',
	'output'      => array(
		array(
			'element'  => '
			.header-sticky-both .headroom.headroom--not-top .branding img,
			.header-sticky-up .headroom.headroom--not-top.headroom--pinned .branding img,
			.header-sticky-down .headroom.headroom--not-top.headroom--unpinned .branding img
			',
			'property' => 'width',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'spacing',
	'settings'    => 'sticky_logo_padding',
	'label'       => esc_html__( 'Logo Padding', 'tractor' ),
	'description' => esc_html__( 'Controls the padding of sticky header logo.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'top'    => '0',
		'right'  => '0',
		'bottom' => '0',
		'left'   => '0',
	),
	'output'      => array(
		array(
			'element'  => '.headroom--not-top .branding__logo .sticky-logo',
			'property' => 'padding',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Mobile Menu Logo', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'image',
	'settings'    => 'mobile_logo',
	'label'       => esc_html__( 'Logo', 'tractor' ),
	'description' => esc_html__( 'Select an image file for mobile menu logo.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => TRACTOR_THEME_IMAGE_URI . '/logo-dark.png',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'image',
	'settings'    => 'mobile_logo_retina',
	'label'       => esc_html__( 'Logo Retina', 'tractor' ),
	'description' => esc_html__( 'Select an image file for mobile menu logo.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => TRACTOR_THEME_IMAGE_URI . '/logo-dark-retina.png',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'dimension',
	'settings'    => 'mobile_logo_width',
	'label'       => esc_html__( 'Logo Width', 'tractor' ),
	'description' => esc_html__( 'Controls the width of mobile menu logo, e.g 120px', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '220px',
	'output'      => array(
		array(
			'element'  => '.page-mobile-menu-logo img',
			'property' => 'width',
		),
	),
) );
