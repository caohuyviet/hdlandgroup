<?php
$section  = 'color_';
$priority = 1;
$prefix   = 'color_';

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'preset',
	'settings' => 'color_preset',
	'title'    => esc_html__( 'Color Preset', 'tractor' ),
	'section'  => $section,
	'default'  => '-1',
	'priority' => $priority ++,
	'multiple' => 3,
	'choices'  => array(
		'-1' => array(
			'label'    => esc_html__( 'None', 'tractor' ),
			'settings' => array(),
		),
		'01' => array(
			'label'    => esc_html__( 'Preset 01', 'tractor' ),
			'settings' => array(
				'primary_color'   => Tractor::PRIMARY_COLOR,
				'secondary_color' => Tractor::SECONDARY_COLOR,
			),
		),
		'02' => array(
			'label'    => esc_html__( 'Preset 02', 'tractor' ),
			'settings' => array(
				'primary_color'                           => '#DE2021',
				'secondary_color'                         => '#11202D',
				'heading_color'                           => '#11202D',
				'link_color'                              => '#11202D',
				'link_color_hover'                        => '#DE2021',
				'button_color'                            => array(
					'color'      => '#fff',
					'border'     => '#DE2021',
					'background' => '#DE2021',
				),
				'button_hover_color'                      => array(
					'color'      => '#DE2021',
					'border'     => '#DE2021',
					'background' => 'rgba(0, 0, 0, 0)',
				),
				'navigation_dropdown_link_hover_color'    => '#DE2021',
				'navigation_dropdown_border_bottom_color' => '#DE2021',
				'search_popup_text_color'                 => '#DE2021',
				'search_popup_icon_hover_color'           => '#DE2021',
			),
		),
		'03' => array(
			'label'    => esc_html__( 'Preset 03', 'tractor' ),
			'settings' => array(
				'primary_color'                           => '#FF732F',
				'secondary_color'                         => '#222222',
				'heading_color'                           => '#222222',
				'link_color'                              => '#222222',
				'link_color_hover'                        => '#FF732F',
				'button_color'                            => array(
					'color'      => '#fff',
					'border'     => '#FF732F',
					'background' => '#FF732F',
				),
				'button_hover_color'                      => array(
					'color'      => '#FF732F',
					'border'     => '#FF732F',
					'background' => 'rgba(0, 0, 0, 0)',
				),
				'navigation_dropdown_link_hover_color'    => '#FF732F',
				'navigation_dropdown_border_bottom_color' => '#FF732F',
				'search_popup_text_color'                 => '#FF732F',
				'search_popup_icon_hover_color'           => '#FF732F',
			),
		),
		'04' => array(
			'label'    => esc_html__( 'Preset 04', 'tractor' ),
			'settings' => array(
				'primary_color'                           => '#0C95EC',
				'secondary_color'                         => '#222222',
				'heading_color'                           => '#222222',
				'link_color'                              => '#222222',
				'link_color_hover'                        => '#0C95EC',
				'button_color'                            => array(
					'color'      => '#fff',
					'border'     => '#0C95EC',
					'background' => '#0C95EC',
				),
				'button_hover_color'                      => array(
					'color'      => '#0C95EC',
					'border'     => '#0C95EC',
					'background' => 'rgba(0, 0, 0, 0)',
				),
				'navigation_dropdown_link_hover_color'    => '#0C95EC',
				'navigation_dropdown_border_bottom_color' => '#0C95EC',
				'search_popup_text_color'                 => '#0C95EC',
				'search_popup_icon_hover_color'           => '#0C95EC',
			),
		),
		'05' => array(
			'label'    => esc_html__( 'Preset 05', 'tractor' ),
			'settings' => array(
				'primary_color'                           => '#F07036',
				'secondary_color'                         => '#06112C',
				'heading_color'                           => '#06112C',
				'link_color'                              => '#06112C',
				'link_color_hover'                        => '#F07036',
				'button_color'                            => array(
					'color'      => '#fff',
					'border'     => '#F07036',
					'background' => '#F07036',
				),
				'button_hover_color'                      => array(
					'color'      => '#F07036',
					'border'     => '#F07036',
					'background' => 'rgba(0, 0, 0, 0)',
				),
				'navigation_dropdown_link_hover_color'    => '#F07036',
				'navigation_dropdown_border_bottom_color' => '#F07036',
				'search_popup_text_color'                 => '#F07036',
				'search_popup_icon_hover_color'           => '#F07036',
			),
		),
		'06' => array(
			'label'    => esc_html__( 'Preset 06', 'tractor' ),
			'settings' => array(
				'primary_color'                           => '#FCC30A',
				'secondary_color'                         => '#222222',
				'heading_color'                           => '#222222',
				'link_color'                              => '#222222',
				'link_color_hover'                        => '#FCC30A',
				'button_color'                            => array(
					'color'      => '#fff',
					'border'     => '#FCC30A',
					'background' => '#FCC30A',
				),
				'button_hover_color'                      => array(
					'color'      => '#FCC30A',
					'border'     => '#FCC30A',
					'background' => 'rgba(0, 0, 0, 0)',
				),
				'navigation_dropdown_link_hover_color'    => '#FCC30A',
				'navigation_dropdown_border_bottom_color' => '#FCC30A',
				'search_popup_text_color'                 => '#FCC30A',
				'search_popup_icon_hover_color'           => '#FCC30A',
			),
		),
		'07' => array(
			'label'    => esc_html__( 'Preset 07', 'tractor' ),
			'settings' => array(
				'primary_color'                           => '#FC844B',
				'secondary_color'                         => '#05103B',
				'heading_color'                           => '#05103B',
				'link_color'                              => '#05103B',
				'link_color_hover'                        => '#FC844B',
				'button_color'                            => array(
					'color'      => '#fff',
					'border'     => '#FC844B',
					'background' => '#FC844B',
				),
				'button_hover_color'                      => array(
					'color'      => '#FC844B',
					'border'     => '#FC844B',
					'background' => 'rgba(0, 0, 0, 0)',
				),
				'navigation_dropdown_link_hover_color'    => '#FC844B',
				'navigation_dropdown_border_bottom_color' => '#FC844B',
				'search_popup_text_color'                 => '#FC844B',
				'search_popup_icon_hover_color'           => '#FC844B',
			),
		),
		'08' => array(
			'label'    => esc_html__( 'Preset 08', 'tractor' ),
			'settings' => array(
				'primary_color'                           => '#7F60D1',
				'secondary_color'                         => '#05103B',
				'heading_color'                           => '#05103B',
				'link_color'                              => '#05103B',
				'link_color_hover'                        => '#7F60D1',
				'button_color'                            => array(
					'color'      => '#fff',
					'border'     => '#7F60D1',
					'background' => '#7F60D1',
				),
				'button_hover_color'                      => array(
					'color'      => '#05103B',
					'border'     => '#05103B',
					'background' => 'rgba(0, 0, 0, 0)',
				),
				'navigation_dropdown_link_hover_color'    => '#7F60D1',
				'navigation_dropdown_border_bottom_color' => '#7F60D1',
				'top_bar_style_01_link_hover_color'       => '#7F60D1',
				'search_popup_text_color'                 => '#7F60D1',
				'search_popup_icon_hover_color'           => '#7F60D1',
			),
		),
		'10' => array(
			'label'    => esc_html__( 'Preset 10', 'tractor' ),
			'settings' => array(
				'primary_color'                           => '#F6732E',
				'secondary_color'                         => '#222222',
				'heading_color'                           => '#222222',
				'link_color'                              => '#222222',
				'link_color_hover'                        => '#F6732E',
				'button_color'                            => array(
					'color'      => '#fff',
					'border'     => '#F6732E',
					'background' => '#F6732E',
				),
				'button_hover_color'                      => array(
					'color'      => '#F6732E',
					'border'     => '#F6732E',
					'background' => 'rgba(0, 0, 0, 0)',
				),
				'navigation_dropdown_link_hover_color'    => '#F6732E',
				'navigation_dropdown_border_bottom_color' => '#F6732E',
				'search_popup_text_color'                 => '#F6732E',
				'search_popup_icon_hover_color'           => '#F6732E',
			),
		),
		'11' => array(
			'label'    => esc_html__( 'Preset 11', 'tractor' ),
			'settings' => array(
				'primary_color'                           => '#D0021B',
				'secondary_color'                         => '#222222',
				'heading_color'                           => '#222222',
				'link_color'                              => '#222222',
				'link_color_hover'                        => '#D0021B',
				'button_color'                            => array(
					'color'      => '#fff',
					'border'     => '#D0021B',
					'background' => '#D0021B',
				),
				'button_hover_color'                      => array(
					'color'      => '#D0021B',
					'border'     => '#D0021B',
					'background' => 'rgba(0, 0, 0, 0)',
				),
				'navigation_dropdown_link_hover_color'    => '#D0021B',
				'navigation_dropdown_border_bottom_color' => '#D0021B',
				'search_popup_text_color'                 => '#D0021B',
				'search_popup_icon_hover_color'           => '#D0021B',
			),
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => 'primary_color',
	'label'       => esc_html__( 'Primary Color', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Tractor::PRIMARY_COLOR,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color',
	'settings'    => 'secondary_color',
	'label'       => esc_html__( 'Secondary Color', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Tractor::SECONDARY_COLOR,
) );
