<?php
$section  = 'top_bar';
$priority = 1;
$prefix   = 'top_bar_';

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => 'global_top_bar',
	'label'    => esc_html__( 'Default Top Bar', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '01',
	'choices'  => Tractor_Helper::get_top_bar_list(),
) );
