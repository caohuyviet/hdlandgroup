<?php
$panel    = 'top_bar';
$priority = 1;

Tractor_Kirki::add_section( 'top_bar', array(
	'title'    => esc_html__( 'General', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'top_bar_style_01', array(
	'title'    => esc_html__( 'Top Bar Style 01', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'top_bar_style_02', array(
	'title'    => esc_html__( 'Top Bar Style 02', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'top_bar_style_03', array(
	'title'    => esc_html__( 'Top Bar Style 03', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'top_bar_style_04', array(
	'title'    => esc_html__( 'Top Bar Style 04', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'top_bar_style_05', array(
	'title'    => esc_html__( 'Top Bar Style 05', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'top_bar_style_06', array(
	'title'    => esc_html__( 'Top Bar Style 06', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'top_bar_style_07', array(
	'title'    => esc_html__( 'Top Bar Style 07', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'top_bar_style_08', array(
	'title'    => esc_html__( 'Top Bar Style 08', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'top_bar_style_09', array(
	'title'    => esc_html__( 'Top Bar Style 09', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'top_bar_style_10', array(
	'title'    => esc_html__( 'Top Bar Style 10', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
