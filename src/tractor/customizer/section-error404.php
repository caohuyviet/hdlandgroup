<?php
$section  = 'error404_page';
$priority = 1;
$prefix   = 'error404_page_';

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'logo',
	'label'    => esc_html__( 'Logo', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'light',
	'choices'  => array(
		'light' => esc_html__( 'Light', 'tractor' ),
		'dark'  => esc_html__( 'Dark', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => 'error404_page_title',
	'label'       => esc_html__( 'Title', 'tractor' ),
	'description' => esc_html__( 'Controls the title that display on error 404 page.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'OOPS! That page can be not found!', 'tractor' ),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => 'error404_sub_title',
	'label'       => esc_html__( 'Sub Title', 'tractor' ),
	'description' => esc_html__( 'Controls the sub title that display on error 404 page.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Leverage agile frameworks to provide a robust synopsis for high level.', 'tractor' ),
) );
