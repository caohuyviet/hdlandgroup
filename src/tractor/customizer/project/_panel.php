<?php
$panel    = 'project';
$priority = 1;

Tractor_Kirki::add_section( 'archive_project', array(
	'title'    => esc_html__( 'Project Archive', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'single_project', array(
	'title'    => esc_html__( 'Project Single', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
