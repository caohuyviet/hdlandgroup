<?php
$section  = 'single_project';
$priority = 1;
$prefix   = 'single_project_';

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'comment_enable',
	'label'       => esc_html__( 'Comments', 'tractor' ),
	'description' => esc_html__( 'Turn on to display comments on single Project posts.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );
