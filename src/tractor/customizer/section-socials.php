<?php
$section  = 'socials';
$priority = 1;
$prefix   = 'social_';

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'social_link_target',
	'label'    => esc_html__( 'Open link in a new tab.', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '1',
	'choices'  => array(
		'0' => esc_html__( 'No', 'tractor' ),
		'1' => esc_html__( 'Yes', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'repeater',
	'settings'  => 'social_link',
	'section'   => $section,
	'priority'  => $priority ++,
	'choices'   => array(
		'labels' => array(
			'add-new-row' => esc_html__( 'Add new social network', 'tractor' ),
		),
	),
	'row_label' => array(
		'type'  => 'field',
		'field' => 'tooltip',
	),
	'default'   => array(
		array(
			'tooltip'    => esc_html__( 'Facebook', 'tractor' ),
			'icon_class' => 'ion-social-facebook',
			'link_url'   => 'https://facebook.com',
		),
		array(
			'tooltip'    => esc_html__( 'Twitter', 'tractor' ),
			'icon_class' => 'ion-social-twitter',
			'link_url'   => 'https://twitter.com',
		),
		array(
			'tooltip'    => esc_html__( 'Instagram', 'tractor' ),
			'icon_class' => 'ion-social-instagram',
			'link_url'   => 'https://instagram.com',
		),
		array(
			'tooltip'    => esc_html__( 'Youtube', 'tractor' ),
			'icon_class' => 'ion-social-youtube',
			'link_url'   => 'https://www.youtube.com',
		),
	),
	'fields'    => array(
		'tooltip'    => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Tooltip', 'tractor' ),
			'description' => esc_html__( 'Enter your hint text for your icon', 'tractor' ),
			'default'     => '',
		),
		'icon_class' => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Icon Class', 'tractor' ),
			'description' => esc_html__( 'This will be the icon class for your link', 'tractor' ),
			'default'     => '',
		),
		'link_url'   => array(
			'type'        => 'text',
			'label'       => esc_html__( 'Link URL', 'tractor' ),
			'description' => esc_html__( 'This will be the link URL', 'tractor' ),
			'default'     => '',
		),
	),
) );
