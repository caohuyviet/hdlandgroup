<?php
$panel    = 'footer';
$priority = 1;

Tractor_Kirki::add_section( 'footer', array(
	'title'    => esc_html__( 'General', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'footer_01', array(
	'title'    => esc_html__( 'Style 01', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'footer_02', array(
	'title'    => esc_html__( 'Style 02', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'footer_03', array(
	'title'    => esc_html__( 'Style 03', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'footer_04', array(
	'title'    => esc_html__( 'Style 04', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'footer_05', array(
	'title'    => esc_html__( 'Style 05', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'footer_06', array(
	'title'    => esc_html__( 'Style 06', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'footer_07', array(
	'title'    => esc_html__( 'Style 07', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'footer_08', array(
	'title'    => esc_html__( 'Style 08', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'footer_10', array(
	'title'    => esc_html__( 'Style 10', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'footer_11', array(
	'title'    => esc_html__( 'Style 11', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'footer_simple', array(
	'title'    => esc_html__( 'Footer Simple', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
