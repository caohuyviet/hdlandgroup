<?php
$section  = 'footer';
$priority = 1;
$prefix   = 'footer_';

$footers = Tractor_Footer::get_list_footers( true );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => $prefix . 'page',
	'label'       => esc_html__( 'Footer', 'tractor' ),
	'description' => esc_html__( 'Select a default footer for all pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'footer-01',
	'choices'     => Tractor_Footer::get_list_footers(),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_service_footer_page',
	'label'       => esc_html__( 'Single Service', 'tractor' ),
	'description' => esc_html__( 'Select default footer that displays on all single service pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'footer-02',
	'choices'     => $footers,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_project_footer_page',
	'label'       => esc_html__( 'Single Project', 'tractor' ),
	'description' => esc_html__( 'Select default footer that displays on all single Project pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'footer-01',
	'choices'     => $footers,
) );
