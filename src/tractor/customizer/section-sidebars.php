<?php
$section             = 'sidebars';
$priority            = 1;
$prefix              = 'sidebars_';
$sidebar_positions   = Tractor_Helper::get_list_sidebar_positions();
$registered_sidebars = Tractor_Helper::get_registered_sidebars();

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => sprintf( '<div class="desc">
			<strong class="insight-label insight-label-info">%s</strong>
			<p>%s</p>
			<p>%s</p>
		</div>', esc_html__( 'IMPORTANT NOTE: ', 'tractor' ), esc_html__( 'Sidebar 2 can only be used if sidebar 1 is selected.', 'tractor' ), esc_html__( 'Sidebar position option will control the position of sidebar 1. If sidebar 2 is selected, it will display on the opposite side.', 'tractor' ) ),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'General Settings', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'number',
	'settings'    => 'one_sidebar_breakpoint',
	'label'       => esc_html__( 'One Sidebar Breakpoint', 'tractor' ),
	'description' => esc_html__( 'Controls the breakpoint when has only one sidebar to make the sidebar 100% width.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'postMessage',
	'default'     => 992,
	'choices'     => array(
		'min'  => 460,
		'max'  => 1300,
		'step' => 10,
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'number',
	'settings'    => 'both_sidebar_breakpoint',
	'label'       => esc_html__( 'Both Sidebars Breakpoint', 'tractor' ),
	'description' => esc_html__( 'Controls the breakpoint when has both sidebars to make sidebars 100% width.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'postMessage',
	'default'     => 1199,
	'choices'     => array(
		'min'  => 460,
		'max'  => 1300,
		'step' => 10,
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'sidebars_below_content_mobile',
	'label'       => esc_html__( 'Sidebars Below Content', 'tractor' ),
	'description' => esc_html__( 'Move sidebars display after main content on smaller screens.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'No', 'tractor' ),
		'1' => esc_html__( 'Yes', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Single Sidebar Layouts', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'number',
	'settings'    => 'single_sidebar_width',
	'label'       => esc_html__( 'Single Sidebar Width', 'tractor' ),
	'description' => esc_html__( 'Controls the width of the sidebar when only one sidebar is present. Input value as % unit, e.g 33.33333', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '33.33333',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'dimension',
	'settings'    => 'single_sidebar_offset',
	'label'       => esc_html__( 'Single Sidebar Offset', 'tractor' ),
	'description' => esc_html__( 'Controls the offset of the sidebar when only one sidebar is present. Enter value including any valid CSS unit, e.g 70px.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Dual Sidebar Layouts', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'number',
	'settings'    => 'dual_sidebar_width',
	'label'       => esc_html__( 'Dual Sidebar Width', 'tractor' ),
	'description' => esc_html__( 'Controls the width of sidebars when dual sidebars are present. Enter value including any valid CSS unit, e.g 33.33333.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '25',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'dimension',
	'settings'    => 'dual_sidebar_offset',
	'label'       => esc_html__( 'Dual Sidebar Offset', 'tractor' ),
	'description' => esc_html__( 'Controls the offset of sidebars when dual sidebars are present. Enter value including any valid CSS unit, e.g 70px.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Pages', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on all pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on all pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'right',
	'choices'  => $sidebar_positions,
) );


Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'page_sidebar_special',
	'label'       => esc_html__( 'Special Sidebar', 'tractor' ),
	'description' => esc_html__( 'Select special sidebar that will display below of first sidebar on all pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'special_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Search Page', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'search_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on search results page.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'blog_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'search_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on search results page.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'search_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'right',
	'choices'  => $sidebar_positions,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'search_page_sidebar_special',
	'label'       => esc_html__( 'Special Sidebar', 'tractor' ),
	'description' => esc_html__( 'Select special sidebar that will display below of first sidebar on search page.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'special_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Front Latest Posts Page', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'home_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on front latest posts page.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'blog_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'home_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on front latest posts page.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'home_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'right',
	'choices'  => $sidebar_positions,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'home_page_sidebar_special',
	'label'       => esc_html__( 'Special Sidebar', 'tractor' ),
	'description' => esc_html__( 'Select special sidebar that will display below of first sidebar front latest posts page.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'special_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Blog Posts', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'post_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on single blog post pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'blog_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'post_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on single blog post pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'post_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'right',
	'choices'  => $sidebar_positions,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'post_page_sidebar_special',
	'label'       => esc_html__( 'Special Sidebar', 'tractor' ),
	'description' => esc_html__( 'Select special sidebar that will display below of first sidebar on single blog post pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'special_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Blog Archive', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'blog_archive_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on blog archive pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'blog_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'blog_archive_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on blog archive pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'blog_archive_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'right',
	'choices'  => $sidebar_positions,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'blog_archive_page_sidebar_special',
	'label'       => esc_html__( 'Special Sidebar', 'tractor' ),
	'description' => esc_html__( 'Select special sidebar that will display below of first sidebar on blog archive pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'special_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Single Product', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'product_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on single product pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'shop_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'product_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on single product pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'product_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'right',
	'choices'  => $sidebar_positions,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'product_page_sidebar_special',
	'label'       => esc_html__( 'Special Sidebar', 'tractor' ),
	'description' => esc_html__( 'Select special sidebar that will display below of first sidebar on single product pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'special_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Product Archive', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'product_archive_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on product archive pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'shop_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'product_archive_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on product archive pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'product_archive_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'left',
	'choices'  => $sidebar_positions,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'product_archive_page_sidebar_special',
	'label'       => esc_html__( 'Special Sidebar', 'tractor' ),
	'description' => esc_html__( 'Select special sidebar that will display below of first sidebar on product archive pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'special_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Single Project', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'project_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on single Project pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'project_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'project_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on single Project pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'project_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'left',
	'choices'  => $sidebar_positions,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'project_page_sidebar_special',
	'label'       => esc_html__( 'Special Sidebar', 'tractor' ),
	'description' => esc_html__( 'Select special sidebar that will display below of first sidebar on single Project pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'special_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Project Archive', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'project_archive_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on Project archive pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'project_archive_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on Project archive pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'project_archive_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'left',
	'choices'  => $sidebar_positions,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'project_archive_page_sidebar_special',
	'label'       => esc_html__( 'Special Sidebar', 'tractor' ),
	'description' => esc_html__( 'Select special sidebar that will display below of first sidebar on Project archive pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'special_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Single Service', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'service_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on single service pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'service_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on single service pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'service_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'left',
	'choices'  => $sidebar_positions,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'service_page_sidebar_special',
	'label'       => esc_html__( 'Special Sidebar', 'tractor' ),
	'description' => esc_html__( 'Select special sidebar that will display below of first sidebar on single service pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'special_sidebar',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'number',
	'settings'    => 'service_page_single_sidebar_width',
	'label'       => esc_html__( 'Single Sidebar Width', 'tractor' ),
	'description' => esc_html__( 'Controls the width of the sidebar when only one sidebar is present. Input value as % unit, e.g 33.33333. This setting will be override global sidebar with, If leave blank then use global sidebar width.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '33.333333',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'custom',
	'settings' => $prefix . 'group_title_' . $priority ++,
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '<div class="big_title">' . esc_html__( 'Service Archive', 'tractor' ) . '</div>',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'service_archive_page_sidebar_1',
	'label'       => esc_html__( 'Sidebar 1', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 1 that will display on service archive pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'service_archive_page_sidebar_2',
	'label'       => esc_html__( 'Sidebar 2', 'tractor' ),
	'description' => esc_html__( 'Select sidebar 2 that will display on service archive pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $registered_sidebars,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => 'service_archive_page_sidebar_position',
	'label'    => esc_html__( 'Sidebar Position', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'left',
	'choices'  => $sidebar_positions,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'service_archive_page_sidebar_special',
	'label'       => esc_html__( 'Special Sidebar', 'tractor' ),
	'description' => esc_html__( 'Select special sidebar that will display below of first sidebar on service archive pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'special_sidebar',
	'choices'     => $registered_sidebars,
) );
