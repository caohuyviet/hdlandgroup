<?php
$section  = 'preloader';
$priority = 1;
$prefix   = 'preloader_';

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'radio-buttonset',
	'settings' => $prefix . 'enable',
	'label'    => esc_html__( 'Enable Preloader', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '0',
	'choices'  => array(
		'0' => esc_html__( 'No', 'tractor' ),
		'1' => esc_html__( 'Yes', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => $prefix . 'style',
	'label'    => esc_html__( 'Style', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'image',
	'choices'  => Tractor_Helper::get_preloader_list(),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'background_color',
	'label'       => esc_html__( 'Background Color', 'tractor' ),
	'description' => esc_html__( 'Controls the background color for pre loader', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => 'rgba(255, 255, 255, 1)',
	'output'      => array(
		array(
			'element'  => '.page-loading',
			'property' => 'background-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'shape_color',
	'label'       => esc_html__( 'Shape Color', 'tractor' ),
	'description' => esc_html__( 'Controls the background color for pre loader', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#ffffff',
	'output'      => array(
		array(
			'element'  => '
			.page-loading .sk-bg-self,
			.page-loading .sk-bg-child > div,
			.page-loading .sk-bg-child-before > div:before
			',
			'property' => 'background-color',
			'suffix'   => '!important',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'image',
	'settings' => 'preloader_image',
	'label'    => esc_html__( 'Image (for Image style)', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => TRACTOR_THEME_IMAGE_URI . '/preloader.gif',
) );
