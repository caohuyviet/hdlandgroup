<?php
$section  = 'title_bar';
$priority = 1;
$prefix   = 'title_bar_';

$title_bar_stylish = Tractor_Helper::get_title_bar_list( true );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'layout',
	'label'       => esc_html__( 'Default Title Bar', 'tractor' ),
	'description' => esc_html__( 'Select default title bar that displays on all pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '01',
	'choices'     => Tractor_Helper::get_title_bar_list(),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'search_title',
	'label'       => esc_html__( 'Search Heading', 'tractor' ),
	'description' => esc_html__( 'Enter text prefix that displays on search results page.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Search results for: ', 'tractor' ),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'home_title',
	'label'       => esc_html__( 'Home Heading', 'tractor' ),
	'description' => esc_html__( 'Enter text that displays on front latest posts page.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Blog', 'tractor' ),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'archive_category_title',
	'label'       => esc_html__( 'Archive Category Heading', 'tractor' ),
	'description' => esc_html__( 'Enter text prefix that displays on archive category page.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Category: ', 'tractor' ),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'archive_tag_title',
	'label'       => esc_html__( 'Archive Tag Heading', 'tractor' ),
	'description' => esc_html__( 'Enter text prefix that displays on archive tag page.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Tag: ', 'tractor' ),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'archive_author_title',
	'label'       => esc_html__( 'Archive Author Heading', 'tractor' ),
	'description' => esc_html__( 'Enter text prefix that displays on archive author page.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Author: ', 'tractor' ),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'archive_year_title',
	'label'       => esc_html__( 'Archive Year Heading', 'tractor' ),
	'description' => esc_html__( 'Enter text prefix that displays on archive year page.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Year: ', 'tractor' ),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'archive_month_title',
	'label'       => esc_html__( 'Archive Month Heading', 'tractor' ),
	'description' => esc_html__( 'Enter text prefix that displays on archive month page.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Month: ', 'tractor' ),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'archive_day_title',
	'label'       => esc_html__( 'Archive Day Heading', 'tractor' ),
	'description' => esc_html__( 'Enter text prefix that displays on archive day page.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Day: ', 'tractor' ),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'single_blog_title',
	'label'       => esc_html__( 'Single Blog Heading', 'tractor' ),
	'description' => esc_html__( 'Enter text that displays on single blog posts. Leave blank to use post title.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Blog', 'tractor' ),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'text',
	'settings'    => $prefix . 'single_product_title',
	'label'       => esc_html__( 'Single Product Heading', 'tractor' ),
	'description' => esc_html__( 'Enter text that displays on single product pages. Leave blank to use product title.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Shop', 'tractor' ),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_page_title_bar_layout',
	'label'       => esc_html__( 'Single Page Title Bar Layout', 'tractor' ),
	'description' => esc_html__( 'Select default Title Bar that displays on all single pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'default',
	'choices'     => $title_bar_stylish,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_title_bar_layout',
	'label'       => esc_html__( 'Single Blog Page Title Bar Layout', 'tractor' ),
	'description' => esc_html__( 'Select default Title Bar that displays on all single blog post pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'default',
	'choices'     => $title_bar_stylish,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_product_title_bar_layout',
	'label'       => esc_html__( 'Single Product Page Title Bar Layout', 'tractor' ),
	'description' => esc_html__( 'Select default Title Bar that displays on all single product pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'default',
	'choices'     => $title_bar_stylish,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_project_title_bar_layout',
	'label'       => esc_html__( 'Single Project Page Title Bar Layout', 'tractor' ),
	'description' => esc_html__( 'Select default Title Bar that displays on all single Project post pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'none',
	'choices'     => $title_bar_stylish,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_service_title_bar_layout',
	'label'       => esc_html__( 'Single Service Page Title Bar Layout', 'tractor' ),
	'description' => esc_html__( 'Select default Title Bar that displays on all single service post pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '05',
	'choices'     => $title_bar_stylish,
) );
