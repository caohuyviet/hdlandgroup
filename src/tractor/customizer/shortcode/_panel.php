<?php
$panel    = 'shortcode';
$priority = 1;

Tractor_Kirki::add_section( 'shortcode_animation', array(
	'title'    => esc_html__( 'Animation', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
