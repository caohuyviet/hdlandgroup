<?php
$section  = 'shortcode_animation';
$priority = 1;
$prefix   = 'shortcode_animation_';

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'enable',
	'label'       => esc_html__( 'Mobile Animation', 'tractor' ),
	'description' => esc_html__( 'Controls the animations on mobile & tablet.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'desktop',
	'choices'     => array(
		'none'    => esc_html__( 'None', 'tractor' ),
		'mobile'  => esc_html__( 'Only Mobile', 'tractor' ),
		'desktop' => esc_html__( 'Only Desktop', 'tractor' ),
		'both'    => esc_html__( 'Both', 'tractor' ),
	),
) );
