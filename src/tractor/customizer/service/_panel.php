<?php
$panel    = 'service';
$priority = 1;

Tractor_Kirki::add_section( 'archive_service', array(
	'title'    => esc_html__( 'Service Archive', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'single_service', array(
	'title'    => esc_html__( 'Service Single', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
