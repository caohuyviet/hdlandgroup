<?php
$section  = 'single_service';
$priority = 1;
$prefix   = 'single_service_';

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => 'single_service_style',
	'label'    => esc_html__( 'Style', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '02',
	'choices'  => array(
		'01' => esc_attr__( 'Style 01', 'tractor' ),
		'02' => esc_attr__( 'Style 02', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'comment_enable',
	'label'       => esc_html__( 'Comments', 'tractor' ),
	'description' => esc_html__( 'Turn on to display comments on single Project posts.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );
