<?php
$section  = 'archive_service';
$priority = 1;
$prefix   = 'archive_service_';

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => 'archive_service_style',
	'label'    => esc_html__( 'Style', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'grid_classic_01',
	'choices'  => array(
		'grid_classic_01' => esc_attr__( 'Grid Classic 01', 'tractor' ),
		'grid_classic_02' => esc_attr__( 'Grid Classic 02', 'tractor' ),
		'grid_classic_03' => esc_attr__( 'Grid Classic 03', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'select',
	'settings' => 'archive_service_thumbnail_size',
	'label'    => esc_html__( 'Thumbnail Size', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => '370x250',
	'choices'  => array(
		'480x480' => esc_attr__( '480x480', 'tractor' ),
		'370x250' => esc_attr__( '370x250', 'tractor' ),
		'570x385' => esc_attr__( '570x385', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'number',
	'settings' => 'archive_service_gutter',
	'label'    => esc_html__( 'Gutter', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 30,
	'choices'  => array(
		'min'  => 0,
		'step' => 1,
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'number',
	'settings' => 'archive_service_row_gutter',
	'label'    => esc_html__( 'Gutter', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 35,
	'choices'  => array(
		'min'  => 0,
		'step' => 1,
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'     => 'text',
	'settings' => 'archive_service_columns',
	'label'    => esc_html__( 'Columns', 'tractor' ),
	'section'  => $section,
	'priority' => $priority ++,
	'default'  => 'xs:1;sm:2;md:3;lg:3',
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'archive_service_animation',
	'label'       => esc_html__( 'Animation', 'tractor' ),
	'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'move-up',
	'choices'     => array(
		'none'             => esc_attr__( 'None', 'tractor' ),
		'fade-in'          => esc_attr__( 'Fade In', 'tractor' ),
		'move-up'          => esc_attr__( 'Move Up', 'tractor' ),
		'scale-up'         => esc_attr__( 'Scale Up', 'tractor' ),
		'fall-perspective' => esc_attr__( 'Fall Perspective', 'tractor' ),
		'fly'              => esc_attr__( 'Fly', 'tractor' ),
		'flip'             => esc_attr__( 'Flip', 'tractor' ),
		'helix'            => esc_attr__( 'Helix', 'tractor' ),
		'pop-up'           => esc_attr__( 'Pop Up', 'tractor' ),
	),
) );
