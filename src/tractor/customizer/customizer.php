<?php
/**
 * Theme Customizer
 *
 * @package Tractor
 * @since   1.0
 */

/**
 * Setup configuration
 */
Tractor_Kirki::add_config( 'theme', array(
	'option_type' => 'theme_mod',
	'capability'  => 'edit_theme_options',
) );

/**
 * Add sections
 */
$priority = 1;

Tractor_Kirki::add_section( 'layout', array(
	'title'    => esc_html__( 'Layout', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'color_', array(
	'title'    => esc_html__( 'Colors', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'background', array(
	'title'    => esc_html__( 'Background', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'typography', array(
	'title'    => esc_html__( 'Typography', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_panel( 'top_bar', array(
	'title'    => esc_html__( 'Top bar', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_panel( 'header', array(
	'title'    => esc_html__( 'Header', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'logo', array(
	'title'    => esc_html__( 'Logo', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_panel( 'navigation', array(
	'title'    => esc_html__( 'Navigation', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'sliders', array(
	'title'    => esc_html__( 'Sliders', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_panel( 'title_bar', array(
	'title'    => esc_html__( 'Page Title Bar', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'sidebars', array(
	'title'    => esc_html__( 'Sidebars', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_panel( 'footer', array(
	'title'    => esc_html__( 'Footer', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_panel( 'blog', array(
	'title'    => esc_html__( 'Blog', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_panel( 'shop', array(
	'title'    => esc_html__( 'Shop', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_panel( 'project', array(
	'title'    => esc_html__( 'Project', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_panel( 'service', array(
	'title'    => esc_html__( 'Service', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'socials', array(
	'title'    => esc_html__( 'Social Networks', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'social_sharing', array(
	'title'    => esc_html__( 'Social Sharing', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_panel( 'search', array(
	'title'    => esc_html__( 'Search', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'error404_page', array(
	'title'    => esc_html__( 'Error 404 Page', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_panel( 'shortcode', array(
	'title'    => esc_html__( 'Shortcodes', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_panel( 'advanced', array(
	'title'    => esc_html__( 'Advanced', 'tractor' ),
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'additional_js', array(
	'title'    => esc_html__( 'Additional JS', 'tractor' ),
	'priority' => $priority ++,
) );

/**
 * Load panel & section files
 */
$files = array(
	TRACTOR_CUSTOMIZER_DIR . '/section-color.php',

	TRACTOR_CUSTOMIZER_DIR . '/top_bar/_panel.php',
	TRACTOR_CUSTOMIZER_DIR . '/top_bar/general.php',
	TRACTOR_CUSTOMIZER_DIR . '/top_bar/style-01.php',

	TRACTOR_CUSTOMIZER_DIR . '/header/_panel.php',
	TRACTOR_CUSTOMIZER_DIR . '/header/general.php',
	TRACTOR_CUSTOMIZER_DIR . '/header/sticky.php',
	TRACTOR_CUSTOMIZER_DIR . '/header/style-01.php',
	TRACTOR_CUSTOMIZER_DIR . '/header/style-02.php',
	TRACTOR_CUSTOMIZER_DIR . '/header/style-03.php',
	TRACTOR_CUSTOMIZER_DIR . '/header/style-04.php',

	TRACTOR_CUSTOMIZER_DIR . '/navigation/_panel.php',
	TRACTOR_CUSTOMIZER_DIR . '/navigation/desktop-menu.php',
	TRACTOR_CUSTOMIZER_DIR . '/navigation/off-canvas-menu.php',
	TRACTOR_CUSTOMIZER_DIR . '/navigation/mobile-menu.php',

	TRACTOR_CUSTOMIZER_DIR . '/section-sliders.php',

	TRACTOR_CUSTOMIZER_DIR . '/title_bar/_panel.php',
	TRACTOR_CUSTOMIZER_DIR . '/title_bar/general.php',
	TRACTOR_CUSTOMIZER_DIR . '/title_bar/style-01.php',
	TRACTOR_CUSTOMIZER_DIR . '/title_bar/style-02.php',
	TRACTOR_CUSTOMIZER_DIR . '/title_bar/style-03.php',
	TRACTOR_CUSTOMIZER_DIR . '/title_bar/style-04.php',
	TRACTOR_CUSTOMIZER_DIR . '/title_bar/style-05.php',
	TRACTOR_CUSTOMIZER_DIR . '/title_bar/style-06.php',

	TRACTOR_CUSTOMIZER_DIR . '/footer/_panel.php',
	TRACTOR_CUSTOMIZER_DIR . '/footer/general.php',
	TRACTOR_CUSTOMIZER_DIR . '/footer/style-01.php',
	TRACTOR_CUSTOMIZER_DIR . '/footer/footer-simple.php',

	TRACTOR_CUSTOMIZER_DIR . '/advanced/_panel.php',
	TRACTOR_CUSTOMIZER_DIR . '/advanced/advanced.php',
	TRACTOR_CUSTOMIZER_DIR . '/advanced/pre-loader.php',
	TRACTOR_CUSTOMIZER_DIR . '/advanced/light-gallery.php',

	TRACTOR_CUSTOMIZER_DIR . '/shortcode/_panel.php',
	TRACTOR_CUSTOMIZER_DIR . '/shortcode/animation.php',

	TRACTOR_CUSTOMIZER_DIR . '/section-background.php',
	TRACTOR_CUSTOMIZER_DIR . '/section-error404.php',
	TRACTOR_CUSTOMIZER_DIR . '/section-layout.php',
	TRACTOR_CUSTOMIZER_DIR . '/section-logo.php',

	TRACTOR_CUSTOMIZER_DIR . '/blog/_panel.php',
	TRACTOR_CUSTOMIZER_DIR . '/blog/archive.php',
	TRACTOR_CUSTOMIZER_DIR . '/blog/single.php',

	TRACTOR_CUSTOMIZER_DIR . '/project/_panel.php',
	TRACTOR_CUSTOMIZER_DIR . '/project/archive.php',
	TRACTOR_CUSTOMIZER_DIR . '/project/single.php',

	TRACTOR_CUSTOMIZER_DIR . '/service/_panel.php',
	TRACTOR_CUSTOMIZER_DIR . '/service/archive.php',
	TRACTOR_CUSTOMIZER_DIR . '/service/single.php',

	TRACTOR_CUSTOMIZER_DIR . '/shop/_panel.php',
	TRACTOR_CUSTOMIZER_DIR . '/shop/archive.php',
	TRACTOR_CUSTOMIZER_DIR . '/shop/single.php',
	TRACTOR_CUSTOMIZER_DIR . '/shop/cart.php',

	TRACTOR_CUSTOMIZER_DIR . '/search/_panel.php',
	TRACTOR_CUSTOMIZER_DIR . '/search/search-page.php',
	TRACTOR_CUSTOMIZER_DIR . '/search/search-popup.php',

	TRACTOR_CUSTOMIZER_DIR . '/section-sharing.php',
	TRACTOR_CUSTOMIZER_DIR . '/section-sidebars.php',
	TRACTOR_CUSTOMIZER_DIR . '/section-socials.php',
	TRACTOR_CUSTOMIZER_DIR . '/section-typography.php',
	TRACTOR_CUSTOMIZER_DIR . '/section-additional-js.php',
);

Tractor::require_files( $files );
