<?php
$section  = 'header';
$priority = 1;
$prefix   = 'header_';

$headers = Tractor_Helper::get_header_list( true );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'global_header',
	'label'       => esc_html__( 'Default Header', 'tractor' ),
	'description' => esc_html__( 'Select default header type for your site.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '04',
	'choices'     => Tractor_Helper::get_header_list(),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_page_header_type',
	'label'       => esc_html__( 'Single Page', 'tractor' ),
	'description' => esc_html__( 'Select default header type that displays on all single pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $headers,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_post_header_type',
	'label'       => esc_html__( 'Single Blog', 'tractor' ),
	'description' => esc_html__( 'Select default header type that displays on all single blog post pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $headers,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_product_header_type',
	'label'       => esc_html__( 'Single Product', 'tractor' ),
	'description' => esc_html__( 'Select default header type that displays on all single product pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $headers,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_service_header_type',
	'label'       => esc_html__( 'Single Service', 'tractor' ),
	'description' => esc_html__( 'Select default header type that displays on all single service pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '22',
	'choices'     => $headers,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'single_project_header_type',
	'label'       => esc_html__( 'Single Project', 'tractor' ),
	'description' => esc_html__( 'Select default header type that displays on all single Project pages.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '',
	'choices'     => $headers,
) );
