<?php
$panel    = 'header';
$priority = 1;

Tractor_Kirki::add_section( 'header', array(
	'title'    => esc_html__( 'General', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_sticky', array(
	'title'    => esc_html__( 'Header Sticky', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_01', array(
	'title'    => esc_html__( 'Header Style 01', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_02', array(
	'title'    => esc_html__( 'Header Style 02', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_03', array(
	'title'    => esc_html__( 'Header Style 03', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_04', array(
	'title'    => esc_html__( 'Header Style 04', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_05', array(
	'title'    => esc_html__( 'Header Style 05', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_06', array(
	'title'    => esc_html__( 'Header Style 06', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_07', array(
	'title'    => esc_html__( 'Header Style 07', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_08', array(
	'title'    => esc_html__( 'Header Style 08', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_09', array(
	'title'    => esc_html__( 'Header Style 09', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_10', array(
	'title'    => esc_html__( 'Header Style 10', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_11', array(
	'title'    => esc_html__( 'Header Style 11', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_12', array(
	'title'    => esc_html__( 'Header Style 12', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_13', array(
	'title'    => esc_html__( 'Header Style 13', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_14', array(
	'title'    => esc_html__( 'Header Style 14', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_15', array(
	'title'    => esc_html__( 'Header Style 15', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_16', array(
	'title'    => esc_html__( 'Header Style 16', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_17', array(
	'title'    => esc_html__( 'Header Style 17', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_18', array(
	'title'    => esc_html__( 'Header Style 18', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_19', array(
	'title'    => esc_html__( 'Header Style 19', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_20', array(
	'title'    => esc_html__( 'Header Style 20', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_21', array(
	'title'    => esc_html__( 'Header Style 21', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_22', array(
	'title'    => esc_html__( 'Header Style 22', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_23', array(
	'title'    => esc_html__( 'Header Style 23', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_24', array(
	'title'    => esc_html__( 'Header Style 24', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_25', array(
	'title'    => esc_html__( 'Header Style 25', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_26', array(
	'title'    => esc_html__( 'Header Style 26', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_27', array(
	'title'    => esc_html__( 'Header Style 27', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'header_style_28', array(
	'title'    => esc_html__( 'Header Style 28', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
