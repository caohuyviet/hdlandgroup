<?php
$section  = 'header_sticky';
$priority = 1;
$prefix   = 'header_sticky_';

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'toggle',
	'settings'    => $prefix . 'enable',
	'label'       => esc_html__( 'Enable', 'tractor' ),
	'description' => esc_html__( 'Enable this option to turn on header sticky feature.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 1,
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => $prefix . 'behaviour',
	'label'       => esc_html__( 'Behaviour', 'tractor' ),
	'description' => esc_html__( 'Controls the behaviour of header sticky when you scroll down to page', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 'both',
	'choices'     => array(
		'both' => esc_html__( 'Sticky on scroll up/down', 'tractor' ),
		'up'   => esc_html__( 'Sticky on scroll up', 'tractor' ),
		'down' => esc_html__( 'Sticky on scroll down', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'padding_top',
	'label'     => esc_html__( 'Padding top', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 0,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 200,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.headroom--not-top .page-header-inner',
			'property' => 'padding-top',
			'units'    => 'px',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'      => 'slider',
	'settings'  => $prefix . 'padding_bottom',
	'label'     => esc_html__( 'Padding bottom', 'tractor' ),
	'section'   => $section,
	'priority'  => $priority ++,
	'default'   => 0,
	'transport' => 'auto',
	'choices'   => array(
		'min'  => 0,
		'max'  => 200,
		'step' => 1,
	),
	'output'    => array(
		array(
			'element'  => '.headroom--not-top .page-header-inner',
			'property' => 'padding-bottom',
			'units'    => 'px',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'spacing',
	'settings'    => $prefix . 'item_padding',
	'label'       => esc_html__( 'Item Padding', 'tractor' ),
	'description' => esc_html__( 'Controls the navigation item level 1 padding of navigation when sticky.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'top'    => '6px',
		'bottom' => '6px',
		'left'   => '14px',
		'right'  => '14px',
	),
	'transport'   => 'auto',
	'output'      => array(
		array(
			'element'  => array(
				'.desktop-menu .headroom--not-top:not(.header-15) .menu--primary .menu__container > li > a',
				'.desktop-menu .headroom--not-top:not(.header-15) .menu--primary .menu__container > ul > li > a',
			),
			'property' => 'padding',
		),
	),
) );
