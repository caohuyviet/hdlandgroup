<?php
$section  = 'search_popup';
$priority = 1;
$prefix   = 'search_popup_';

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'textarea',
	'settings'    => $prefix . 'text',
	'label'       => esc_html__( 'Search Popup Text', 'tractor' ),
	'description' => esc_html__( 'Enter the text that displays below search field in popup search.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => esc_html__( 'Hit enter to search or ESC to close', 'tractor' ),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'background_color',
	'label'       => esc_html__( 'Background', 'tractor' ),
	'description' => esc_html__( 'Controls the background color for popup search', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => 'rgba(0, 0, 0, .85)',
	'output'      => array(
		array(
			'element'  => '.page-popup-search',
			'property' => 'background-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'text_color',
	'label'       => esc_html__( 'Text Color', 'tractor' ),
	'description' => esc_html__( 'Controls the text color in popup search', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Tractor::PRIMARY_COLOR,
	'output'      => array(
		array(
			'element'  => '
				.page-popup-search .search-field,
				.page-popup-search .search-field:focus,
				.page-popup-search .form-description',
			'property' => 'color',
		),
		array(
			'element'  => '.page-popup-search .search-field:-webkit-autofill',
			'property' => '-webkit-text-fill-color',
		),
		array(
			'element'  => '.popup-search-opened .page-popup-search .search-field',
			'property' => 'border-bottom-color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'icon_color',
	'label'       => esc_html__( 'Icon Color', 'tractor' ),
	'description' => esc_html__( 'Controls the icon color in popup search', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => '#fff',
	'output'      => array(
		array(
			'element'  => '.popup-search-close',
			'property' => 'color',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'color-alpha',
	'settings'    => $prefix . 'icon_hover_color',
	'label'       => esc_html__( 'Icon Hover Color', 'tractor' ),
	'description' => esc_html__( 'Controls the icon color when hover in popup search', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'transport'   => 'auto',
	'default'     => Tractor::PRIMARY_COLOR,
	'output'      => array(
		array(
			'element'  => '.popup-search-close:hover',
			'property' => 'color',
		),
	),
) );
