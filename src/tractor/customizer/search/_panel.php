<?php
$panel    = 'search';
$priority = 1;

Tractor_Kirki::add_section( 'search_page', array(
	'title'    => esc_html__( 'Search Page', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'search_popup', array(
	'title'    => esc_html__( 'Search Popup', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
