<?php
$panel    = 'blog';
$priority = 1;

Tractor_Kirki::add_section( 'blog_archive', array(
	'title'    => esc_html__( 'Blog Archive', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'blog_single', array(
	'title'    => esc_html__( 'Blog Single Post', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
