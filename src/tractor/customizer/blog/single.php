<?php
$section  = 'blog_single';
$priority = 1;
$prefix   = 'single_post_';

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'background',
	'settings'    => $prefix . 'banner_background',
	'label'       => esc_html__( 'Banner Background', 'tractor' ),
	'description' => esc_html__( 'Controls the background of banner in single blog posts style 02.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => array(
		'background-color'      => '#222',
		'background-image'      => TRACTOR_THEME_IMAGE_URI . '/title-bar-bg-blog.jpg',
		'background-repeat'     => 'no-repeat',
		'background-size'       => 'cover',
		'background-attachment' => 'scroll',
		'background-position'   => 'center center',
	),
	'output'      => array(
		array(
			'element' => '.entry-banner',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_feature_enable',
	'label'       => esc_html__( 'Featured Image', 'tractor' ),
	'description' => esc_html__( 'Turn on to display featured image on blog single posts.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_title_enable',
	'label'       => esc_html__( 'Post Title', 'tractor' ),
	'description' => esc_html__( 'Turn on to display the post title.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_categories_enable',
	'label'       => esc_html__( 'Categories', 'tractor' ),
	'description' => esc_html__( 'Turn on to display the categories on blog single posts.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_tags_enable',
	'label'       => esc_html__( 'Tags', 'tractor' ),
	'description' => esc_html__( 'Turn on to display the tags on blog single posts.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_date_enable',
	'label'       => esc_html__( 'Post Meta Date', 'tractor' ),
	'description' => esc_html__( 'Turn on to display the date on blog single posts.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_author_enable',
	'label'       => esc_html__( 'Post Author', 'tractor' ),
	'description' => esc_html__( 'Turn on to display the author on blog single posts.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_like_enable',
	'label'       => esc_html__( 'Post Like', 'tractor' ),
	'description' => esc_html__( 'Turn on to display the like button on blog single posts.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_view_enable',
	'label'       => esc_html__( 'Post View', 'tractor' ),
	'description' => esc_html__( 'Turn on to display the view button on blog single posts.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_comment_count_enable',
	'label'       => esc_html__( 'Comment Count', 'tractor' ),
	'description' => esc_html__( 'Turn on to display the comment count on blog single posts.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_share_enable',
	'label'       => esc_html__( 'Post Sharing', 'tractor' ),
	'description' => esc_html__( 'Turn on to display the social sharing on blog single posts.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_author_box_enable',
	'label'       => esc_html__( 'Author Info Box', 'tractor' ),
	'description' => esc_html__( 'Turn on to display the author info box on blog single posts.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_pagination_enable',
	'label'       => esc_html__( 'Previous/Next Pagination', 'tractor' ),
	'description' => esc_html__( 'Turn on to display the previous/next post pagination on blog single posts.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_related_enable',
	'label'       => esc_html__( 'Related', 'tractor' ),
	'description' => esc_html__( 'Turn on to display related posts on blog single posts.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'            => 'number',
	'settings'        => 'single_post_related_number',
	'label'           => esc_html__( 'Number of related posts item', 'tractor' ),
	'section'         => $section,
	'priority'        => $priority ++,
	'default'         => 3,
	'choices'         => array(
		'min'  => 0,
		'max'  => 50,
		'step' => 1,
	),
	'active_callback' => array(
		array(
			'setting'  => 'single_post_related_enable',
			'operator' => '==',
			'value'    => '1',
		),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'single_post_comment_enable',
	'label'       => esc_html__( 'Comments', 'tractor' ),
	'description' => esc_html__( 'Turn on to display comments on blog single posts.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '0',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );
