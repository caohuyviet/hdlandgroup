<?php
$section  = 'shop_archive';
$priority = 1;
$prefix   = 'shop_archive_';

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'select',
	'settings'    => 'shop_archive_new_days',
	'label'       => esc_html__( 'New Badge (Days)', 'tractor' ),
	'description' => esc_html__( 'If the product was published within the newness time frame display the new badge.', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '90',
	'choices'     => array(
		'0'  => esc_html__( 'None', 'tractor' ),
		'1'  => esc_html__( '1 day', 'tractor' ),
		'2'  => esc_html__( '2 days', 'tractor' ),
		'3'  => esc_html__( '3 days', 'tractor' ),
		'4'  => esc_html__( '4 days', 'tractor' ),
		'5'  => esc_html__( '5 days', 'tractor' ),
		'6'  => esc_html__( '6 days', 'tractor' ),
		'7'  => esc_html__( '7 days', 'tractor' ),
		'8'  => esc_html__( '8 days', 'tractor' ),
		'9'  => esc_html__( '9 days', 'tractor' ),
		'10' => esc_html__( '10 days', 'tractor' ),
		'15' => esc_html__( '15 days', 'tractor' ),
		'20' => esc_html__( '20 days', 'tractor' ),
		'25' => esc_html__( '25 days', 'tractor' ),
		'30' => esc_html__( '30 days', 'tractor' ),
		'60' => esc_html__( '60 days', 'tractor' ),
		'90' => esc_html__( '90 days', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'shop_archive_quick_view',
	'label'       => esc_html__( 'Quick view', 'tractor' ),
	'description' => esc_html__( 'Turn on to display the quick view button', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'shop_archive_compare',
	'label'       => esc_html__( 'Compare', 'tractor' ),
	'description' => esc_html__( 'Turn on to display the compare button', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'radio-buttonset',
	'settings'    => 'shop_archive_wishlist',
	'label'       => esc_html__( 'Wishlist', 'tractor' ),
	'description' => esc_html__( 'Turn on to display the wishlist button', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => '1',
	'choices'     => array(
		'0' => esc_html__( 'Off', 'tractor' ),
		'1' => esc_html__( 'On', 'tractor' ),
	),
) );

Tractor_Kirki::add_field( 'theme', array(
	'type'        => 'number',
	'settings'    => 'shop_archive_number_item',
	'label'       => esc_html__( 'Number items', 'tractor' ),
	'description' => esc_html__( 'Controls the number of products display on shop archive page', 'tractor' ),
	'section'     => $section,
	'priority'    => $priority ++,
	'default'     => 8,
	'choices'     => array(
		'min'  => 1,
		'max'  => 30,
		'step' => 1,
	),
) );
