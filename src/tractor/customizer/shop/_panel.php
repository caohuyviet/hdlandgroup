<?php
$panel    = 'shop';
$priority = 1;

Tractor_Kirki::add_section( 'shop_archive', array(
	'title'    => esc_html__( 'Shop Archive', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'shop_single', array(
	'title'    => esc_html__( 'Shop Single', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );

Tractor_Kirki::add_section( 'shopping_cart', array(
	'title'    => esc_html__( 'Shopping Cart', 'tractor' ),
	'panel'    => $panel,
	'priority' => $priority ++,
) );
