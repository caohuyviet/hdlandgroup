<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Tractor_VC_Icon_Tractor' ) ) {
	class Tractor_VC_Icon_Tractor {

		public function __construct() {
			/*
			 * Add styles & script file only on add new or edit post type.
			 */
			add_action( 'load-post.php', array( $this, 'enqueue_scripts' ) );
			add_action( 'load-post-new.php', array( $this, 'enqueue_scripts' ) );

			add_filter( 'vc_iconpicker-type-tractor', array( $this, 'add_fonts' ) );

			add_action( 'vc_enqueue_font_icon_element', array( $this, 'vc_element_enqueue' ) );

			add_filter( 'tractor_vc_icon_libraries', array( $this, 'add_to_libraries' ) );
		}

		public function add_to_libraries( $libraries ) {
			$libraries[ esc_html__( 'Tractor', 'tractor' ) ] = 'tractor';

			return $libraries;
		}

		public function vc_element_enqueue( $font ) {
			switch ( $font ) {
				case 'tractor':
					wp_enqueue_style( 'font-tractor', TRACTOR_THEME_URI . '/assets/fonts/tractor/font-tractor.css', null, null );
					break;
			}
		}

		public function enqueue_scripts() {
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
		}

		function admin_enqueue_scripts() {
			wp_enqueue_style( 'font-tractor', TRACTOR_THEME_URI . '/assets/fonts/tractor/font-tractor.css', null, null );
		}

		public function add_fonts( $icons ) {
			$new_icons = array(
				array( 'tractor-icon-agent' => 'tractor-icon-agent' ),
				array( 'tractor-icon-agent2' => 'tractor-icon-agent2' ),
				array( 'tractor-icon-blueprint' => 'tractor-icon-blueprint' ),
				array( 'tractor-icon-blueprint2' => 'tractor-icon-blueprint2' ),
				array( 'tractor-icon-building' => 'tractor-icon-building' ),
				array( 'tractor-icon-chat-bubble' => 'tractor-icon-chat-bubble' ),
				array( 'tractor-icon-clock' => 'tractor-icon-clock' ),
				array( 'tractor-icon-flats' => 'tractor-icon-flats' ),
				array( 'tractor-icon-gavel' => 'tractor-icon-gavel' ),
				array( 'tractor-icon-house-key' => 'tractor-icon-house-key' ),
				array( 'tractor-icon-house-plan' => 'tractor-icon-house-plan' ),
				array( 'tractor-icon-key' => 'tractor-icon-key' ),
				array( 'tractor-icon-location-pin' => 'tractor-icon-location-pin' ),
				array( 'tractor-icon-paper' => 'tractor-icon-paper' ),
				array( 'tractor-icon-percentage' => 'tractor-icon-percentage' ),
				array( 'tractor-icon-skycraper' => 'tractor-icon-skycraper' ),
			);

			return array_merge( $icons, $new_icons );
		}
	}

	new Tractor_VC_Icon_Tractor();
}
