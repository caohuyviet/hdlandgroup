<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Tractor_Metabox' ) ) {
	class Tractor_Metabox {

		/**
		 * Tractor_Metabox constructor.
		 */
		public function __construct() {
			add_filter( 'insight_core_meta_boxes', array( $this, 'register_meta_boxes' ) );
			add_filter( 'tractor_page_meta_box_presets', array( $this, 'page_meta_box_presets' ) );
		}

		public function page_meta_box_presets( $presets ) {
			$presets[] = 'color_preset';

			return $presets;
		}

		/**
		 * Register Metabox
		 *
		 * @param $meta_boxes
		 *
		 * @return array
		 */
		public function register_meta_boxes( $meta_boxes ) {
			$page_registered_sidebars = Tractor_Helper::get_registered_sidebars( true );

			$general_options = array(
				array(
					'title'  => esc_html__( 'General', 'tractor' ),
					'fields' => array(
						array(
							'id'      => 'site_layout',
							'type'    => 'select',
							'title'   => esc_html__( 'Layout', 'tractor' ),
							'desc'    => 'When you choose Default, the value in <a href="' . admin_url( 'customize.php' ) . '" target="_blank">Customizer</a> will be used.',
							'options' => array(
								''      => esc_html__( 'Default', 'tractor' ),
								'boxed' => esc_html__( 'Boxed', 'tractor' ),
								'wide'  => esc_html__( 'Wide', 'tractor' ),
							),
							'default' => '',
						),
						array(
							'id'    => 'site_width',
							'type'  => 'text',
							'title' => esc_html__( 'Site Width', 'tractor' ),
							'desc'  => esc_html__( 'Controls the site width for this page. Enter value including any valid CSS unit, e.g 1200px. Leave blank to use global setting.', 'tractor' ),
						),
						array(
							'id'      => 'content_padding',
							'type'    => 'switch',
							'title'   => esc_html__( 'Content Padding', 'tractor' ),
							'desc'    => 'By default, the page content has the padding top & bottom is 100px.',
							'default' => '1',
							'options' => array(
								'1'      => esc_html__( 'Default', 'tractor' ),
								'0'      => esc_html__( 'No Padding', 'tractor' ),
								'top'    => esc_html__( 'No Top Padding', 'tractor' ),
								'bottom' => esc_html__( 'No Bottom Padding', 'tractor' ),
							),
						),
						array(
							'id'      => 'body_class',
							'type'    => 'text',
							'title'   => esc_html__( 'Body Class', 'tractor' ),
							'desc'    => esc_html__( 'Add a class name to body and refer to it in custom CSS.', 'tractor' ),
							'default' => '',
						),
					),
				),
				array(
					'title'  => esc_html__( 'Color', 'tractor' ),
					'fields' => array(
						array(
							'id'      => 'color_preset',
							'type'    => 'select',
							'title'   => esc_html__( 'Preset', 'tractor' ),
							'desc'    => esc_html__( 'Select a preset of color for this page.', 'tractor' ),
							'options' => array(
								'-1' => esc_html__( 'None', 'tractor' ),
								'01' => esc_html__( 'Preset 01', 'tractor' ),
								'02' => esc_html__( 'Preset 02', 'tractor' ),
								'03' => esc_html__( 'Preset 03', 'tractor' ),
								'04' => esc_html__( 'Preset 04', 'tractor' ),
								'05' => esc_html__( 'Preset 05', 'tractor' ),
								'06' => esc_html__( 'Preset 06', 'tractor' ),
								'07' => esc_html__( 'Preset 07', 'tractor' ),
								'08' => esc_html__( 'Preset 08', 'tractor' ),
								'10' => esc_html__( 'Preset 10', 'tractor' ),
								'11' => esc_html__( 'Preset 11', 'tractor' ),
							),
							'default' => '-1',
						),
					),
				),
				array(
					'title'  => esc_html__( 'Background', 'tractor' ),
					'fields' => array(
						array(
							'id'      => 'site_background_message',
							'type'    => 'message',
							'title'   => esc_html__( 'Info', 'tractor' ),
							'message' => esc_html__( 'These options controls the background on boxed mode.', 'tractor' ),
						),
						array(
							'id'    => 'site_background_color',
							'type'  => 'color',
							'title' => esc_html__( 'Background Color', 'tractor' ),
							'desc'  => esc_html__( 'Controls the background color of the outer background area in boxed mode of this page.', 'tractor' ),
						),
						array(
							'id'    => 'site_background_image',
							'type'  => 'media',
							'title' => esc_html__( 'Background Image', 'tractor' ),
							'desc'  => esc_html__( 'Controls the background image of the outer background area in boxed mode of this page.', 'tractor' ),
						),
						array(
							'id'      => 'site_background_repeat',
							'type'    => 'select',
							'title'   => esc_html__( 'Background Repeat', 'tractor' ),
							'desc'    => esc_html__( 'Controls the background repeat of the outer background area in boxed mode of this page.', 'tractor' ),
							'options' => array(
								'no-repeat' => esc_html__( 'No repeat', 'tractor' ),
								'repeat'    => esc_html__( 'Repeat', 'tractor' ),
								'repeat-x'  => esc_html__( 'Repeat X', 'tractor' ),
								'repeat-y'  => esc_html__( 'Repeat Y', 'tractor' ),
							),
						),
						array(
							'id'      => 'site_background_attachment',
							'type'    => 'select',
							'title'   => esc_html__( 'Background Attachment', 'tractor' ),
							'desc'    => esc_html__( 'Controls the background attachment of the outer background area in boxed mode of this page.', 'tractor' ),
							'options' => array(
								''       => esc_html__( 'Default', 'tractor' ),
								'fixed'  => esc_html__( 'Fixed', 'tractor' ),
								'scroll' => esc_html__( 'Scroll', 'tractor' ),
							),
						),
						array(
							'id'    => 'site_background_position',
							'type'  => 'text',
							'title' => esc_html__( 'Background Position', 'tractor' ),
							'desc'  => esc_html__( 'Controls the background position of the outer background area in boxed mode of this page.', 'tractor' ),
						),
						array(
							'id'    => 'site_background_size',
							'type'  => 'text',
							'title' => esc_html__( 'Background Size', 'tractor' ),
							'desc'  => esc_html__( 'Controls the background size of the outer background area in boxed mode of this page.', 'tractor' ),
						),
						array(
							'id'      => 'content_background_message',
							'type'    => 'message',
							'title'   => esc_html__( 'Info', 'tractor' ),
							'message' => esc_html__( 'These options controls the background of main content on this page.', 'tractor' ),
						),
						array(
							'id'    => 'content_background_color',
							'type'  => 'color',
							'title' => esc_html__( 'Background Color', 'tractor' ),
							'desc'  => esc_html__( 'Controls the background color of main content on this page.', 'tractor' ),
						),
						array(
							'id'    => 'content_background_image',
							'type'  => 'media',
							'title' => esc_html__( 'Background Image', 'tractor' ),
							'desc'  => esc_html__( 'Controls the background image of main content on this page.', 'tractor' ),
						),
						array(
							'id'      => 'content_background_repeat',
							'type'    => 'select',
							'title'   => esc_html__( 'Background Repeat', 'tractor' ),
							'desc'    => esc_html__( 'Controls the background repeat of main content on this page.', 'tractor' ),
							'options' => array(
								'no-repeat' => esc_html__( 'No repeat', 'tractor' ),
								'repeat'    => esc_html__( 'Repeat', 'tractor' ),
								'repeat-x'  => esc_html__( 'Repeat X', 'tractor' ),
								'repeat-y'  => esc_html__( 'Repeat Y', 'tractor' ),
							),
						),
						array(
							'id'    => 'content_background_position',
							'type'  => 'text',
							'title' => esc_html__( 'Background Position', 'tractor' ),
							'desc'  => esc_html__( 'Controls the background position of main content on this page.', 'tractor' ),
						),
					),
				),
				array(
					'title'  => esc_html__( 'Header', 'tractor' ),
					'fields' => array(
						array(
							'id'      => 'top_bar_type',
							'type'    => 'select',
							'title'   => esc_html__( 'Top Bar Type', 'tractor' ),
							'desc'    => 'When you choose Default, the value in <a href="' . admin_url( 'customize.php' ) . '" target="_blank">Customizer</a> will be used.',
							'default' => '',
							'options' => Tractor_Helper::get_top_bar_list( true ),
						),
						array(
							'id'      => 'header_type',
							'type'    => 'select',
							'title'   => esc_html__( 'Header Type', 'tractor' ),
							'desc'    => 'When you choose Default, the value in <a href="' . admin_url( 'customize.php' ) . '" target="_blank">Customizer</a> will be used.',
							'default' => '',
							'options' => Tractor_Helper::get_header_list( true ),
						),
						array(
							'id'      => 'menu_display',
							'type'    => 'select',
							'title'   => esc_html__( 'Primary Menu', 'tractor' ),
							'desc'    => esc_html__( 'Select which menu displays on this page.', 'tractor' ),
							'default' => '',
							'options' => Tractor_Helper::get_all_menus(),
						),
						array(
							'id'      => 'menu_one_page',
							'type'    => 'switch',
							'title'   => esc_html__( 'One Page Menu', 'tractor' ),
							'default' => '0',
							'options' => array(
								'0' => esc_html__( 'Disable', 'tractor' ),
								'1' => esc_html__( 'Enable', 'tractor' ),
							),
						),
						array(
							'id'      => 'custom_logo',
							'type'    => 'media',
							'title'   => esc_html__( 'Custom Logo', 'tractor' ),
							'desc'    => esc_html__( 'Select custom logo for this page.', 'tractor' ),
							'default' => '',
						),
						array(
							'id'      => 'custom_logo_retina',
							'type'    => 'media',
							'title'   => esc_html__( 'Custom Logo Retina', 'tractor' ),
							'desc'    => esc_html__( 'Select custom logo retina for this page.', 'tractor' ),
							'default' => '',
						),
						array(
							'id'      => 'custom_logo_width',
							'type'    => 'text',
							'title'   => esc_html__( 'Custom Logo Width', 'tractor' ),
							'desc'    => esc_html__( 'Controls the width of logo, e.g 190px', 'tractor' ),
							'default' => '',
						),
						array(
							'id'      => 'custom_sticky_logo_width',
							'type'    => 'text',
							'title'   => esc_html__( 'Custom Sticky Logo Width', 'tractor' ),
							'desc'    => esc_html__( 'Controls the width of sticky logo, e.g 150px', 'tractor' ),
							'default' => '',
						),
					),
				),
				array(
					'title'  => esc_html__( 'Title', 'tractor' ),
					'fields' => array(
						array(
							'id'      => 'page_title_bar_layout',
							'type'    => 'select',
							'title'   => esc_html__( 'Layout', 'tractor' ),
							'desc'    => 'When you choose Default, the value in <a href="' . admin_url( 'customize.php' ) . '" target="_blank">Customizer</a> will be used.',
							'default' => 'default',
							'options' => Tractor_Helper::get_title_bar_list( true ),
						),
						array(
							'id'      => 'page_title_bar_background_color',
							'type'    => 'color',
							'title'   => esc_html__( 'Background Color', 'tractor' ),
							'default' => '',
						),
						array(
							'id'      => 'page_title_bar_background',
							'type'    => 'media',
							'title'   => esc_html__( 'Background Image', 'tractor' ),
							'default' => '',
						),
						array(
							'id'      => 'page_title_bar_background_overlay',
							'type'    => 'color',
							'title'   => esc_html__( 'Background Overlay', 'tractor' ),
							'default' => '',
						),
						array(
							'id'    => 'page_title_bar_custom_heading',
							'type'  => 'text',
							'title' => esc_html__( 'Custom Title', 'tractor' ),
							'desc'  => esc_html__( 'Insert custom heading for the page title bar. Leave blank to use default.', 'tractor' ),
						),
					),
				),
				array(
					'title'  => esc_html__( 'Sidebar', 'tractor' ),
					'fields' => array(
						array(
							'id'      => 'page_sidebar_position',
							'type'    => 'switch',
							'title'   => esc_html__( 'Position', 'tractor' ),
							'desc'    => 'When you choose Default, the value in <a href="' . admin_url( 'customize.php' ) . '" target="_blank">Customizer</a> will be used.',
							'default' => 'default',
							'options' => Tractor_Helper::get_list_sidebar_positions( true ),
						),
						array(
							'id'      => 'page_sidebar_1',
							'type'    => 'select',
							'title'   => esc_html__( 'Sidebar 1', 'tractor' ),
							'desc'    => esc_html__( 'Select sidebar 1 that will display on this page.', 'tractor' ),
							'default' => 'default',
							'options' => $page_registered_sidebars,
						),
						array(
							'id'      => 'page_sidebar_2',
							'type'    => 'select',
							'title'   => esc_html__( 'Sidebar 2', 'tractor' ),
							'desc'    => esc_html__( 'Select sidebar 2 that will display on this page.', 'tractor' ),
							'default' => 'default',
							'options' => $page_registered_sidebars,
						),
					),
				),
				array(
					'title'  => esc_html__( 'Slider', 'tractor' ),
					'fields' => array(
						array(
							'id'      => 'revolution_slider',
							'type'    => 'select',
							'title'   => esc_html__( 'Revolution Slider', 'tractor' ),
							'desc'    => esc_html__( 'Select the unique name of the slider.', 'tractor' ),
							'options' => Tractor_Helper::get_list_revslider(),
						),
						array(
							'id'      => 'slider_position',
							'type'    => 'select',
							'title'   => esc_html__( 'Position', 'tractor' ),
							'default' => 'below',
							'options' => array(
								'above' => esc_html__( 'Above Header', 'tractor' ),
								'below' => esc_html__( 'Below Header', 'tractor' ),
							),
						),
					),
				),
				array(
					'title'  => esc_html__( 'Footer', 'tractor' ),
					'fields' => array(
						array(
							'id'      => 'footer_page',
							'type'    => 'select',
							'title'   => esc_html__( 'Footer', 'tractor' ),
							'desc'    => 'When you choose Default, the value in <a href="' . admin_url( 'customize.php' ) . '" target="_blank">Customizer</a> will be used.',
							'default' => 'default',
							'options' => Tractor_Footer::get_list_footers( true ),
						),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_page_options',
				'title'      => esc_html__( 'Page Options', 'tractor' ),
				'post_types' => array( 'page' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => $general_options,
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_post_options',
				'title'      => esc_html__( 'Page Options', 'tractor' ),
				'post_types' => array( 'post' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array_merge( array(
							array(
								'title'  => esc_html__( 'Post', 'tractor' ),
								'fields' => array(
									array(
										'id'    => 'post_gallery',
										'type'  => 'gallery',
										'title' => esc_html__( 'Gallery Format', 'tractor' ),
									),
									array(
										'id'    => 'post_video',
										'type'  => 'textarea',
										'title' => esc_html__( 'Video Format', 'tractor' ),
									),
									array(
										'id'    => 'post_audio',
										'type'  => 'textarea',
										'title' => esc_html__( 'Audio Format', 'tractor' ),
									),
									array(
										'id'    => 'post_quote_text',
										'type'  => 'text',
										'title' => esc_html__( 'Quote Format - Source Text', 'tractor' ),
									),
									array(
										'id'    => 'post_quote_name',
										'type'  => 'text',
										'title' => esc_html__( 'Quote Format - Source Name', 'tractor' ),
									),
									array(
										'id'    => 'post_quote_url',
										'type'  => 'text',
										'title' => esc_html__( 'Quote Format - Source Url', 'tractor' ),
									),
									array(
										'id'    => 'post_link',
										'type'  => 'text',
										'title' => esc_html__( 'Link Format', 'tractor' ),
									),
								),
							),
						), $general_options ),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_product_options',
				'title'      => esc_html__( 'Page Options', 'tractor' ),
				'post_types' => array( 'product' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => $general_options,
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_project_options',
				'title'      => esc_html__( 'Page Options', 'tractor' ),
				'post_types' => array( 'project' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array_merge( array(
							array(
								'title'  => esc_html__( 'Project', 'tractor' ),
								'fields' => array(
									array(
										'id'      => 'project_message',
										'type'    => 'message',
										'title'   => esc_html__( 'Info', 'tractor' ),
										'message' => esc_html__( 'These options just use for the project.', 'tractor' ),
									),
									array(
										'id'      => 'project_vi_tri',
										'type'    => 'text',
										'title'   => esc_html__( 'Vị trí', 'tractor' ),
										'default' => '',
									),
									array(
										'id'      => 'project_quy_mo',
										'type'    => 'text',
										'title'   => esc_html__( 'Quy mô', 'tractor' ),
										'default' => '',
									),
									array(
										'id'      => 'project_tong_dien_tich',
										'type'    => 'text',
										'title'   => esc_html__( 'Tổng diện tích', 'tractor' ),
										'default' => '',
									),
								),
							),
						), $general_options ),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_service_options',
				'title'      => esc_html__( 'Page Options', 'tractor' ),
				'post_types' => array( 'service' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array_merge( array(
							array(
								'title'  => esc_html__( 'Service', 'tractor' ),
								'fields' => array(
									array(
										'id'      => 'service_message',
										'type'    => 'message',
										'title'   => esc_html__( 'Info', 'tractor' ),
										'message' => esc_html__( 'These options just use for the service.', 'tractor' ),
									),
									array(
										'id'      => 'service_style',
										'type'    => 'select',
										'title'   => esc_html__( 'Style', 'tractor' ),
										'desc'    => esc_html__( 'Select style for the single service page.', 'tractor' ),
										'default' => '',
										'options' => array(
											''   => esc_html__( 'Default', 'tractor' ),
											'01' => esc_html__( 'Style 01', 'tractor' ),
											'02' => esc_html__( 'Style 02', 'tractor' ),
										),
									),
									array(
										'id'      => 'service_icon',
										'type'    => 'text',
										'title'   => esc_html__( 'Icon', 'tractor' ),
										'desc'    => esc_html__( 'Add the icon for this service.', 'tractor' ),
										'default' => '',
									),
								),
							),
						), $general_options ),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_testimonial_options',
				'title'      => esc_html__( 'Testimonial Options', 'tractor' ),
				'post_types' => array( 'testimonial' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array(
							array(
								'title'  => esc_html__( 'Testimonial Details', 'tractor' ),
								'fields' => array(
									array(
										'id'      => 'by_line',
										'type'    => 'text',
										'title'   => esc_html__( 'By Line', 'tractor' ),
										'desc'    => esc_html__( 'Enter a byline for the customer giving this testimonial (for example: "CEO of Thememove").', 'tractor' ),
										'default' => '',
									),
									array(
										'id'      => 'url',
										'type'    => 'text',
										'title'   => esc_html__( 'Url', 'tractor' ),
										'desc'    => esc_html__( 'Enter a URL that applies to this customer (for example: http://www.thememove.com/).', 'tractor' ),
										'default' => '',
									),
									array(
										'id'      => 'rating',
										'type'    => 'select',
										'title'   => esc_html__( 'Rating', 'tractor' ),
										'default' => '',
										'options' => array(
											''  => esc_html__( 'Select a rating', 'tractor' ),
											'1' => esc_html__( '1 Star', 'tractor' ),
											'2' => esc_html__( '2 Stars', 'tractor' ),
											'3' => esc_html__( '3 Stars', 'tractor' ),
											'4' => esc_html__( '4 Stars', 'tractor' ),
											'5' => esc_html__( '5 Stars', 'tractor' ),
										),
									),
								),
							),
						),
					),
				),
			);

			$meta_boxes[] = array(
				'id'         => 'insight_footer_options',
				'title'      => esc_html__( 'Footer Options', 'tractor' ),
				'post_types' => array( 'ic_footer' ),
				'context'    => 'normal',
				'priority'   => 'high',
				'fields'     => array(
					array(
						'type'  => 'tabpanel',
						'items' => array(
							array(
								'title'  => esc_html__( 'Effect', 'tractor' ),
								'fields' => array(
									array(
										'id'      => 'effect',
										'type'    => 'switch',
										'title'   => esc_html__( 'Effect', 'tractor' ),
										'default' => '',
										'options' => array(
											''         => esc_html__( 'Normal', 'tractor' ),
											'parallax' => esc_html__( 'Parallax', 'tractor' ),
										),
									),
								),
							),
							array(
								'title'  => esc_html__( 'Styling', 'tractor' ),
								'fields' => array(
									array(
										'id'      => 'style',
										'type'    => 'select',
										'title'   => esc_html__( 'Style', 'tractor' ),
										'default' => '01',
										'options' => array(
											'01' => esc_html__( 'Style 01', 'tractor' ),
											'02' => esc_html__( 'Style 02', 'tractor' ),
											'03' => esc_html__( 'Style 03', 'tractor' ),
											'04' => esc_html__( 'Style 04', 'tractor' ),
											'05' => esc_html__( 'Style 05', 'tractor' ),
											'06' => esc_html__( 'Style 06', 'tractor' ),
											'07' => esc_html__( 'Style 07', 'tractor' ),
											'08' => esc_html__( 'Style 08', 'tractor' ),
											'10' => esc_html__( 'Style 10', 'tractor' ),
											'11' => esc_html__( 'Style 11', 'tractor' ),
										),
									),
								),
							),
						),
					),
				),
			);

			return $meta_boxes;
		}

	}

	new Tractor_Metabox();
}
