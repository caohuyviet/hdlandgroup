<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue scripts and styles.
 */
if ( ! class_exists( 'Tractor_Enqueue' ) ) {
	class Tractor_Enqueue {

		protected static $instance = null;

		public function __construct() {
			add_action( 'enqueue_block_editor_assets', array( $this, 'gutenberg' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ) );
		}

		function gutenberg() {
			wp_enqueue_style( 'gutenberg-google-font', 'https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i', null, null );
			wp_enqueue_style( 'gutenberg-editor', TRACTOR_THEME_URI . '/gutenberg-style.css', null, null );
		}


		function requeue_wpcf7_scripts() {
			global $post;
			if ( is_a( $post, 'WP_Post' ) &&
			     ( has_shortcode( $post->post_content, 'contact-form-7' ) ||
			       has_shortcode( $post->post_content, 'tm_contact_form_7' ) ||
			       has_shortcode( $post->post_content, 'tm_contact_form_7_box' )
			     )
			) {
				if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
					wpcf7_enqueue_scripts();
				}

				if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
					wpcf7_enqueue_styles();
				}
			}
		}

		public static function instance() {
			if ( null === self::$instance ) {
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Enqueue scripts & styles.
		 *
		 * @access public
		 */
		public function enqueue() {
			$post_type = get_post_type();

			wp_enqueue_style( 'font-tractor', TRACTOR_THEME_URI . '/assets/fonts/tractor/font-tractor.css', null, null );
			wp_enqueue_style( 'font-icomoon', TRACTOR_THEME_URI . '/assets/fonts/icomoon/font-icomoon.css', null, null );
			wp_register_style( 'font-ion', TRACTOR_THEME_URI . '/assets/fonts/ion/font-ion.min.css', null, null );

			wp_register_style( 'spinkit', TRACTOR_THEME_URI . '/assets/libs/spinkit/spinkit.css', null, null );

			wp_register_style( 'justified-gallery', TRACTOR_THEME_URI . '/assets/libs/justified-gallery/justifiedGallery.min.css', null, '3.6.3' );
			wp_register_script( 'justified-gallery', TRACTOR_THEME_URI . '/assets/libs/justified-gallery/jquery.justifiedGallery.min.js', array( 'jquery' ), '3.6.3', true );

			wp_register_script( 'picturefill', TRACTOR_THEME_URI . '/assets/libs/picturefill/picturefill.min.js', array( 'jquery' ), null, true );
			wp_register_script( 'mousewheel', TRACTOR_THEME_URI . "/assets/libs/mousewheel/jquery.mousewheel.js", array( 'jquery' ), TRACTOR_THEME_VERSION, true );
			wp_enqueue_style( 'lightgallery', TRACTOR_THEME_URI . '/assets/libs/light-gallery/css/lightgallery.min.css', null, '1.6.4' );
			wp_enqueue_script( 'lightgallery', TRACTOR_THEME_URI . "/assets/libs/light-gallery/js/lightgallery-all.js", array(
				'jquery',
				'picturefill',
				'mousewheel',
			), null, true );

			wp_register_style( 'lightslider', TRACTOR_THEME_URI . '/assets/libs/light-slider/css/lightslider.min.css', null, '1.1.6' );
			wp_register_script( 'lightslider', TRACTOR_THEME_URI . "/assets/js/lightslider.js", array( 'jquery' ), '1.1.6', true );

			wp_register_style( 'swiper', TRACTOR_THEME_URI . '/assets/libs/swiper/css/swiper.min.css', null, '4.0.3' );
			wp_register_script( 'swiper', TRACTOR_THEME_URI . "/assets/libs/swiper/js/swiper.js", array( 'jquery' ), '4.0.3', true );

			// Fix VC waypoints.
			if ( ! wp_script_is( 'vc_waypoints', 'registered' ) ) {
				wp_register_script( 'vc_waypoints', TRACTOR_THEME_URI . '/assets/libs/vc-waypoints/vc-waypoints.min.js', array( 'jquery' ), null, true );
			}

			// Register scripts
			wp_register_script( 'slimscroll', TRACTOR_THEME_URI . '/assets/libs/slimscroll/jquery.slimscroll.min.js', array( 'jquery' ), '1.3.8', true );
			wp_register_script( 'easing', TRACTOR_THEME_URI . '/assets/libs/easing/jquery.easing.min.js', array( 'jquery' ), '1.3', true );
			wp_register_script( 'matchheight', TRACTOR_THEME_URI . '/assets/libs/matchheight/jquery.matchheight-min.js', array( 'jquery' ), TRACTOR_THEME_VERSION, true );
			wp_register_script( 'gmap3', TRACTOR_THEME_URI . '/assets/libs/gmap3/gmap3.min.js', array( 'jquery' ), TRACTOR_THEME_VERSION, true );
			wp_register_script( 'countdown', TRACTOR_THEME_URI . '/assets/libs/jquery.countdown/js/jquery.countdown.min.js', array( 'jquery' ), TRACTOR_THEME_VERSION, true );
			wp_register_script( 'typed', TRACTOR_THEME_URI . '/assets/libs/typed/typed.min.js', array( 'jquery' ), null, true );

			// Fix WordPress old version not registered this script
			if ( ! wp_script_is( 'imagesloaded', 'registered' ) ) {
				wp_register_script( 'imagesloaded', TRACTOR_THEME_URI . '/assets/libs/imagesloaded/imagesloaded.min.js', array( 'jquery' ), null, true );
			}

			wp_register_script( 'isotope-masonry', TRACTOR_THEME_URI . '/assets/libs/isotope/js/isotope.pkgd.min.js', array( 'jquery' ), TRACTOR_THEME_VERSION, true );
			wp_register_script( 'isotope-packery', TRACTOR_THEME_URI . '/assets/libs/packery-mode/packery-mode.pkgd.min.js', array(
				'jquery',
				'imagesloaded',
				'isotope-masonry',
			), TRACTOR_THEME_VERSION, true );

			wp_register_script( 'smooth-scroll', TRACTOR_THEME_URI . '/assets/libs/smooth-scroll-for-web/SmoothScroll.min.js', array(
				'jquery',
			), '1.4.6', true );

			wp_register_script( 'smooth-scroll-for-web', TRACTOR_THEME_URI . "/assets/libs/smooth-scroll-for-web/SmoothScroll.min.js", array(), TRACTOR_THEME_VERSION, true );

			wp_register_script( 'lazyload', TRACTOR_THEME_URI . "/assets/libs/lazyload/lazyload.js", array(), TRACTOR_THEME_VERSION, true );

			wp_register_script( 'odometer', TRACTOR_THEME_URI . '/assets/libs/odometer/odometer.min.js', array(
				'jquery',
			), TRACTOR_THEME_VERSION, true );

			wp_register_script( 'counter-up', TRACTOR_THEME_URI . '/assets/libs/countTo/jquery.countTo.js', array(
				'jquery',
			), TRACTOR_THEME_VERSION, true );

			// Counter JS by Tractor theme
			wp_register_script( 'tractor-counter', TRACTOR_THEME_URI . "/assets/js/counter.js", array(
				'jquery',
			), TRACTOR_THEME_VERSION, true );

			wp_register_script( 'chart', TRACTOR_THEME_URI . '/assets/libs/chart/chart.min.js', array(
				'jquery',
			), TRACTOR_THEME_VERSION, true );

			// Chart JS by Tractor theme
			wp_register_script( 'tractor-chart', TRACTOR_THEME_URI . "/assets/js/chart.js", array(
				'jquery',
				'vc_waypoints',
				'chart',
			), TRACTOR_THEME_VERSION, true );

			wp_register_script( 'circle-progress', TRACTOR_THEME_URI . '/assets/libs/circle-progress/circle-progress.min.js', array( 'jquery' ), null, true );

			// Circle Progress JS by Tractor theme
			wp_register_script( 'tractor-circle-progress', TRACTOR_THEME_URI . "/assets/js/circle-progress.js", array(
				'jquery',
				'vc_waypoints',
				'circle-progress',
			), null, true );

			wp_register_script( 'tractor-pricing', TRACTOR_THEME_URI . "/assets/js/pricing.js", array(
				'jquery',
				'matchheight',
			), null, true );

			// Accordion JS by Tractor theme
			wp_register_script( 'tractor-accordion', TRACTOR_THEME_URI . "/assets/js/accordion.js", array( 'jquery' ), TRACTOR_THEME_VERSION, true );

			// Enqueue the theme's style.css
			wp_enqueue_style( 'tractor-style', get_stylesheet_uri() );
			wp_enqueue_style( 'font-ion' );
			wp_enqueue_style( 'swiper' );
			wp_enqueue_style( 'spinkit' );

			// Register scripts
			if ( Tractor::setting( 'header_sticky_enable' ) ) {
				wp_enqueue_script( 'headroom', TRACTOR_THEME_URI . "/assets/js/headroom.js", array( 'jquery' ), TRACTOR_THEME_VERSION, true );
			}

			if ( Tractor::setting( 'smooth_scroll_enable' ) ) {
				// Smooth scroll bar
				wp_enqueue_script( 'smooth-scroll' );
			}

			// Smooth scroll link
			wp_enqueue_script( 'jquery-smooth-scroll', TRACTOR_THEME_URI . '/assets/libs/smooth-scroll/jquery.smooth-scroll.min.js', array( 'jquery' ), TRACTOR_THEME_VERSION, true );

			wp_enqueue_script( 'swiper' );
			wp_enqueue_script( 'matchheight' );
			wp_enqueue_script( 'vc_waypoints' );
			wp_enqueue_script( 'smartmenus', TRACTOR_THEME_URI . "/assets/libs/smartmenus/jquery.smartmenus.js", array( 'jquery' ), TRACTOR_THEME_VERSION, true );

			if ( Tractor::setting( 'lazy_load_images' ) ) {
				wp_enqueue_script( 'lazyload' );
			}

			// The comment-reply script
			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				if ( $post_type === 'post' ) {
					if ( Tractor::setting( 'single_post_comment_enable' ) === '1' ) {
						wp_enqueue_script( 'comment-reply' );
					}
				} elseif ( $post_type === 'project' ) {
					if ( Tractor::setting( 'single_project_comment_enable' ) === '1' ) {
						wp_enqueue_script( 'comment-reply' );
					}
				} elseif ( $post_type === 'service' ) {
					if ( Tractor::setting( 'single_service_comment_enable' ) === '1' ) {
						wp_enqueue_script( 'comment-reply' );
					}
				} else {
					wp_enqueue_script( 'comment-reply' );
				}
			}

			// Load Visual Composer JS above theme JS
			wp_enqueue_script( 'wpb_composer_front_js' );

			// Enqueue main JS
			wp_enqueue_script( 'tractor-script', TRACTOR_THEME_URI . "/assets/js/main.js", array(
				'jquery',
			), TRACTOR_THEME_VERSION, true );

			if ( Tractor_Helper::active_woocommerce() ) {
				wp_enqueue_script( 'tractor-woo', TRACTOR_THEME_URI . "/assets/js/woo.js", array(
					'tractor-script',
				), TRACTOR_THEME_VERSION, true );
			}

			// Enqueue custom variable JS
			$js_variables = array(
				'templateUrl'               => TRACTOR_THEME_URI,
				'ajaxurl'                   => admin_url( 'admin-ajax.php' ),
				'primary_color'             => Tractor::setting( 'primary_color' ),
				'header_sticky_enable'      => Tractor::setting( 'header_sticky_enable' ),
				'scroll_top_enable'         => Tractor::setting( 'scroll_top_enable' ),
				'lazyLoadImages'            => Tractor::setting( 'lazy_load_images' ),
				'light_gallery_auto_play'   => Tractor::setting( 'light_gallery_auto_play' ),
				'light_gallery_download'    => Tractor::setting( 'light_gallery_download' ),
				'light_gallery_full_screen' => Tractor::setting( 'light_gallery_full_screen' ),
				'light_gallery_zoom'        => Tractor::setting( 'light_gallery_zoom' ),
				'light_gallery_thumbnail'   => Tractor::setting( 'light_gallery_thumbnail' ),
				'light_gallery_share'       => Tractor::setting( 'light_gallery_share' ),
				'mobile_menu_breakpoint'    => Tractor::setting( 'mobile_menu_breakpoint' ),
				'isSingleProduct'           => is_singular( 'product' ),
				'like'                      => esc_html__( 'Like', 'tractor' ),
				'unlike'                    => esc_html__( 'Unlike', 'tractor' ),
			);
			wp_localize_script( 'tractor-script', '$insight', $js_variables );

			// Custom JS
			if ( Tractor::setting( 'custom_js_enable' ) == 1 ) {
				wp_add_inline_script( 'tractor-script', html_entity_decode( Tractor::setting( 'custom_js' ) ) );
			}
		}
	}

	new Tractor_Enqueue();
}
