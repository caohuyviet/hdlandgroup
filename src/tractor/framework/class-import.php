<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Initial OneClick import for this theme
 */
if ( ! class_exists( 'Tractor_Import' ) ) {
	class Tractor_Import {

		public function __construct() {
			add_filter( 'insight_core_import_demos', array( $this, 'import_demos' ) );
			//add_filter( 'insight_core_import_generate_thumb', array( $this, 'import_generate_thumb' ) );
		}

		public function import_demos() {
			return array(
				'tractor'  => array(
					'screenshot' => TRACTOR_THEME_URI . '/screenshot.jpg',
					'name'       => TRACTOR_THEME_NAME,
					'url'        => 'https://api.thememove.com/import/tractor/tractor-tractor.zip',
				),
				'tractor2' => array(
					'screenshot' => TRACTOR_THEME_URI . '/screenshot.jpg',
					'name'       => TRACTOR_THEME_NAME . ' (Dropbox)',
					'url'        => 'https://www.dropbox.com/s/a1gm4b6b8u0fn0d/tractor-tractor.zip?dl=1',
				),
			);
		}

		/**
		 * Generate thumbnail while importing
		 */
		function import_generate_thumb() {
			return true;
		}
	}

	new Tractor_Import();
}
