<?php
// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Plugin installation and activation for WordPress themes
 */
if ( ! class_exists( 'Tractor_Register_Plugins' ) ) {
	class Tractor_Register_Plugins {

		public function __construct() {
			add_filter( 'insight_core_tgm_plugins', array( $this, 'register_required_plugins' ) );
		}

		public function register_required_plugins() {
			/*
			 * Array of plugin arrays. Required keys are name and slug.
			 * If the source is NOT from the .org repo, then source is also required.
			 */
			$plugins = array(
				array(
					'name'     => esc_html__( 'Insight Core', 'tractor' ),
					'slug'     => 'insight-core',
					'source'   => $this->get_plugin_source_url( 'insight-core-2.2.0-8CbQzkCQZz.zip' ),
					'version'  => '2.2.0',
					'required' => true,
				),
				array(
					'name'     => esc_html__( 'WPBakery Page Builder', 'tractor' ),
					'slug'     => 'js_composer',
					'source'   => $this->get_plugin_source_url( 'js_composer-6.6.0-GTz1qxX6Xb.zip' ),
					'version'  => '6.6.0',
					'required' => true,
				),
				array(
					'name'     => esc_html__( 'WPBakery Page Builder (Visual Composer) Clipboard', 'tractor' ),
					'slug'     => 'vc_clipboard',
					'source'  => $this->get_plugin_source_url( 'vc_clipboard-4.5.8-6x4EjSaacf.zip' ),
					'version' => '4.5.8',
					'required' => false,
				),
				array(
					'name'     => esc_html__( 'Contact Form 7', 'tractor' ),
					'slug'     => 'contact-form-7',
					'required' => false,
				),
				array(
					'name'     => esc_html__( 'MailChimp for WordPress', 'tractor' ),
					'slug'     => 'mailchimp-for-wp',
					'required' => false,
				),
				array(
					'name'     => esc_html__( 'Revolution Slider', 'tractor' ),
					'slug'     => 'revslider',
					'source'   => $this->get_plugin_source_url( 'revslider-6.4.11-mJl1BiurFh.zip' ),
					'version'  => '6.4.11',
					'required' => true,
				),
			);

			return $plugins;
		}

		public function get_plugin_source_url( $file_name ) {
			return 'https://api.thememove.com/download/' . $file_name;
		}

	}

	new Tractor_Register_Plugins();
}
