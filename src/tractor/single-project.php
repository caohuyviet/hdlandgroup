<?php
/**
 * The template for displaying all single Project posts.
 *
 * @package Tractor
 * @since   1.0
 */
get_header();
?>
<?php Tractor_Templates::title_bar(); ?>

	<div id="page-content" class="page-content">

		<?php /*if ( has_post_thumbnail() ) { */?><!--
			<div class="project-feature post-thumbnail"
			     style="background-image: url(<?php /*echo get_the_post_thumbnail_url( get_the_ID(), 'full' ); */?>);"></div>
		--><?php /*} */?>

		<div class="container">
			<div class="row">

				<?php Tractor_Templates::render_sidebar( 'left' ); ?>

				<div class="page-main-content">

					<?php while ( have_posts() ) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php the_content(); ?>
						</article>

						<?php
						// If comments are open or we have at least one comment, load up the comment template.
						if ( Tractor::setting( 'single_project_comment_enable' ) === '1' && ( comments_open() || get_comments_number() ) ) :
							comments_template();
						endif;
						?>
					<?php endwhile; ?>
				</div>

				<?php Tractor_Templates::render_sidebar( 'right' ); ?>

			</div>
		</div>
	</div>

<?php if ( Tractor::setting( 'single_project_comment_enable' ) === '1' ) : ?>
	<?php get_template_part( 'components/comment-form' ); ?>
<?php endif; ?>

<?php
get_footer();
