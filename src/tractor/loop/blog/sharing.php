<?php
if ( ! class_exists( 'InsightCore_Share' ) ) {
	return;
}
$social_items = Tractor::setting( 'social_sharing_items' );
$social_order = Tractor::setting( 'social_sharing_order' );
if ( ! empty( $social_items ) ) {
	?>
	<div class="post-share">
		<div class="post-share-title heading-color"><?php esc_html_e( 'Share', 'tractor' ); ?></div>
		<div class="post-share-list">
			<?php InsightCore_Share::get_buttons( $social_items, $social_order ); ?>
		</div>
	</div>
	<?php
}
