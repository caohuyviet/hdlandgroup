<div class="post-read-more">
	<a href="<?php the_permalink(); ?>">
		<span class="btn-text">
			<?php esc_html_e( 'Read More', 'tractor' ); ?>
		</span>
	</a>
</div>
