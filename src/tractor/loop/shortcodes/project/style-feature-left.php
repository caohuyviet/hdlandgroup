<?php
$loop_count = 0;

while ( $tractor_query->have_posts() ) :
	$tractor_query->the_post();


	$meta                   = unserialize( get_post_meta( get_the_ID(), 'insight_project_options', true ) );
	$project_vi_tri         = Tractor_Helper::get_the_post_meta( $meta, 'project_vi_tri', '' );
	$project_quy_mo         = Tractor_Helper::get_the_post_meta( $meta, 'project_quy_mo', '' );
	$project_tong_dien_tich = Tractor_Helper::get_the_post_meta( $meta, 'project_tong_dien_tich', '' );
	?>
	<?php if ( $loop_count === 0 ) : ?>
	<div <?php post_class( 'project-item grid-item featured-project' ); ?>
		<?php if ( has_post_thumbnail() ) : ?>
			<?php
			$image_url = get_the_post_thumbnail_url( null, 'full' );

			if ( $image_size !== 'full' ) {
				$_sizes  = explode( 'x', $image_size );
				$_width  = $_sizes[0];
				$_height = $_sizes[1];

				$image_url = Tractor_Helper::aq_resize( array(
					'url'    => $image_url,
					'width'  => $_width,
					'height' => $_height,
					'crop'   => true,
					'echo'   => true,
					'alt'    => get_the_title(),
				) );
			}
			?>
			style="background-image: url('<?php echo esc_url( $image_url ); ?>')"
		<?php endif; ?>
	>
		<div class="post-item-wrap">
			<div class="post-info">
				<h5 class="post-title">
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h5>

				<div class="post-excerpt">
					<?php Tractor_Templates::excerpt( array(
						'limit' => 120,
						'type'  => 'character',
					) ); ?>
				</div>

				<div class="post-read-more">
					<a href="<?php the_permalink(); ?>">
						<span class="btn-text">
							<?php esc_html_e( 'Chi tiết', 'tractor' ); ?>
						</span>
						<span class="btn-icon ion-arrow-right-c"></span>
					</a>
				</div>
			</div>
		</div>
	</div>
<?php else: ?>
	<div <?php post_class( 'project-item grid-item normal-project' ); ?>>
		<div class="post-item-wrap">
			<div class="post-thumbnail-wrap"
				<?php if ( has_post_thumbnail() ) : ?>
					<?php
					$image_url = get_the_post_thumbnail_url( null, 'full' );

					if ( $image_size !== 'full' ) {
						$_sizes  = explode( 'x', $image_size );
						$_width  = $_sizes[0];
						$_height = $_sizes[1];

						$image_url = Tractor_Helper::aq_resize( array(
							'url'    => $image_url,
							'width'  => $_width,
							'height' => $_height,
							'crop'   => true,
							'echo'   => true,
							'alt'    => get_the_title(),
						) );
					}
					?>
					style="background-image: url('<?php echo esc_url( $image_url ); ?>')"
				<?php endif; ?>>
				<a href="<?php the_permalink(); ?>">
				</a>
			</div>
			<div class="post-info">
				<h5 class="post-title">
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h5>

				<div class="post-excerpt">
					<?php Tractor_Templates::excerpt( array(
						'limit' => 120,
						'type'  => 'character',
					) ); ?>
				</div>

				<div class="post-read-more">
					<a href="<?php the_permalink(); ?>">
						<span class="btn-text">
							<?php esc_html_e( 'Chi tiết', 'tractor' ); ?>
						</span>
						<span class="btn-icon ion-arrow-right-c"></span>
					</a>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
	<?php $loop_count ++; ?>
<?php endwhile; ?>
