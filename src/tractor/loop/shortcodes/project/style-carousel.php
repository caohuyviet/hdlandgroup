<?php
while ( $tractor_query->have_posts() ) :
	$tractor_query->the_post();
	$classes = array( 'project-item swiper-slide' );

	$meta                   = unserialize( get_post_meta( get_the_ID(), 'insight_project_options', true ) );
	$project_vi_tri         = Tractor_Helper::get_the_post_meta( $meta, 'project_vi_tri', '' );
	$project_quy_mo         = Tractor_Helper::get_the_post_meta( $meta, 'project_quy_mo', '' );
	$project_tong_dien_tich = Tractor_Helper::get_the_post_meta( $meta, 'project_tong_dien_tich', '' );
	?>
	<div <?php post_class( implode( ' ', $classes ) ); ?>>
		<div class="swiper-item">
			<a href="<?php the_permalink(); ?>">
				<div class="post-item-wrap">
					<div class="post-thumbnail-wrap">
						<a href="<?php the_permalink(); ?>">
							<div class="post-thumbnail">
								<?php if ( has_post_thumbnail() ) { ?>
									<?php
									$image_url = get_the_post_thumbnail_url( null, 'full' );

									if ( $image_size !== '' ) {
										$_sizes  = explode( 'x', $image_size );
										$_width  = $_sizes[0];
										$_height = $_sizes[1];

										Tractor_Helper::get_lazy_load_image( array(
											'url'    => $image_url,
											'width'  => $_width,
											'height' => $_height,
											'crop'   => true,
											'echo'   => true,
											'alt'    => get_the_title(),
										) );
									}
									?>
								<?php } else { ?>
									<?php Tractor_Templates::image_placeholder( 480, 480 ); ?>
								<?php } ?>

							</div>
						</a>
					</div>
					<div class="post-info">
						<h5 class="post-title">
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h5>
						<!--<div class="post-categories">
					<?php /*echo get_the_term_list( get_the_ID(), 'project_category', '', ', ', '' ); */
						?>
				</div>-->

						<?php if ( $project_vi_tri !== '' ) { ?>
							<div class="project-info project-vi-tri">
								<i class="tractor-icon-location-pin"></i>
								<span class="project-info-label"><?php echo esc_html( 'Vị trí: ', 'tractor' ) ?></span><?php echo esc_html( $project_vi_tri ); ?>
							</div>
						<?php } ?>

						<?php if ( $project_tong_dien_tich !== '' ) { ?>
							<div class="project-info project-tong-dien-tich">
								<i class="tractor-icon-house-plan"></i>
								<span class="project-info-label"><?php echo esc_html( 'Tổng diện tích: ', 'tractor' ) ?></span><?php echo esc_html( $project_tong_dien_tich ); ?>
							</div>
						<?php } ?>

						<?php if ( $project_quy_mo !== '' ) { ?>
							<div class="project-info project-quy-mo">
								<i class="tractor-icon-skycraper"></i>
								<span class="project-info-label"><?php echo esc_html( 'Quy mô: ', 'tractor' ) ?></span><?php echo esc_html( $project_quy_mo ); ?>
							</div>
						<?php } ?>
					</div>
				</div>
			</a>
		</div>
	</div>
<?php endwhile;
