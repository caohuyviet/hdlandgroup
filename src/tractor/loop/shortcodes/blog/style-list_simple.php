<?php
while ( $tractor_query->have_posts() ) :
	$tractor_query->the_post();
	$classes = array( 'grid-item', 'post-item' );
	?>
	<div <?php post_class( implode( ' ', $classes ) ); ?>>

		<div class="post-item-wrap">
			<?php if ( has_post_thumbnail() ) { ?>
				<div class="post-feature post-thumbnail">
					<a href="<?php the_permalink(); ?>">
						<?php
						$full_image_size = get_the_post_thumbnail_url( null, 'full' );
						Tractor_Helper::get_lazy_load_image( array(
							'url'    => $full_image_size,
							'width'  => 105,
							'height' => 80,
							'crop'   => true,
							'echo'   => true,
							'alt'    => get_the_title(),
						) );
						?>
					</a>
				</div>
			<?php } ?>

			<div class="post-info">

				<?php get_template_part( 'loop/blog/title' ); ?>

				<div class="post-date"><?php echo get_the_date(); ?></div>

			</div>
		</div>

	</div>
<?php endwhile;
