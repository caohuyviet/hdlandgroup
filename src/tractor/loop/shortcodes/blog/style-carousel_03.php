<?php
while ( $tractor_query->have_posts() ) :
	$tractor_query->the_post();
	$classes = array( 'post-item swiper-slide' );
	?>
	<div <?php post_class( implode( ' ', $classes ) ); ?>>
		<div class="post-item-wrap">
			<div class="post-info">
				<?php if ( has_category() ) : ?>
					<div class="post-categories">
						<?php the_category( ' ' ); ?>
					</div>
				<?php endif; ?>

				<?php get_template_part( 'loop/blog/title' ); ?>

				<div class="post-date">
					<?php echo get_the_date( 'M d, Y' ); ?>
				</div>
			</div>

			<div class="post-excerpt">
				<?php Tractor_Templates::excerpt( array(
					'limit' => 20,
					'type'  => 'word',
				) ); ?>
			</div>

			<div class="post-read-more">
				<a href="<?php the_permalink(); ?>">
					<span class="btn-text">
						<?php esc_html_e( 'Read More', 'tractor' ); ?>
					</span>
					<span class="btn-icon ion-arrow-right-c"></span>
				</a>
			</div>
		</div>
	</div>
<?php endwhile;
