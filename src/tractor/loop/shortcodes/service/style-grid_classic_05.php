<?php
while ( $tractor_query->have_posts() ) :
	$tractor_query->the_post();
	$classes = array( 'service-item grid-item' );

	$meta         = unserialize( get_post_meta( get_the_ID(), 'insight_service_options', true ) );
	$service_icon = Tractor_Helper::get_the_post_meta( $meta, 'service_icon', '' );
	?>
	<div <?php post_class( implode( ' ', $classes ) ); ?>>
		<div class="post-item-wrap">
			<div class="post-thumbnail-wrap"
				<?php if ( has_post_thumbnail() ) : ?>
					<?php
					$image_url = get_the_post_thumbnail_url( null, 'full' );

					if ( $image_size !== 'full' ) {
						$_sizes  = explode( 'x', $image_size );
						$_width  = $_sizes[0];
						$_height = $_sizes[1];

						$image_url = Tractor_Helper::aq_resize( array(
							'url'    => $image_url,
							'width'  => $_width,
							'height' => $_height,
							'crop'   => true,
							'echo'   => true,
							'alt'    => get_the_title(),
						) );
					}
					?>
					style="background-image: url('<?php echo esc_url( $image_url ); ?>')"
				<?php endif; ?>
			></div>

			<div class="post-info-wrap">
				<div class="post-info">

					<h3 class="post-title">
						<?php if ( $service_icon !== '' ) { ?>
							<i class="post-icon <?php echo esc_attr( $service_icon ); ?>"></i>
						<?php } ?>
						<a href="<?php the_permalink(); ?>"><?php echo Tractor_Helper::break_words( get_the_title() ); ?></a>
					</h3>

					<div class="post-excerpt">
						<?php Tractor_Templates::excerpt( array(
							'limit' => 27,
							'type'  => 'word',
						) ); ?>
					</div>

					<a class="post-read-more" href="<?php the_permalink(); ?>">
						<span class="btn-text">
							<?php esc_html_e( 'Read More', 'tractor' ); ?>
						</span>
					</a>

				</div>
			</div>

		</div>
	</div>
<?php endwhile;
