<?php
$service_slides_template = '';
$service_thumbs_template = '';
?>

<?php while ( $tractor_query->have_posts() ) : $tractor_query->the_post(); ?>
	<?php
	$meta         = unserialize( get_post_meta( get_the_ID(), 'insight_service_options', true ) );
	$service_icon = Tractor_Helper::get_the_post_meta( $meta, 'service_icon', '' );

	$service_thumbs_template .= '<div class="swiper-slide"><div class="post-info"><div class="post-icon"><i class="' . esc_attr( $service_icon ) . '"></i></div><div class="post-title">' . get_the_title() . '</div></div></div>';
	?>

	<?php ob_start(); ?>

	<div class="swiper-slide">
		<div class="service-item"
		     style="background-image: url(<?php echo get_the_post_thumbnail_url( get_the_ID(), 'full' ); ?>);">
			<div class="container">
				<div class="service-item-inner">
					<div class="service-title">
						<?php the_title(); ?>
					</div>
					<div class="service-excerpt">
						<?php echo get_the_excerpt(); ?>
					</div>
					<div class="service-more">
						<a href="<?php echo get_permalink(); ?>"><?php esc_html_e( 'Read More', 'tractor' ); ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php
	$service_slides_template .= ob_get_contents();
	ob_end_clean();
	?>

<?php endwhile; ?>

<div class="tm-swiper tm-service-pagination equal-height v-center h-center">
	<div class="swiper-container">
		<div class="swiper-wrapper">
			<?php echo "{$service_thumbs_template}"; ?>
		</div>
	</div>
</div>

<div class="swiper-container">
	<div class="swiper-wrapper">
		<?php echo "{$service_slides_template}"; ?>
	</div>
</div>
