<?php
extract( $tractor_shortcode_atts );
?>
<div class="swiper-container">
	<div class="swiper-wrapper">
		<?php while ( $tractor_query->have_posts() ) :
			$tractor_query->the_post();

			$_meta = unserialize( get_post_meta( get_the_ID(), 'insight_testimonial_options', true ) );
			?>
			<div class="swiper-slide">

				<div class="testimonial-item">
					<?php if ( has_post_thumbnail() ): ?>
						<?php
						$full_image_size = get_the_post_thumbnail_url( null, 'full' );
						$image_url       = Tractor_Helper::aq_resize( array(
							'url'    => $full_image_size,
							'width'  => 85,
							'height' => 85,
							'crop'   => true,
						) );
						?>
						<div class="post-thumbnail">
							<img src="<?php echo esc_url( $image_url ); ?>"
							     alt="<?php echo esc_attr( get_the_title() ); ?>"/>
						</div>
					<?php endif; ?>

					<div class="testimonial-info">
						<div class="testimonial-main-info">
							<h6 class="testimonial-name">
								<?php the_title(); ?>
								<?php if ( isset( $_meta['by_line'] ) ) : ?>
									<span
										class="testimonial-by-line"><?php echo esc_html( $_meta['by_line'] ); ?></span>
								<?php endif; ?>
							</h6>
						</div>
						<?php if ( isset( $_meta['rating'] ) && $_meta['rating'] !== '' ): ?>
							<div class="testimonial-rating">
								<?php Tractor_Templates::get_rating_template( $_meta['rating'] ); ?>
							</div>
						<?php endif; ?>
						<div class="testimonial-desc"><?php the_content(); ?></div>
					</div>

					<div class="line"></div>

				</div>

			</div>

		<?php endwhile; ?>
	</div>
</div>
