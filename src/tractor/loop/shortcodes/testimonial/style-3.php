<?php
extract( $tractor_shortcode_atts );
?>
<div class="swiper-container">
	<div class="swiper-wrapper">
		<?php while ( $tractor_query->have_posts() ) :
			$tractor_query->the_post();

			$_meta = unserialize( get_post_meta( get_the_ID(), 'insight_testimonial_options', true ) );
			?>
			<div class="swiper-slide">

				<div class="testimonial-item">
					<?php if ( has_post_thumbnail() ): ?>
						<?php
						$full_image_size = get_the_post_thumbnail_url( null, 'full' );
						$image_url       = Tractor_Helper::aq_resize( array(
							'url'    => $full_image_size,
							'width'  => 500,
							'height' => 380,
							'crop'   => true,
						) );
						?>
						<div class="post-thumbnail">
							<img src="<?php echo esc_url( $image_url ); ?>"
							     alt="<?php echo esc_attr( get_the_title() ); ?>"/>
						</div>
					<?php endif; ?>

					<div class="quote-icon">
						<svg width="25px" height="18px" viewBox="0 0 25 18" version="1.1"
						     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							<defs></defs>
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<g transform="translate(-390.000000, -3643.000000)" fill="#FFFFFF" fill-rule="nonzero"
								   id="Group-14">
									<g transform="translate(375.000000, 3626.000000)">
										<g id="Group-2">
											<g id="quote" transform="translate(15.000000, 17.000000)">
												<polygon id="Shape"
												         points="0 10.8000169 5.5 10.8000169 1.83330469 18 7.33330469 18 11 10.8000169 11 0 0 0"></polygon>
												<polygon id="Shape"
												         points="14 0 14 10.8000169 19.5 10.8000169 15.8333047 18 21.3333047 18 25 10.8000169 25 0"></polygon>
											</g>
										</g>
									</g>
								</g>
							</g>
						</svg>
					</div>

					<div class="testimonial-info">
						<div class="testimonial-main-info">
							<h6 class="testimonial-name">
								<?php the_title(); ?>
								<?php if ( isset( $_meta['by_line'] ) ) : ?>
									<span
										class="testimonial-by-line"><?php echo esc_html( $_meta['by_line'] ); ?></span>
								<?php endif; ?>
							</h6>
						</div>
						<div class="testimonial-desc"><?php the_content(); ?></div>
						<?php if ( isset( $_meta['rating'] ) && $_meta['rating'] !== '' ): ?>
							<div class="testimonial-rating">
								<?php Tractor_Templates::get_rating_template( $_meta['rating'] ); ?>
							</div>
						<?php endif; ?>
					</div>

					<div class="line"></div>

				</div>

			</div>

		<?php endwhile; ?>
	</div>
</div>
