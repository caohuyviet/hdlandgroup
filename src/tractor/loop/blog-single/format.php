<?php if ( has_post_thumbnail() ) { ?>
	<?php
	$tractor_thumbnail_w = 1170;
	$tractor_thumbnail_h = 740;

	$full_image_size = get_the_post_thumbnail_url( null, 'full' );
	?>
	<div class="post-feature post-thumbnail">
		<?php
		Tractor_Helper::get_lazy_load_image( array(
			'url'    => $full_image_size,
			'width'  => $tractor_thumbnail_w,
			'height' => $tractor_thumbnail_h,
			'crop'   => true,
			'echo'   => true,
			'alt'    => get_the_title(),
		) );
		?>

	</div>
<?php } ?>
