<?php
/**
 * Define constant
 */
$theme = wp_get_theme();

if ( ! empty( $theme['Template'] ) ) {
	$theme = wp_get_theme( $theme['Template'] );
}

if ( ! defined( 'DS' ) ) {
	define( 'DS', DIRECTORY_SEPARATOR );
}

define( 'TRACTOR_THEME_NAME', $theme['Name'] );
define( 'TRACTOR_THEME_VERSION', $theme['Version'] );
define( 'TRACTOR_THEME_DIR', get_template_directory() );
define( 'TRACTOR_THEME_URI', get_template_directory_uri() );
define( 'TRACTOR_THEME_IMAGE_DIR', get_template_directory() . DS . 'assets' . DS . 'images' );
define( 'TRACTOR_THEME_IMAGE_URI', get_template_directory_uri() . DS . 'assets' . DS . 'images' );
define( 'TRACTOR_CHILD_THEME_URI', get_stylesheet_directory_uri() );
define( 'TRACTOR_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'TRACTOR_FRAMEWORK_DIR', get_template_directory() . DS . 'framework' );
define( 'TRACTOR_CUSTOMIZER_DIR', TRACTOR_THEME_DIR . DS . 'customizer' );
define( 'TRACTOR_WIDGETS_DIR', TRACTOR_THEME_DIR . DS . 'widgets' );
define( 'TRACTOR_VC_MAPS_DIR', TRACTOR_THEME_DIR . DS . 'vc-extend' . DS . 'vc-maps' );
define( 'TRACTOR_VC_PARAMS_DIR', TRACTOR_THEME_DIR . DS . 'vc-extend' . DS . 'vc-params' );
define( 'TRACTOR_VC_SHORTCODE_CATEGORY', esc_html__( 'By', 'tractor' ) . ' ' . TRACTOR_THEME_NAME );
define( 'TRACTOR_PROTOCOL', is_ssl() ? 'https' : 'http' );

require_once TRACTOR_FRAMEWORK_DIR . '/class-static.php';

$files = array(
	TRACTOR_FRAMEWORK_DIR . '/class-init.php',
	TRACTOR_FRAMEWORK_DIR . '/class-global.php',
	TRACTOR_FRAMEWORK_DIR . '/class-actions-filters.php',
	TRACTOR_FRAMEWORK_DIR . '/class-admin.php',
	TRACTOR_FRAMEWORK_DIR . '/class-customize.php',
	TRACTOR_FRAMEWORK_DIR . '/class-enqueue.php',
	TRACTOR_FRAMEWORK_DIR . '/class-functions.php',
	TRACTOR_FRAMEWORK_DIR . '/class-helper.php',
	TRACTOR_FRAMEWORK_DIR . '/class-color.php',
	TRACTOR_FRAMEWORK_DIR . '/class-import.php',
	TRACTOR_FRAMEWORK_DIR . '/class-kirki.php',
	TRACTOR_FRAMEWORK_DIR . '/class-metabox.php',
	TRACTOR_FRAMEWORK_DIR . '/class-plugins.php',
	TRACTOR_FRAMEWORK_DIR . '/class-query.php',
	TRACTOR_FRAMEWORK_DIR . '/class-custom-css.php',
	TRACTOR_FRAMEWORK_DIR . '/class-templates.php',
	TRACTOR_FRAMEWORK_DIR . '/class-aqua-resizer.php',
	TRACTOR_FRAMEWORK_DIR . '/class-visual-composer.php',
	TRACTOR_FRAMEWORK_DIR . '/class-vc-icon-ion.php',
	TRACTOR_FRAMEWORK_DIR . '/class-vc-icon-pe-stroke-7.php',
	TRACTOR_FRAMEWORK_DIR . '/class-vc-icon-tractor.php',
	TRACTOR_FRAMEWORK_DIR . '/class-vc-icon-icomoon.php',
	TRACTOR_FRAMEWORK_DIR . '/class-walker-nav-menu.php',
	TRACTOR_FRAMEWORK_DIR . '/class-widget.php',
	TRACTOR_FRAMEWORK_DIR . '/class-widgets.php',
	TRACTOR_FRAMEWORK_DIR . '/class-footer.php',
	TRACTOR_FRAMEWORK_DIR . '/class-post-type-blog.php',
	TRACTOR_FRAMEWORK_DIR . '/class-post-type-service.php',
	TRACTOR_FRAMEWORK_DIR . '/class-post-type-project.php',
	TRACTOR_FRAMEWORK_DIR . '/class-woo.php',
	TRACTOR_FRAMEWORK_DIR . '/tgm-plugin-activation.php',
	TRACTOR_FRAMEWORK_DIR . '/tgm-plugin-registration.php',
);

/**
 * Load Framework.
 */
Tractor::require_files( $files );

/**
 * Init the theme
 */
Tractor_Init::instance();
