=== Tractor ===

Contributors: thememove
Tags: editor-style, featured-images, microformats, post-formats, rtl-language-support, sticky-post, threaded-comments, translation-ready

Requires at least: 4.4
Tested up to: 4.9.x
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Tractor - Industrial & Manufacturing Businesses WordPress Theme

== Description ==

Tractor brings the young and vibrant look to your website with high flexibility in customization. This is the theme of beautifully crafted pages and elements for multiple purposes.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =


== Changelog ==

= 1.4.7 - May 14 2021 =
- Updated:
Insight Core plugin - v.2.2.0
Revolution Slider plugin - v.6.4.8
WPBakery Page Builder Clipboard plugin - v.4.5.8

= 1.4.6 - March 12 2021 =
- Updated:
Insight Core plugin - v.2.0.0
Revolution Slider plugin - v.6.4.3

= 1.0.0 - July 18 2018 =
* Initial release

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2015 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
