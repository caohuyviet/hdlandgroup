<?php
// Check post meta off all pages
add_action( 'init', 'test' );
function test() {
	$pages   = get_pages();
	if ( $pages ) {
		foreach ( $pages as $page ) {
			$post_options = unserialize( get_post_meta( $page->ID, 'insight_page_options', true ) );
			if ( $post_options !== false && isset( $post_options['custom_logo'] ) && $post_options['custom_logo'] !== '' ) {
				Tractor_Helper::d( $page->post_title );
			}
			/*if ( strpos( $page->post_content, 'tm_product_categories' ) !== false) {
				Tractor_Helper::d( $page->post_title );
			}*/
		}
	}
}

add_action( 'init', 'test' );
function test() {
	$types = array(
		'post',
		'page',
		'service',
		'case_study',
	);

	foreach ( $types as $type ) {
		$query = new WP_Query( array(
			'post_type'      => $type,
			'posts_per_page' => - 1,
			'post_status'    => 'publish',
		) );

		while ( $query->have_posts() ) {
			$query->the_post();


			if ( strpos( get_the_content(), 'tm_box_icon style="20"' ) !== false ) {
				Tractor_Helper::d( get_the_title() );
			}

		}

		wp_reset_query();
	}
}
